/* eslint-disable camelcase,prefer-const */

import { apiRequest } from 'api/config/request';
import { getFormData } from 'utils/api';

export default class BranchService {
  static getBranches() {
    return apiRequest({
      url: '/branches',
      method: 'GET',
    });
  }

  static getBranch(slug, abortSignal) {
    return apiRequest({
      url: `/branches/${slug}`,
      method: 'GET',
      signal: abortSignal,
    });
  }

  static getBranchEvents(slug, abortSignal) {
    return apiRequest({
      url: `/branches/${slug}/events`,
      method: 'GET',
      signal: abortSignal,
    });
  }

  static editBranch(slug, data, withBannerPicture = false) {
    let {
      profile_picture_path, ...requestData
    } = data;
    if (withBannerPicture) {
      requestData = {
        ...requestData,
        profile_picture_path,
      };
    }
    return apiRequest({
      url: `/branches/${slug}`,
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      data: getFormData({
        ...requestData,
        _method: 'PUT',
      }),
    });
  }

  static manageBranchMembership(branchSlug, applicationSlug, acceptance) {
    return apiRequest({
      url: `/branches/${branchSlug}/applications/${applicationSlug}/approval`,
      method: 'POST',
      data: {
        acceptance,
      },
    });
  }

  static removeMember(branchSlug, userSlug) {
    return apiRequest({
      url: `/branches/${branchSlug}/remove/${userSlug}`,
      method: 'DELETE',
    });
  }

  static apply(branchSlug) {
    return apiRequest({
      url: `/branches/${branchSlug}/apply`,
      method: 'POST',
    });
  }

  static changeMemberRole(branchSlug, userSlug, newRole) {
    return apiRequest({
      url: `/branches/${branchSlug}/change-role/${userSlug}`,
      method: 'POST',
      data: {
        branch_role_id: newRole,
      },
    });
  }
}

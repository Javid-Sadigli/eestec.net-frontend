import { apiRequest } from 'api/config/request';

export default class MemberService {
  static getMembers(endpoint, abortSignal) {
    return apiRequest({
      url: endpoint,
      method: 'GET',
      signal: abortSignal,
    });
  }
}

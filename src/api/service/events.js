/* eslint-disable camelcase,prefer-const */

import { apiRequest, request } from 'api/config/request';
import axios from 'axios';
import { getFormData } from 'utils/api';

export default class EventService {
  static getEvents({ customGetEventsLink, filters }) {
    if (customGetEventsLink) {
      return request(axios, {
        url: `${customGetEventsLink}${filters ? `&${filters}` : ''}`,
        method: 'GET',
      });
    }
    return apiRequest({
      url: `/events${filters ? `?${filters}` : ''}`,
      method: 'GET',
    });
  }

  static getEvent(slug) {
    return apiRequest({
      url: `/events/${slug}`,
      method: 'GET',
    });
  }

  static editEvent(slug, data, withBannerPicture = false, withSquarePicture = false) {
    let {
      profile_picture, banner_path, ...requestData
    } = data;
    if (withBannerPicture) {
      requestData = {
        ...requestData,
        banner_path,
      };
    } else if (withSquarePicture) {
      requestData = {
        ...requestData,
        profile_picture,
      };
    }
    return apiRequest({
      url: `/events/${slug}`,
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      data: getFormData({
        ...requestData,
        _method: 'PUT',
      }),
    });
  }

  static getEventTypes() {
    return apiRequest({
      url: '/events/types',
      method: 'GET',
    });
  }
}

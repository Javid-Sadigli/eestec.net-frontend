/*
 This file contains the utility function used to manipulate the browser DOM
 or extract information from it.
*/

let ID_COUNTER = 1;

// Checks if a part of an element is outside of the screen, so the element isn't fully visible
export const isBeyondScreen = (element, addition = 0) => {
  const rect = element.getBoundingClientRect();
  return (window.innerHeight < rect.bottom + addition) && rect.top > addition;
};

// TODO: This can be replaced with useId() when we migrate to React 18
// eslint-disable-next-line no-plusplus
export const generateRandomId = (prefix = 'id') => `${prefix}-${ID_COUNTER++}`;

// Checks if two elements overlap in the DOM
export const elementsOverlap = (el1, el2) => {
  if (!el1.current || !el2.current) {
    return false;
  }
  const domRect1 = el1.current.getBoundingClientRect();
  const domRect2 = el2.current.getBoundingClientRect();

  return !(
    domRect1.top > domRect2.bottom
    || domRect1.right < domRect2.left
    || domRect1.bottom < domRect2.top
    || domRect1.left > domRect2.right
  );
};

// Get the full dimensions of an element, including margin and padding
export const getElementFullDimension = (element) => {
  const computedStyle = window.getComputedStyle(element);
  const dimensionMapper = (item) => parseInt(computedStyle[item], 10);
  const widthElements = ['width', 'paddingLeft', 'paddingRight', 'marginLeft', 'marginRight'];
  const heightElements = ['height', 'paddingTop', 'paddingBottom', 'marginTop', 'marginBottom'];
  return {
    width: widthElements.map(dimensionMapper).reduce((a, b) => a + b),
    height: heightElements.map(dimensionMapper).reduce((a, b) => a + b),
  };
};

// getMaxScrollLeft gets the maximum value that the scrollLeft property of a container can have
export const getMaxScrollLeft = (container) => container.scrollWidth - container.clientWidth;

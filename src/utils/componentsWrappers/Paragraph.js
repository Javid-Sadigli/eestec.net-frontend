import React from 'react';

const Paragraph = React.forwardRef((props, ref) => (
  <p {...props} ref={ref} />
));

export default Paragraph;

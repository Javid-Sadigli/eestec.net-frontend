import React from 'react';

const Div = React.forwardRef((props, ref) => (
  <div {...props} ref={ref} />
));

export default Div;

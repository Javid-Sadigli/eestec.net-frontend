/*
 This file contains helper functions for manipulating branches data,
 extracting specific information from it and others.
*/

import { acronym } from 'utils/misc';

// Returns true if a branch has a status of TERMINATED, otherwise false
export const isBranchTerminated = (branchType) => branchType?.name.toLowerCase() === 'terminated';

// Returns the full name of a branch, e.g. "LC Waikiki"
export const getBranchTypeAndName = (name, type) => {
  if (type?.name && !isBranchTerminated(type)) {
    // Only LCs can be suspended so for SUSPENDED status, present the name as LC
    const typeName = type.name.toLowerCase() === 'suspended' ? 'Local Committee' : type.name;
    // Acronymize the type of the branch
    return `${acronym(typeName)} ${name}`;
  }
  // Return just the name of the city if there is no information about the branch type
  // or if the branch has been terminated
  return name;
};

// branchesTypeOrdering provides an order for branches types sorting since
const branchesTypeOrdering = {
  'Local Committee': 4,
  'Junior Local Committee': 3,
  Observer: 2,
  Suspended: 4, // treat suspended like an LC when it comes to ordering
  Terminated: 1,
};

// Sorting interface for branches. Sort first by branch type, then by name
export const sortBranchesByTypeAndName = (a, b) => {
  // If there is no information about type of at least one of the pair of branches, sort by name
  if (!a.branch_type || !b.branch_type) {
    return a.name.localeCompare(b.name);
  }
  const diff = branchesTypeOrdering[b.branch_type.name] - branchesTypeOrdering[a.branch_type.name];
  if (!diff) {
    return a.name.localeCompare(b.name);
  }
  return diff;
};

// Constant values for handling application status
export const BranchApplicationStatus = {
  // A given user cannot apply to the branch (is already a member or is a guest)
  INVALID: 0,
  // A given user can apply to the branch
  CAN_APPLY: 1,
  // The application of a given user is pending acceptance
  PENDING: 2,
};

/*
  getBranchApplicationStatus identifies what application status the user
  who is a member of branches defined in branchesList and
  who applied to branches in applicationsList has.
*/
export const getBranchApplicationStatus = (branchSlug, branchesList, applicationsList) => {
  // If the user has no branches (so a guest visits the page), the status is invalid
  if (!branchesList) {
    return BranchApplicationStatus.INVALID;
  }

  const applicationFinder = (branchApplicationInfo) => branchApplicationInfo.slug === branchSlug;

  if (branchesList.find(applicationFinder)) { // a user is a member of the branch
    return BranchApplicationStatus.INVALID;
  }
  if (applicationsList.find(applicationFinder)) { // a user has applied to the branch
    return BranchApplicationStatus.PENDING;
  }
  return BranchApplicationStatus.CAN_APPLY; // otherwise can apply
};

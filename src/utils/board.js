/*
 This file contains hardcoded information about the Board of Association members
 presented at https://eestec.net/contact

 At some point it maybe good to put that data in BE and fetch it.
*/

import imgAA from 'assets/img/board/vc-aa.png';
import imgEA from 'assets/img/board/vc-ea.jpg';
import imgIA from 'assets/img/board/vc-ia.jpg';
import imgChair from 'assets/img/board/chair.jpg';
import imgTreasurer from 'assets/img/board/treasurer.png';

const boardMembersData = [
  {
    name: 'Anastasia Outziola',
    img: imgChair,
    desc: [
      'the operational work of the Board,',
      'Board Meetings,',
      'Congresses.',
    ],
    occupation: 'Chairperson',
  },
  {
    name: 'Maritina Bizoumi',
    img: imgTreasurer,
    desc: [
      'account keeping and financial reports,',
      'the Board\'s access to the Association\'s bank accounts',
      'maintaining and updating the legal documents of the Association.',
    ],
    occupation: 'Treasurer',
  },
  {
    name: 'Julia Jaworska',
    img: imgEA,
    desc: [
      'initiation of EESTEC branches,',
      'contact with other NGOs,',
      'contacts with universities,',
      'company contacts and fundraising,',
      'public relations.',
    ],
    occupation: 'Vice Chairperson for External Affairs',
  },
  {
    name: 'Andreas Symeonidis',
    img: imgIA,
    desc: [
      'coordinating the work of Contact Persons,',
      'the activation and motivation of internationally active members of branches,',
      'the activation, education and motivation of all levels of EESTEC branches,',
      'cooperations within the network of EESTEC.',
    ],
    occupation: 'Vice Chairperson for Internal Affairs',
  },
  {
    name: 'Fotis Diamantidis',
    img: imgAA,
    desc: [
      'digital and physical documentation preservation and transfer,',
      'data collection and evaluation,',
      'status reports,',
      'ensuring internal compliance with internal regulations and legal documents,',
      'quality, validity and effective conduct of EESTEC Events.',
    ],
    occupation: 'Vice Chairperson for Administrative Affairs',
  },
];

export default boardMembersData;

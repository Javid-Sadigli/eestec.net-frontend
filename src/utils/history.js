/*
 This file contains hardcoded EESTEC history information
 presented at https://eestec.net/about-us

 At some point it maybe (?) good to put that data in BE and fetch it.
*/

import histImg1 from 'assets/img/history/1 - 1986 -The organisers of the EESTIC_86.jpg';
import histImg2 from 'assets/img/history/2 - 1990.jpg';
import histImg3 from 'assets/img/history/3 - 1995.jpg';
import histImg4 from 'assets/img/history/4 - 1997.jpg';
import histImg5 from 'assets/img/history/5 - 1998.jpg';
import histImg6 from 'assets/img/history/6 - 1999.jpg';
import histImg7 from 'assets/img/history/7 - 2018.jpg';
import histLogo from 'assets/img/history/old-logo.jpg';

const historyData = [
  {
    title: 'December 1964',
    text: 'The need for creating an association for European electrical engineering students'
      + ' existed long before EESTEC itself was created. The first official attempt of doing such'
      + ' a thing was in December of 1964 when Berlin Electrotechnical Faculty invited 18 European'
      + ' university representatives to meet in Berlin.  In the last General Assembly, EURIELEC was'
      + ' founded and the first EURIELEC Congress was organized in 1965, thanks'
      + ' to the Dutch industry\'s generous support. Despite'
      + ' the success in its early years, EURIELEC was dissolved in 1972.'
      + ' \nVarious attempts were made over the next twelve years, but no organization was able to form'
      + ' a sustainable structure to reconnect electrical engineering students in Europe.',
    img: histImg1,
  },
  {
    title: '1984',
    text: 'In 1984, the boards of three Dutch student guilds from ETV (Delft), Thor (Eindhoven) and'
      + ' Scintilla (Enschede) decided to try and reignite the interest of other European Student Associations'
      + ' in renewing the international student activities. They exchanged ideas with professional organizations'
      + ' such as IEEE EUREL and SEFI and invited all former EURIELEC member universities to take part in a new'
      + ' international annual conference for electrical engineering students, which they later named EESTIC'
      + ' (Electrical Engineering STudents International Conference).',
    img: histLogo,
  },
  {
    title: 'April 27 to May 3, 1986',
    text: 'The inaugural gathering was held in Eindhoven, The Netherlands, between April 27 and May 3, 1986,'
      + ' and was attended by 50 students from 33 different cities in 17 different countries. The last day of '
      + 'the conference is considered to be the founding of EESTEC. The delegates agreed on a list of 14 clauses,'
      + ' describing the structure and function of the newly formed organization. Also, the concept of NatComs'
      + ' (National Committees) was introduced, as a single point of contact for each country.',
    img: histImg2,
  },
  {
    title: '1987, Second Congress',
    text: 'The second EESTEC Congress, held in Nova Gorica and Ljubljana, seems to be the Congress of the'
      + ' National Committee creation and a lot of initiatives were started. The final number of participants'
      + ' was 44, coming from 35 cities in 14 European countries. This is the year in which the current logo appeared.'
      + '\nAlso, in 1987 there were two workshops: in Vienna (Nov 6.-9. 1987), 25 participants'
      + ' from 12 countries, and in Madrid (Dec. 12.-14. 1987), where participants from 4 countries participated.',
    img: histImg3,
  },
  {
    title: '1989 - 1990',
    text: 'Some key changes were made to the statutes during the 4th conference in Budapest in 1989.'
      + ' Although the EESTEC acronym was kept, the full name was changed to Electrical Engineering STudents European association'
      + ' to reflect the year-round activities. The annual meeting was also renamed from “Conference” to “Congress”,'
      + ' and the first international board was elected, consisting of a Chairman and one Vice-Chairman.'
      + '\nThe structure of the international board was changed a year later in Zurich, as the second board was elected that consisted'
      + ' of a Chairman, and two Vice-Chairmen.',
    img: histImg4,
  },
  {
    title: '1995',
    text: 'EESTEC was officially incorporated in 1995, and the official seat was moved from Budapest to Zurich to facilitate international'
      + ' financial transactions. Also, in the same year, the NatComs were eliminated, giving each LC (Local Committee) direct representation'
      + ' at the EESTEC activities, and its own levels of engagement.',
    img: histImg5,
  },
  {
    title: 'Some important facts',
    text: '1998 - Alumni relations functions were added to the organization'
      + '\n2007 - the official logo/signature was finalized as it is today'
      + ', the definition of Local Committees was changed, the terms Observers (new committees) and Junior Local Committee were introduced',
    img: histImg6,
  },
  {
    title: 'Nowadays',
    text: 'EESTEC can be found in 46 cities around Europe. Its members are still very active and keep organizing events locally and internationally'
      + ' in order to meet each other, grow and create various opportunities for electrical engineering and computer science students from all over the continent.'
      + '\n The current EESTEC board consists of a Chairperson, a Treasurer, Vice-Chairperson for External Affairs, Vice-Chairperson for Internal Affairs and Vice-Chairperson for Administrative Affairs.',
    img: histImg7,
  },
];

export default historyData;

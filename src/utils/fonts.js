/*
 This file imports the CSS information about the self-hosted fonts that EESTEC.net uses
*/

import '@fontsource/roboto/100.css';
import '@fontsource/roboto/300.css';
import '@fontsource/roboto/400.css';
import '@fontsource/roboto/500.css';
import '@fontsource/roboto/900.css';

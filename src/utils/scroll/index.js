/** ***************
 * Original source from: https://codesandbox.io/embed/scroll-to-element-react-hs0gj
 **************** */

import scroll, { getElementPosition } from 'utils/scroll/scroll';

export default scroll;
export {
  getElementPosition,
};

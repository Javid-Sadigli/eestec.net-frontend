/*
 This file contains hardcoded information about branch roles.

 At some point it maybe good to fetch this data from BE but it does not seem necessary.
*/

// eslint-disable-next-line import/prefer-default-export
export const BranchRoles = {
  BOARD: 1,
  ACTIVE_MEMBER: 2,
  ALUMNI: 3,
};

/*
 This file contains hardcoded testimonials information
 presented at https://eestec.net/about-us

 At some point it maybe (?) good to put that data in BE and fetch it.
*/

import testimonialImgSebastian from 'assets/img/testimonials/sebastian.jpg';
import testimonialImgNil from 'assets/img/testimonials/nil.jpg';
import testimonialImgDimitris from 'assets/img/testimonials/dimitris.jpg';
import testimonialImgVasilije from 'assets/img/testimonials/vasilije.jpg';
import testimonialImgMarko from 'assets/img/testimonials/marko.jpg';
import testimonialImgUros from 'assets/img/testimonials/uros.jpg';

const testimonials = [
  {
    id: 1,
    name: 'Dimitris Dedemadis',
    position: 'BATCAM - LC Xanthi',
    text: 'During the past years, in my EESTEC path, I have been through '
      + 'quite a few positions and roles that I can say have contributed '
      + 'to my growth and the person I am today. An experience that had '
      + 'stood out from this was being the Project Leader of the EESTech '
      + 'Challenge Project during the 2020/2021 mandate. Inside EESTEC, '
      + 'you have the opportunity to reflect on your priorities, your '
      + 'work ethic, your sense of responsibility, and your values. '
      + 'This is an experience that stays with you and I can say it '
      + 'still fuels me with inspiration for my future steps. The last '
      + 'words I would like to share is that people are the most '
      + 'important aspect, good results and great final products '
      + 'are just a side effect of putting the effort and taking '
      + 'care of the needs of the people around you. Well, I guess '
      + 'EESTEC is the perfect environment to experience that '
      + 'and I am grateful I could be a part of it.',
    img: testimonialImgDimitris,
  },
  {
    id: 2,
    name: 'Sebastian Skoczeń',
    position: 'Alumni - LC Krakow',
    text: 'At some point in your life, you\'ll realize that at the end of '
      + 'the day the greatest capital of your life are people - people '
      + 'you\'ve met, people you\'ve influenced and finally people with '
      + 'whom you\'ve shared part of your journey. It\'s all about the '
      + 'people and EESTEC is people.',
    img: testimonialImgSebastian,
  },
  {
    id: 3,
    name: 'Uroš Savurdić',
    position: 'ex-Regionalization Project Leader - LC Belgrade',
    text: 'I\'ve joined EESTEC with the goal to travel and meet new '
      + 'people, and I am still active with the passion to grow and '
      + 'support others in their development. Being a leader on '
      + 'the international level made me more clear about who I '
      + 'am and what parts of me I would like to improve and I '
      + 'also made some bonds with amazing individuals from all around '
      + 'Europe. I believe that it is better to have intense '
      + 'experiences while we are students because now I have a clear '
      + 'vision of where I want to be in the next couple of years.',
    img: testimonialImgUros,
  },
  {
    id: 4,
    name: 'Marko Rajković',
    position: 'Alumni - LC Novi Sad',
    text: 'My EESTEC experience started on the first day of my studies. '
      + 'Along the way, I’ve met dozens of amazing people, gained '
      + 'a handful of very useful hard and soft skills, applied '
      + 'knowledge practically, and most importantly, gained a '
      + 'family for life. All of this has transferred well to '
      + 'my professional life now.',
    img: testimonialImgMarko,
  },
  {
    id: 5,
    name: 'Nil Buket Irmak',
    position: 'Member - LC Istanbul',
    text: 'I have been with EESTEC since October 2021. '
      + 'From what I have seen in my own branch and in the '
      + 'International Teams, I can say that the atmosphere '
      + 'and energy of EESTEC are incredibly beautiful. It '
      + 'is a great advantage that there are so many '
      + 'options for one\'s self-development and that '
      + 'EESTEC\'s activities for development, information '
      + 'and entertainment are continuous. Even as a PR '
      + 'assistant and Design Team member in my branch, '
      + 'I learned a lot of interesting things. I am very happy '
      + 'with everything I can experience here.',
    img: testimonialImgNil,
  },
  {
    id: 6,
    name: 'Vasilije Pantić',
    position: 'COC - LC Novi Sad',
    text: 'Every EESTECer has its own unique experience being in the '
      + 'Association and I can definitely say that my EESTEC experience '
      + 'is a delightful journey. In EESTEC, I had worked on many projects '
      + 'as a member and as a coordinator, both locally and internationally '
      + 'and through that work I had met many amazing people that I\'m still '
      + 'in contact with and had so much fun with. Spring Congress 2020 made '
      + 'my creativity and out-of-the-box thinking grow bigger. I have also '
      + 'realized how much potential I have by working with more experienced '
      + 'people. It was a marvelous experience that I will always remember '
      + 'as my biggest trophy in EESTEC. I would recommend everyone to '
      + 'somehow be part of EESTEC because this is not an opportunity like '
      + 'others and no one regrets the experience from it.',
    img: testimonialImgVasilije,
  },
];

export default testimonials;

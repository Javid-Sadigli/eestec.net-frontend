/*
 This file contains all the validators that are used by the forms in the project.
 These validators can be passed as arguments while using the hook useForm().

 If a validation fails, the validators return a string with the error information, otherwise null.
*/

export const validateEmail = (email) => (
  // use a regular expression checking if a string is an email
  /^[^\s@]+@[^\s@]+\.[^\s@]+$/.test(email)
    ? null
    : 'This is not a valid email address.'
);

export const validateSamePasswords = (pass1, pass2) => (pass1 === pass2 ? null : 'Passwords do not match.');

export const validateNotEmpty = (text) => (text ? null : 'This field cannot be empty.');

export const validateMinLength = (text, minLength) => (
  (text.length >= minLength) ? null : `The minimum length is ${minLength} characters.`
);

export const validateMaxLength = (text, maxLength) => (
  (text.length <= maxLength) ? null : `The maximum length is ${maxLength} characters.`
);

export const validateStrongPassword = (text) => {
  const requirements = {
    upperCase: false,
    lowerCase: false,
    number: false,
    special: false,
  };
  for (const char of text) {
    // eslint-disable-next-line no-restricted-globals
    if (!isNaN(char)) {
      requirements.number = true;
    } else if (char.toUpperCase() === char.toLowerCase()) {
      requirements.special = true;
    } else if (char.toUpperCase() === char) {
      requirements.upperCase = true;
    } else {
      requirements.lowerCase = true;
    }
    if (Object.values(requirements).every((elem) => elem)) {
      return null;
    }
  }
  return 'Not satisfying the min. requirements.';
};

/**
 * Gets the validators chosen for a particular case together with the associated formData.
 * @param validators - object with keys of formData as keys and validator function as their values
 * @param formData - formData to be checked by the validators
 * @returns {0|{}} - found validation errors or null if no errors found
 */
const validateFields = (validators, formData) => {
  const errorsFound = {};
  Object.entries(validators).forEach(([formKey, validateFuncArr]) => {
    let validationResult;
    for (const validate of validateFuncArr) {
      validationResult = validate(formData[formKey]);
      if (validationResult) {
        errorsFound[formKey] = validationResult;
        break;
      }
    }
  });
  return Object.keys(errorsFound).length && errorsFound;
};

export default validateFields;

/*
 This file contains different types of Grid settings used by project's components
 that take advantage of Material UI's Grid component.
*/

// Grid settings for listings of PersonCards
export const membersGridSettings = {
  xs: 6,
  sm: 4,
  md: 3,
  lg: 3,
  xl: 2,
  columns: 12,
  spacing: 1,
};

// Grid settings for listings of EventCards if the page also has a filter box on a side
export const eventWithFiltersGridSettings = {
  xs: 6,
  sm: 4,
  md: 6,
  lg: 4,
  xl: 3,
  xxl: 2,
  columns: 12,
  spacing: 1,
};

// Grid settings for listings of BranchCards
export const branchesGridSettings = {
  xs: 12,
  sm: 6,
  md: 6,
  lg: 4,
  xl: 4,
  xxl: 3,
  columns: 12,
  spacing: 3,
};

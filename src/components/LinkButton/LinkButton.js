import React from 'react';
import PropTypes from 'prop-types';

import 'components/LinkButton/LinkButton.css';

/**
 __LinkButton__ is a piece of text that acts as a button but is formatted like a link.
 It is implemented because links should not handle click actions normally.
*/
function LinkButton({
  id, className, onClick, title, children, disabled,
}) {
  let buttonClass = 'link-button';
  buttonClass += className ? ` ${className}` : '';
  return (
    <button
      type="button"
      disabled={disabled}
      className={buttonClass}
      id={id}
      title={title}
      onClick={onClick}
    >
      {children}
    </button>
  );
}

LinkButton.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
  ]).isRequired,
  id: PropTypes.string,
  /**
   Used for accessibility purposes.
  */
  title: PropTypes.string,
  className: PropTypes.string,
  onClick: PropTypes.func.isRequired,
  disabled: PropTypes.bool,
};

LinkButton.defaultProps = {
  className: '',
  id: null,
  disabled: false,
  title: null,
};

export default LinkButton;

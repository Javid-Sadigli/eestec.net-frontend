/* eslint-disable camelcase */
import React from 'react';
import PropTypes from 'prop-types';
import { Card } from 'components/Card/Card';
import ButtonLink from 'components/ButtonLink/ButtonLink';

import 'components/PersonCard/PersonCard.css';
import personPlaceholder from 'assets/img/member-placeholder.jpg';

/**
 __PersonCard__ is a variant of [`Card`](./?path=/docs/components-cards-card)
 which is used for describing people - their names, positions and pictures.
 */
function PersonCard({ item, buttonText, customCardButton }) {
  const {
    profile_photo_path,
    first_name,
    last_name,
    position,
    slug,
    applicationId,
    pivot,
  } = item;
  const isInBoard = pivot?.branch_role_id === 1;

  let classes = 'person-card';
  classes += position ? ' with-position' : '';

  const CustomCardButtonElement = customCardButton?.element;
  let customCardButtonExtraActions;
  if (customCardButton?.props?.extraActions) {
    const onClickArgument = (applicationId && !pivot) ? [applicationId, slug] : slug;
    if (isInBoard) {
      customCardButtonExtraActions = customCardButton.props.extraActions.filter(
        (extraAction) => !extraAction.isAddToBoard,
      );
    } else {
      customCardButtonExtraActions = customCardButton.props.extraActions;
    }
    customCardButtonExtraActions = customCardButtonExtraActions.map((extraAction) => ({
      text: extraAction.text,
      onClick: extraAction.onClick
        ? (callbackFunction) => {
          extraAction.onClick(onClickArgument, callbackFunction);
        } : undefined,
      Icon: extraAction.Icon,
    }));
  }

  return (
    <Card
      image={profile_photo_path || personPlaceholder}
      title={`${first_name} ${last_name}`}
      className={classes}
      customCardButton={customCardButton ? (
        <CustomCardButtonElement
          label={customCardButton.props?.label}
          mainAction={{
            text: customCardButton.props?.mainAction.text,
            component: customCardButton.props?.mainAction?.component,
            Icon: customCardButton.props?.mainAction?.Icon,
            onClick: customCardButton.props?.mainAction?.onClick
              ? (callbackFunction) => {
                customCardButton.props.mainAction.onClick(slug, callbackFunction);
              } : undefined,
            to: customCardButton.props?.mainAction?.component ? `/users/${slug}` : undefined,
          }}
          extraActions={customCardButtonExtraActions}
        />
      ) : null}
      button={{
        to: `/users/${slug}`,
        replace: false,
        text: buttonText,
        type: ButtonLink,
      }}
    >
      {position && (
        <p>{position}</p>
      )}
    </Card>
  );
}

PersonCard.propTypes = {
  /**
   An object containing all the necessary information for a person to be displayed as a PersonCard.
   If `profile_photo_path` is not specified, a default placeholder picture is used.
   This object contains also additional information like e.g. `applicationId` or `pivot` that
   is used for managing members by administrators of entities.
   */
  item: PropTypes.shape({
    first_name: PropTypes.string,
    last_name: PropTypes.string,
    position: PropTypes.string,
    profile_photo_path: PropTypes.string,
    slug: PropTypes.string,
    applicationId: PropTypes.number,
    pivot: PropTypes.shape({
      branch_id: PropTypes.number,
      user_id: PropTypes.number,
      branch_role_id: PropTypes.number,
    }),
  }).isRequired,
  buttonText: PropTypes.string,
  /**
   If the default [`Button`](/?path=/docs/components-button) should be
   replaced with another type of a button, with `customCardButton` we can specify
   an elementType of such a custom button and properties that this button should receive.

   For now the properties shape is constructed in a way that fits
   [`SplitButton`](/?path=/docs/components-button--splitbutton).
   In the future it can be modified.
   */
  customCardButton: PropTypes.shape({
    element: PropTypes.elementType.isRequired,
    props: PropTypes.shape({
      label: PropTypes.string,
      mainAction: PropTypes.shape({
        text: PropTypes.string,
        component: PropTypes.elementType,
        onClick: PropTypes.func,
        Icon: PropTypes.elementType,
      }).isRequired,
      extraActions: PropTypes.arrayOf(PropTypes.shape({
        text: PropTypes.string,
        onClick: PropTypes.func,
        Icon: PropTypes.elementType,
      })),
    }),
  }),
};

PersonCard.defaultProps = {
  buttonText: 'View profile',
  customCardButton: null,
};

export default PersonCard;

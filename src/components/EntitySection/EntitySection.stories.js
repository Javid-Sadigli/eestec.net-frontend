import React from 'react';
import EntitySection from 'components/EntitySection/EntitySection';
import { headingVariants } from 'components/Heading/Heading';
import CardList from 'components/CardList/CardList';
import { cardListItemMapper, loremIpsum } from '../../../.storybook/helpers';

export default {
  title: 'Components/Entity components/EntitySection',
  component: EntitySection,
  argTypes: {
    headingVariant: {
      options: headingVariants,
    },
    headingStyleVariant: {
      options: headingVariants,
    },
    children: {
      control: false,
    },
    innerRef: {
      control: false,
    },
  },
};

function Template(args) {
  return (
    <EntitySection {...args} />
  );
}

export const Empty = Template.bind({});
Empty.args = {
  title: 'Empty entity section',
};

export const WithText = Template.bind({});
WithText.args = {
  title: 'Entity section with text',
  children: loremIpsum,
};

export const WithCardList = Template.bind({});
WithCardList.args = {
  title: 'Entity section with a Card list',
  children: (
    <CardList
      loading={false}
      emptyMessage="There are no cards here"
      items={[...Array(8).keys()].map(cardListItemMapper.Card)}
    />
  ),
};

export const Nested = Template.bind({});
Nested.args = {
  title: 'Entity Section',
  nested: true,
  children: (
    <EntitySection title="Nested entity section" headingVariant="h2">
      {loremIpsum}
    </EntitySection>
  ),
};

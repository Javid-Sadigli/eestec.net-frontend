import React from 'react';
import PropTypes from 'prop-types';
import Heading, { headingVariants } from 'components/Heading/Heading';

import './EntitySection.css';

/**
 __EntitySection__ is a section of an entity page (e.g. Branch Page, Event Page, Profile Page).
 It is used to easily wrap different sections of the page.

 __Note:__ You can nest entity sections within each other to achieve e.g. different types
 of events presented on a certain page.
 */
function EntitySection({
  className,
  headingVariant,
  title,
  children,
  innerRef,
  visibilityCondition,
  nested,
  headingClassName,
  headingStyleVariant,
  headingLineAfter,
}) {
  if (!visibilityCondition) {
    return null;
  }

  let headingClasses = 'capitalize-first-letter';
  headingClasses += nested ? ' with-subheading' : '';
  headingClasses += headingClassName ? ` ${headingClassName}` : '';

  return (
    <div className={className} ref={innerRef}>
      <Heading
        variant={headingVariant}
        styleVariant={headingStyleVariant}
        lineAfter={headingVariant === 'h1' || headingLineAfter}
        className={headingClasses}
      >
        {title}
      </Heading>
      {children}
    </div>
  );
}

EntitySection.propTypes = {
  className: PropTypes.string,
  /**
   HTML variant of the section's heading - from `h1` to `h6` depending on the meaning.
  */
  headingVariant: PropTypes.oneOf(headingVariants),
  title: PropTypes.string.isRequired,
  /**
   The actual content of the entity section. It can be a node such as e.g.
   a [`CardList`](/?path=/story/components-cards-cardlist) or even another `EntitySection`,
   or it can be just a regular text.
  */
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.string]).isRequired,
  /**
   A ref that is passed to the actual `input` HTML element.
  */
  innerRef: PropTypes.shape({ current: PropTypes.instanceOf(Element) }),
  /**
   A condition can be specified to hide the component if its not needed.
   It is used e.g. for entities' events lists if a certain type of events is empty for
   the entity.
  */
  visibilityCondition: PropTypes.bool,
  /**
   Used to manage the spacings between headings of potential nested entity sections.
   This is generally NOT the best way to handle it.

   TODO: _Replace `nested` with pure CSS (tip: use :has() pseudo selector)_
  */
  nested: PropTypes.bool,
  headingClassName: PropTypes.string,
  /**
   If the looks of the section's heading should be different from the chosen `variant`, the proper
   styling should be set using this property.
  */
  headingStyleVariant: PropTypes.oneOf(headingVariants),
  /**
   __Note__: The line-after is always present for the `h1` section headings regardless of this prop.
  */
  headingLineAfter: PropTypes.bool,
};

EntitySection.defaultProps = {
  className: 'entity-section',
  headingVariant: 'h1',
  innerRef: null,
  visibilityCondition: true,
  nested: false,
  headingClassName: null,
  headingStyleVariant: null,
  headingLineAfter: false,
};

export default EntitySection;

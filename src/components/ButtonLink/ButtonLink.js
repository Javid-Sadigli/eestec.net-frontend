import React, { useState } from 'react';
import CircularProgress from '@mui/material/CircularProgress';
import PropTypes from 'prop-types';
import { createRipple } from 'components/Button/Button';
import { Link } from 'react-router-dom';

import 'components/Button/Button.css';

/**
 __ButtonLink__ is a variant of [Button](./?path=/docs/components-button) that looks
 exactly the same but works like a link, redirects instead of doing any click action.

 It can receive the same kinds of `className` in order to get the proper style, it also
 supports [Material UI Icons](https://mui.com/material-ui/material-icons/).
 */
function ButtonLink({
  icon, to, replace, children, className, id,
}) {
  const [horizontalSpinnerPadding, setHorizontalSpinnerPadding] = useState({});
  const [isLoading, setIsLoading] = useState(false);

  const handleClick = (e) => {
    setIsLoading(true);
    createRipple(e);
    setHorizontalSpinnerPadding({
      paddingLeft: `${(e.target.clientWidth - 37) / 2}px`,
      paddingRight: `${(e.target.clientWidth - 37) / 2}px`,
    });
  };

  const Icon = icon;
  const iconToRender = icon && <Icon sx={{ fontSize: '17px', verticalAlign: 'bottom' }} />;

  let classes = 'button-link';
  classes += className ? ` ${className}` : '';

  return isLoading ? (
    <CircularProgress size={37} sx={horizontalSpinnerPadding} className={className} />
  ) : (
    <Link
      to={to}
      replace={replace}
      onClick={handleClick}
      className={classes}
      id={id}
    >
      {children}
      {iconToRender}
    </Link>
  );
}

ButtonLink.propTypes = {
  /**
   Classes for implemented variants are i.a. `"primary-button"`,
   `"secondary-button"`, `"inverted"`, `"round"`.
   */
  className: PropTypes.string,
  /**
   The path that the button should redirect to.
  */
  to: PropTypes.string.isRequired,
  /**
   If `true`, the current path will be replaced in the history with the new one.
  */
  replace: PropTypes.bool,
  /**
   An imported element type of a
   [Material UI Icon](https://mui.com/material-ui/material-icons/).
   */
  icon: PropTypes.elementType,
  /**
   Can be a string text or an element<br />(e.g. `<span>...</span>`)
   */
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
  ]),
  id: PropTypes.string,
};

ButtonLink.defaultProps = {
  className: null,
  icon: null,
  children: null,
  id: null,
  replace: null,
};

export default ButtonLink;

import React from 'react';
import { withRouter } from 'storybook-addon-react-router-v6';
import ButtonLink from './ButtonLink';

export default {
  title: 'Components/Link variants/ButtonLink',
  component: ButtonLink,
  decorators: [withRouter],
  argTypes: {
    icon: {
      control: false,
    },
    to: {
      control: false,
    },
  },
};

export function Default(args) {
  return <ButtonLink {...args} />;
}
Default.args = {
  to: '/?path=/story/components-linkvariants-buttonlink',
  children: 'Button link',
  className: 'primary-button',
};

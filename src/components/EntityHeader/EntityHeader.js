import React from 'react';
import PropTypes from 'prop-types';

import './EntityHeader.css';

/**
 __EntityHeader__ is the top part of an entity page (e.g. Branch Page, Event Page, Profile Page).
 It is used to present the most important information about the entity and display
 a background picture.
*/
function EntityHeader({
  blurred, backgroundImage, title, subtitle, detailsFields, className, children,
}) {
  let classes = 'entity-header';
  classes += blurred ? '' : ' blurred';
  classes += className ? ` ${className}` : '';

  let headingClasses = null;
  if (title?.length >= 35) {
    headingClasses = 'entity-long-title';
  }

  return (
    <div className="entity-header-wrapper">
      <div
        className={classes}
        style={backgroundImage ? { backgroundImage: `url(${backgroundImage})` } : null}
      >
        {title && (
          <div className="entity-header-titles">
            <h1 className={headingClasses}>{title}</h1>
            {subtitle && <p>{subtitle}</p>}
          </div>
        )}
        {detailsFields.length > 0 && (
          <div className="entity-header-details-wrapper">
            {detailsFields
              .filter((detail) => detail?.value)
              .map((detail) => (
                <div key={detail.title}>
                  <p>{detail.title}</p>
                  <p>{detail.value}</p>
                </div>
              ))}
          </div>
        )}
        {children && (
          <div className="entity-header-children-wrapper">
            {children}
          </div>
        )}
      </div>
    </div>
  );
}

EntityHeader.propTypes = {
  /**
   Most often used to blur the header while data is still loading.

   TODO 1: _Actually this should be negated as now the header is blurry when the value is false._

   TODO 2: _Replace it with skeleton?_
  */
  blurred: PropTypes.bool,
  /**
   If `null`, a placeholder is used.
  */
  backgroundImage: PropTypes.string,
  title: PropTypes.string,
  className: PropTypes.string,
  subtitle: PropTypes.string,
  /**
   An array of objects, details of which are being rendered in the bottom-right corner of the
   entity header.
  */
  detailsFields: PropTypes.arrayOf(PropTypes.shape({
    title: PropTypes.string,
    value: PropTypes.oneOfType([
      PropTypes.string,
      PropTypes.number,
      PropTypes.node,
    ]),
  })),
  /**
   Children are displayed in the top-right corner of the entity header.
   For now only used to display buttons to edit the entities' pictures.
  */
  children: PropTypes.node,
};

EntityHeader.defaultProps = {
  title: null,
  blurred: false,
  backgroundImage: null,
  subtitle: null,
  detailsFields: [],
  className: null,
  children: null,
};

export default EntityHeader;

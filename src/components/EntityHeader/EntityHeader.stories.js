import React from 'react';
import EntityHeader from 'components/EntityHeader/EntityHeader';
import ImageFileInputComponent from 'components/ImageFileInput/ImageFileInput';
import VrpanoIcon from '@mui/icons-material/Vrpano';

import 'views/public/Event/Event.css';
import 'views/public/Branch/Branch.css';
import 'views/internal/Profile/Profile.css';

const typeMapper = {
  Person: 'person-header',
  Branch: 'branch-header',
  Event: 'event-header',
};

const childrenMapper = {
  ImageFileInput: (
    <ImageFileInputComponent
      // eslint-disable-next-line no-console
      onChange={(file) => console.log(file)}
      title="ImageFileInput's title"
      disabled={false}
      Icon={VrpanoIcon}
      isLoading={false}
    />
  ),
  None: null,
  Text: (
    <span>
      This is children text.
    </span>
  ),
};

export default {
  title: 'Components/Entity components/EntityHeader',
  component: EntityHeader,
  decorators: [(Story) => (
    <div style={{ width: '90%', margin: '2rem auto' }}>
      {Story()}
    </div>
  )],
  parameters: {
    layout: 'fullscreen',
  },
  args: {
    blurred: true,
  },
  argTypes: {
    children: {
      options: ['None', 'ImageFileInput', 'Text'],
      defaultValue: 'None',
      control: { type: 'radio' },
    },
    backgroundPlaceholderType: {
      options: ['Person', 'Branch', 'Event'],
      control: { type: 'radio' },
      defaultValue: 'Person',
      table: {
        disable: true,
      },
    },
  },
};

function Template({
  backgroundPlaceholderType,
  className,
  children,
  ...args
}) {
  let classes = typeMapper[backgroundPlaceholderType];
  classes += className ? ` ${className}` : '';
  return (
    <EntityHeader {...args} className={classes}>
      {childrenMapper[children]}
    </EntityHeader>
  );
}

export const Person = Template.bind({});
Person.args = {
  title: 'John Doe',
  backgroundPlaceholderType: 'Person',
};

export const Branch = Template.bind({});
Branch.args = {
  title: 'LC Waikiki',
  subtitle: 'Local Committee',
  backgroundPlaceholderType: 'Branch',
  detailsFields: [
    {
      title: 'Founded in',
      value: '1999',
    },
    {
      title: 'Number of members',
      value: '2137',
    },
  ],
};

export const Event = Template.bind({});
Event.args = {
  title: 'IT Sprint',
  subtitle: 'Operational Event',
  backgroundPlaceholderType: 'Event',
  detailsFields: [
    {
      title: 'Organizer',
      value: 'LC Waikiki',
    },
    {
      title: 'Dates',
      value: '01.01.2022 - 08.01.2022',
    },
  ],
};

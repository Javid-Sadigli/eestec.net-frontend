/* eslint-disable camelcase */
import React from 'react';
import LocationOnIcon from '@mui/icons-material/LocationOn';
import CalendarTodayIcon from '@mui/icons-material/CalendarToday';
import PropTypes from 'prop-types';
import { format } from 'date-fns';
import { Card } from 'components/Card/Card';
import ButtonLink from 'components/ButtonLink/ButtonLink';

import 'components/EventCard/EventCard.css';
import entityPlaceholder from 'assets/img/entity-placeholder.jpg';

/**
 __EventCard__ is a variant of [`Card`](./?path=/docs/components-cards-card)
 which is used for describing an event - its name, dates, location and picture.
 */
export default function EventCard({ item, buttonText, customCardButton }) {
  const {
    name, start_date, location, profile_picture_path, end_date, slug, branches,
  } = item;

  const CustomCardButtonElement = customCardButton?.element;
  let customCardButtonExtraActions;
  if (customCardButton?.props?.extraActions) {
    customCardButtonExtraActions = customCardButton.props?.extraActions.map((extraAction) => ({
      text: extraAction.text,
      onClick: (callbackFunction) => extraAction.onClick(slug, callbackFunction),
      Icon: extraAction.Icon,
    }));
  }

  const locationContent = location || branches?.map((branch) => branch.name).join(' & ');
  return (
    <Card
      image={profile_picture_path || entityPlaceholder}
      title={name}
      className="event-card"
      customCardButton={customCardButton ? (
        <CustomCardButtonElement
          label={customCardButton.props?.label}
          mainAction={{
            text: customCardButton.props?.mainAction.text,
            component: customCardButton.props?.mainAction?.component,
            Icon: customCardButton.props?.mainAction?.Icon,
            onClick: customCardButton.props?.mainAction?.onClick
              ? (callbackFunction) => {
                customCardButton.props.mainAction.onClick(slug, callbackFunction);
              } : undefined,
            to: customCardButton.props?.mainAction?.component ? `/events/${slug}` : undefined,
          }}
          extraActions={customCardButtonExtraActions}
        />
      ) : null}
      button={{
        to: `/events/${slug}`,
        replace: false,
        text: buttonText,
        type: ButtonLink,
      }}
    >
      <p className="caption-icon">
        <CalendarTodayIcon />
        <span>
          {format(new Date(start_date), 'dd.MM.yyyy')}
          {' '}
          -
          {' '}
          {format(new Date(end_date), 'dd.MM.yyyy')}
        </span>
      </p>
      <p className="caption-icon">
        {locationContent && (
        <>
          <LocationOnIcon />
          <span className="capitalized">{locationContent}</span>
        </>
        )}
      </p>
    </Card>
  );
}

EventCard.propTypes = {
  /**
   An object containing all the necessary information for an event to be displayed as
   an `EventCard`. If `profile_picture_path` is not specified, a default placeholder
   picture is used. If `location` is not specified, all the locations from the branches
   array are put together into one string and displayed as a location of an event.
   */
  item: PropTypes.shape({
    name: PropTypes.string,
    slug: PropTypes.string,
    location: PropTypes.string,
    start_date: PropTypes.string,
    end_date: PropTypes.string,
    profile_picture_path: PropTypes.string,
    // eslint-disable-next-line react/forbid-prop-types
    branches: PropTypes.array,
  }).isRequired,
  buttonText: PropTypes.string,
  /**
   If the default [`Button`](/?path=/docs/components-button) should be
   replaced with another type of a button, with `customCardButton` we can specify
   an elementType of such a custom button and properties that this button should receive.

   For now the properties shape is constructed in a way that fits
   [`SplitButton`](/?path=/docs/components-button--splitbutton).
   In the future it can be modified.
   */
  customCardButton: PropTypes.shape({
    element: PropTypes.elementType.isRequired,
    props: PropTypes.shape({
      label: PropTypes.string,
      mainAction: PropTypes.shape({
        text: PropTypes.string,
        component: PropTypes.elementType,
        onClick: PropTypes.func,
        Icon: PropTypes.elementType,
      }).isRequired,
      extraActions: PropTypes.arrayOf(PropTypes.shape({
        text: PropTypes.string,
        component: PropTypes.elementType,
        onClick: PropTypes.func,
        Icon: PropTypes.elementType,
      })),
    }),
  }),
};

EventCard.defaultProps = {
  buttonText: 'Read more',
  customCardButton: null,
};

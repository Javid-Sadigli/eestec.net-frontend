/* eslint-disable no-alert */
import React from 'react';
import EventCard from 'components/EventCard/EventCard';
import { withRouter } from 'storybook-addon-react-router-v6';
import { splitButtonMockedProps } from '../../../.storybook/helpers';

export default {
  title: 'Components/Cards/EventCard',
  component: EventCard,
  decorators: [withRouter],
  args: {
    item: {
      name: 'IT Sprint',
      slug: 'it-sprint',
      location: 'LC Waikiki',
      start_date: '2022-01-01',
      end_date: '2022-01-02',
    },
  },
  argTypes: {
    customCardButton: {
      options: ['None', 'SplitButton'],
      control: { type: 'radio' },
      defaultValue: 'None',
    },
  },
};

function Template({ customCardButton, ...args }) {
  return (
    <EventCard
      {...args}
      customCardButton={customCardButton === 'None' ? null : splitButtonMockedProps}
    />
  );
}

export const Default = Template.bind({});

/* eslint-disable react/forbid-prop-types */
import React from 'react';
import PropTypes from 'prop-types';
import Button from 'components/Button/Button';
import TextInput from 'components/TextInput/TextInput';

import './ConditionalInput.css';

const changeFunctions = new Map([
  [Button, 'onClick'],
  [TextInput, 'onValueChange'],
  // build this map while adding new components that make use of ConditionalInput
]);

/**
 __ConditionalInput__ is a component that depending on a condition is displayed in a static
 non-editable way or is an input that can be manipulated by a user.

 It usually comes together with `EditIcons` that handle the conditions.

 __NOTE:__ This component has a map `changeFunctions`
 (`Component -> functionChangeValue`) in the source code.
 Whenever some new types of buttons or inputs should be added conditionally with this component
 that map has to be updated, otherwise it won't work with the components not specified
 within the map.
 */
function ConditionalInput({
  content,
  editableContent,
  onChange,
  condition,
  inputComponent,
  staticComponent,
  className,
  isAllowedToEdit,
  isInputDisabled,
}) {
  let Element;
  let elementProperties;
  let elementContent;
  if (condition && isAllowedToEdit) {
    Element = inputComponent.element;
    elementProperties = {
      ...inputComponent.props,
      value: editableContent,
      [changeFunctions.get(Element) || 'onChange']: onChange,
      disabled: isInputDisabled,
    };
    elementContent = null;
  } else {
    Element = staticComponent.element;
    elementProperties = {
      ...staticComponent.props,
    };
    elementContent = content;
  }

  let elementClasses = 'conditional-input';
  let classes = 'conditional-input-wrapper';
  elementClasses += elementProperties.className ? ` ${elementProperties.className}` : '';
  classes += className ? ` ${className}` : '';

  return (
    <div className={classes}>
      <Element {...elementProperties} className={elementClasses}>
        {elementContent}
      </Element>
    </div>
  );
}

ConditionalInput.propTypes = {
  /**
   The content of the static non-editable version of the component.
   As a rule of thumb, it should be the same as `editableContent` yet the props are here
   separately in case `content` has to be wrapped into some nodes or split into multiple paragraphs.
  */
  content: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  /**
   The content of the editable version of the component.
   As a rule of thumb, it should be the same as `content` yet the props are here
   separately in case `content` has to be wrapped into some nodes or split into multiple paragraphs.
   */
  editableContent: PropTypes.string,
  onChange: PropTypes.func.isRequired,
  /**
   A boolean value indicating whether the `inputComponent` should be displayed
   (if `condition` set to `true`) or `staticComponent` should be displayed
   (if `condition` set to `false`).
  */
  condition: PropTypes.bool,
  /**
   The `elementType` and properties of the component that should be displayed if the
   condition is set to `true`.
  */
  inputComponent: PropTypes.shape({
    element: PropTypes.oneOfType([
      PropTypes.func, PropTypes.element, PropTypes.elementType,
    ]).isRequired,
    props: PropTypes.object,
  }).isRequired,
  /**
   The `elementType` and properties of the component that should be displayed if the
   condition is set to `false`.
   */
  staticComponent: PropTypes.shape({
    element: PropTypes.oneOfType([
      PropTypes.func, PropTypes.element, PropTypes.elementType,
    ]).isRequired,
    props: PropTypes.object,
  }).isRequired,
  className: PropTypes.string,
  /**
   An additional layer of sanity/security - disable editing always if this is set to `false`.
  */
  isAllowedToEdit: PropTypes.bool,
  isInputDisabled: PropTypes.bool,
};

ConditionalInput.defaultProps = {
  className: null,
  isAllowedToEdit: false,
  condition: null,
  content: null,
  editableContent: null,
  isInputDisabled: false,
};

export default ConditionalInput;

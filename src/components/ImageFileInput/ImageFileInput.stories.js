import React from 'react';
import ImageFileInput from 'components/ImageFileInput/ImageFileInput';
import VrpanoIcon from '@mui/icons-material/Vrpano';

export default {
  title: 'Components/ImageFileInput',
  component: ImageFileInput,
  argTypes: {
    Icon: { control: false },
  },
};

function Template(args) {
  return <ImageFileInput {...args} />;
}

export const Default = Template.bind({});
Default.args = {
  // eslint-disable-next-line no-console
  onChange: (e) => console.log(e),
  Icon: VrpanoIcon,
};

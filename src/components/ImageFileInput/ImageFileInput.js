import React from 'react';
import PropTypes from 'prop-types';
import Tooltip from '@mui/material/Tooltip';
import IconButton from '@mui/material/IconButton';
import { CircularProgress } from '@mui/material';

import './ImageFileInput.css';

/**
 __ImageFileInput__ is a button serving as an input of type `file`.
 It is displayed in a form of an circular icon and accepts PNG, JPG, SVG and GIF
 image formats.

 In the future this component can be extended if needed - to accept other types of files
 depending on the given properties.
 */
function ImageFileInput({
  title, Icon, className, disabled, onChange, isLoading,
}) {
  let classes = 'picture-change-button';
  classes += className ? ` ${className}` : '';

  const handleChange = (e) => {
    if (e.target.files.length > 0) {
      onChange(e.target.files[0]);
    }
  };

  const resetFileInput = (e) => {
    e.target.value = null;
  };

  return isLoading ? (
    <div className="image-file-input-loader">
      <CircularProgress size={32} />
    </div>
  ) : (
    <Tooltip title={title} arrow>
      <IconButton
        component="label"
        disabled={disabled}
        className={classes}
        onClick={resetFileInput}
      >
        <Icon />
        <input
          onChange={handleChange}
          type="file"
          hidden
          accept="image/png, image/jpeg, image/jpg, image/svg, image/gif"
        />
      </IconButton>
    </Tooltip>
  );
}

ImageFileInput.propTypes = {
  className: PropTypes.string,
  /**
   Displayed on hover as a tooltip.
  */
  title: PropTypes.string,
  /**
   An imported element type of a
   [Material UI Icon](https://mui.com/material-ui/material-icons/).
  */
  Icon: PropTypes.elementType.isRequired,
  disabled: PropTypes.bool,
  /**
   The component passes the chosen file as a parameter of the given function.
  */
  onChange: PropTypes.func.isRequired,
  isLoading: PropTypes.bool,
};

ImageFileInput.defaultProps = {
  className: null,
  title: 'Image input',
  disabled: false,
  isLoading: false,
};

export default ImageFileInput;

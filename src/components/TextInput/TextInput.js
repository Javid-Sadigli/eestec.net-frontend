import React, { useCallback, useState } from 'react';
import VisibilityIcon from '@mui/icons-material/Visibility';
import VisibilityOffIcon from '@mui/icons-material/VisibilityOff';
import PropTypes from 'prop-types';

import ErrorMessage from 'components/Message/components/ErrorMessage/ErrorMessage';
import ClickableIcon from 'components/ClickableIcon/ClickableIcon';
import { generateRandomId } from 'utils/dom';

import 'components/TextInput/TextInput.css';

/**
 __TextInput__ is the text input used in forms on the website.
 */
function TextInput({
  type = 'text',
  label,
  id = generateRandomId('text-input'),
  value,
  onValueChange,
  errorMessage,
  className,
  autoComplete,
  disabled,
  multiline,
  placeholder,
  innerRef,
}) {
  const [isPasswordVisible, setPasswordVisible] = useState(false);

  const handlePasswordVisibilityChange = useCallback(
    () => setPasswordVisible((prevState) => !prevState),
    [setPasswordVisible],
  );

  let passwordIcon = null;
  let inputInnerType = type;
  if (type === 'password') {
    passwordIcon = <VisibilityIcon className="visibility-icon" />;
    passwordIcon = (
      <ClickableIcon
        onClick={handlePasswordVisibilityChange}
        icon={isPasswordVisible ? VisibilityOffIcon : VisibilityIcon}
        className="visibility-icon"
        title="Show/Hide password"
      />
    );
    inputInnerType = isPasswordVisible ? 'text' : 'password';
  }

  let textInputClassName = `input-container ${type}`;
  textInputClassName += className ? ` ${className}` : '';
  textInputClassName += errorMessage ? ' with-error' : '';
  textInputClassName += errorMessage ? ' multiline' : '';

  const inputProps = {
    onChange: onValueChange,
    disabled,
    id,
    autoComplete,
    value,
    placeholder,
    ref: innerRef,
  };

  return (
    <div className={textInputClassName}>
      {multiline ? (
        <textarea
          {...inputProps}
        />
      ) : (
        <input
          {...inputProps}
          type={inputInnerType}
        />
      )}
      <label className={value && 'filled'} htmlFor={id}>
        {label}
      </label>
      {passwordIcon}
      {errorMessage && <ErrorMessage>{errorMessage}</ErrorMessage>}
    </div>
  );
}

TextInput.propTypes = {
  /**
   One of the HTML text input types as specified
   [here](https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input).
   */
  type: PropTypes.string,
  /**
   Used for indicating what TextInput is for.
   */
  label: PropTypes.string,
  /**
   A value that should be displayed as input.
   */
  value: PropTypes.string.isRequired,
  /**
   A functioned that should be called on each input change.
   */
  onValueChange: PropTypes.func.isRequired,
  errorMessage: PropTypes.string,
  className: PropTypes.string,
  // eslint-disable-next-line react/require-default-props
  id: PropTypes.string,
  /**
   See [here](https://developer.mozilla.org/en-US/docs/Web/HTML/Attributes/autocomplete).
   */
  autoComplete: PropTypes.string,
  disabled: PropTypes.bool,
  /**
   A boolean value that indicates if the input should allow line-breaks
   */
  multiline: PropTypes.bool,
  /**
   __Note:__ Should not be used with the default styling of an input as `label`
   works as a placeholder in such a case.
   */
  placeholder: PropTypes.string,
  /**
   A ref that is passed to the actual `input` HTML element.
   */
  innerRef: PropTypes.shape({ current: PropTypes.instanceOf(Element) }),
};

TextInput.defaultProps = {
  type: 'text',
  errorMessage: null,
  className: null,
  autoComplete: null,
  disabled: false,
  label: null,
  multiline: false,
  placeholder: null,
  innerRef: null,
};

export default TextInput;

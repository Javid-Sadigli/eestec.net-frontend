import React from 'react';
import TextInput from 'components/TextInput/TextInput';
import { useArgs } from '@storybook/addons';

export default {
  title: 'Components/TextInput',
  component: TextInput,
  args: {
    value: '',
  },
  argTypes: {
    ref: {
      control: false,
    },
  },
};

function Template({ value, ...args }) {
  // eslint-disable-next-line no-unused-vars
  const [_, updateArgs] = useArgs();
  return (
    <TextInput
      {...args}
      value={value}
      onValueChange={(e) => updateArgs({ value: e.target.value })}
    />
  );
}

export const Default = Template.bind({});
Default.args = {
  label: 'Default input',
};

export const Password = Template.bind({});
Password.args = {
  label: 'Password input',
  type: 'password',
};

export const WithError = Template.bind({});
WithError.args = {
  label: 'Input with error',
  errorMessage: 'Something went wrong!',
};

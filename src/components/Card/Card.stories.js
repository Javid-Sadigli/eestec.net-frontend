import React from 'react';
import { Card } from 'components/Card/Card';
import placeholderImg from 'assets/img/entity-placeholder.jpg';
import Button from 'components/Button/Button';

export default {
  title: 'Components/Cards/Card',
  component: Card,
  argTypes: {
    children: {
      control: { type: 'text' },
    },
    customCardButton: {
      control: false,
    },
    button: {
      options: ['Without', 'With'],
      control: { type: 'radio' },
      defaultValue: 'Without',
    },
  },
};

const buttonProps = {
  type: Button,
  // eslint-disable-next-line no-alert
  onClick: () => alert('You clicked a card button!'),
  text: 'Button',
};

function Template({ button, ...args }) {
  return <Card {...args} button={button === 'With' ? buttonProps : null} />;
}

export const Default = Template.bind({});
Default.args = {
  image: placeholderImg,
  title: 'Default Card',
};

export const WithSubtitle = Template.bind({});
WithSubtitle.args = {
  image: placeholderImg,
  title: 'Card with a subtitle',
  children: <p style={{ marginTop: 0 }}>Default Card&apos;s subtitle</p>,
};

export const WithButton = Template.bind({});
WithButton.args = {
  image: placeholderImg,
  title: 'Card with a button',
  button: 'With',
};

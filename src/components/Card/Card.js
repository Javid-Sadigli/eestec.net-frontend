import React from 'react';
import PropTypes from 'prop-types';
import Button from 'components/Button/Button';

import 'components/Card/Card.css';

/**
 __Card__ is the default version of a card in the UI, not connected to any particular
 type. It serves as an interface for other cards like e.g.
 [`EventCard`](./?path=/docs/components-cards-eventcard) or
 [`PersonCard`](./?path=/docs/components-cards-personcard).
*/
export function Card({
  image, title, children, button, className, customCardButton,
}) {
  let classes = 'card';
  classes += className ? ` ${className}` : '';

  let buttonContent = null;
  if (customCardButton) {
    buttonContent = customCardButton;
  } else if (button) {
    const ButtonToRender = button?.type || Button;
    buttonContent = (
      <ButtonToRender
        className="card-button primary-button"
        onClick={button?.onClick}
        to={button?.to}
        replace={button?.replace}
      >
        {button.text}
      </ButtonToRender>
    );
  }

  return (
    <div className={classes}>
      <img className="card-image" src={image} alt={title} />
      <div className="caption">
        <div className="caption-details">
          <h3 className="caption-title">{title}</h3>
          {children && (
            <div className="caption-details-text">
              {children}
            </div>
          )}
        </div>
        {buttonContent}
      </div>
    </div>
  );
}

Card.propTypes = {
  className: PropTypes.string,
  image: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  /**
   Children of the Card element appear under its title. They can include additional
   details of a card or a subtitle for the card. It can be any JSX node.
  */
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.string]),
  button: PropTypes.oneOfType([
    PropTypes.shape({
      type: PropTypes.elementType,
      onClick: PropTypes.func.isRequired,
      text: PropTypes.string.isRequired,
    }),
    PropTypes.shape({
      type: PropTypes.elementType,
      to: PropTypes.string.isRequired,
      replace: PropTypes.bool,
      text: PropTypes.string.isRequired,
    }),
  ]),
  /**
   A node of an element that should be displayed instead of the regular `Button`.
   As a matter of fact, this can be any node, yet as a convention it is used by
   the `EventCard`, `PersonCard` and others to define their own buttons if needed
   (e.g. `SplitButton`) and render Card passing the node as `customCardButton`.
  */
  customCardButton: PropTypes.node,
};

Card.defaultProps = {
  button: null,
  className: null,
  children: null,
  customCardButton: null,
};

export default function CardWrapper({ item, ...props }) {
  return <Card image={item.image} title={item.title} {...props} />;
}

CardWrapper.propTypes = {
  item: PropTypes.shape({
    image: PropTypes.string,
    title: PropTypes.string,
  }).isRequired,
};

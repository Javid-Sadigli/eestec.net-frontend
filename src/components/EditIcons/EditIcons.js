import React from 'react';
import PropTypes from 'prop-types';
import IconButton from '@mui/material/IconButton';
import EditIcon from '@mui/icons-material/Edit';
import ClearIcon from '@mui/icons-material/Clear';
import CheckIcon from '@mui/icons-material/Check';

import './EditIcons.css';
import { CircularProgress } from '@mui/material';

/**
 __EditIcons__ are buttons that are used together with `ConditionalInput` and
 handle the input's actions: condition for the input visibility, saving the input data,
 canceling the input.

 TODO: Maybe this component can be used inside `ConditionalInput` and with the use of
 React portals and passed proprties it could be rendered in the places we want.
*/
function EditIcons({
  className,
  onEditStart,
  onEditCancel,
  onEditSubmit,
  isBeingEdited,
  isLoading,
  shouldDisplay,
  shouldBeVisible,
}) {
  if (!shouldDisplay) {
    return null;
  }
  let classes = 'edit-icons-wrapper';
  classes += className ? ` ${className}` : '';
  classes += isBeingEdited ? ' editing' : '';
  return (
    <div className={classes}>
      {isBeingEdited ? (
        <>
          <IconButton
            onClick={onEditSubmit}
            disabled={isLoading}
            title="Submit edit"
          >
            {isLoading ? <CircularProgress size={24} /> : <CheckIcon />}
          </IconButton>
          <IconButton
            onClick={onEditCancel}
            disabled={isLoading}
            title="Cancel editing"
          >
            <ClearIcon />
          </IconButton>
        </>
      ) : (
        <IconButton
          onClick={onEditStart}
          disabled={isLoading}
          title="Edit information"
          className={shouldBeVisible ? null : 'hidden'}
        >
          <EditIcon />
        </IconButton>
      )}
    </div>
  );
}

EditIcons.propTypes = {
  className: PropTypes.string,
  onEditStart: PropTypes.func.isRequired,
  onEditCancel: PropTypes.func.isRequired,
  onEditSubmit: PropTypes.func.isRequired,
  isBeingEdited: PropTypes.bool,
  isLoading: PropTypes.bool,
  shouldDisplay: PropTypes.bool,
  shouldBeVisible: PropTypes.bool,
};

EditIcons.defaultProps = {
  shouldDisplay: true,
  isLoading: false,
  className: null,
  isBeingEdited: false,
  shouldBeVisible: true,
};

export default EditIcons;

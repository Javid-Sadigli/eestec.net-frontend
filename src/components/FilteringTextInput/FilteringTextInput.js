import React from 'react';
import PropTypes from 'prop-types';
import SearchIcon from '@mui/icons-material/Search';

import './FilteringTextInput.css';

/**
 __FilteringTextInput__ is a variant of a text input that is used in places where filters
 of various sorts are applied, therefore it serves a different purpose than the regular
 [`TextInput`](./?path=/docs/components-textinput).

 */
function FilteringTextInput({
  id,
  value,
  onChange,
  className,
  title,
  placeholder,
}) {
  let classes = 'filtering-text-input';
  classes += className ? ` ${className}` : '';

  return (
    <div className="filtering-text-input-container">
      <input
        className={classes}
        title={title}
        type="text"
        value={value}
        onChange={onChange}
        id={id}
        placeholder={placeholder}
      />
      <SearchIcon />
    </div>
  );
}

FilteringTextInput.propTypes = {
  /**
   A value that should be displayed as input.
  */
  value: PropTypes.string.isRequired,
  /**
   Functions passed here should be used together with hooks
   `useDynamicSearch` or `useDebouncedSearch` to serve filtering.
  */
  onChange: PropTypes.func.isRequired,
  className: PropTypes.string,
  id: PropTypes.string,
  /**
   Used for indicating what TextInput is for (accessibility purposes).
  */
  title: PropTypes.string,
  /**
   Used for indicating what TextInput is for.
  */
  placeholder: PropTypes.string,
};

FilteringTextInput.defaultProps = {
  className: null,
  id: null,
  placeholder: null,
  title: 'Filter search input',
};

export default FilteringTextInput;

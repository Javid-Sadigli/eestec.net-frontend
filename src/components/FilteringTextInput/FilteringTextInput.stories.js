import React, { useState } from 'react';
import FilteringTextInput from 'components/FilteringTextInput/FilteringTextInput';
import useDynamicSearch from 'hooks/useDynamicSearch';

export default {
  title: 'Components/FilteringTextInput',
  component: FilteringTextInput,
  argTypes: {
    value: { control: false },
  },
  parameters: {
    docs: {
      source: {
        type: 'code',
      },
    },
  },
};

const exampleElements = [
  'One',
  'Two',
  'Three',
  'Four',
  'Five',
  'Six',
];

export function Default(args) {
  const [elements] = useState(exampleElements); // ['One', 'Two', 'Three', 'Four', 'Five', 'Six']
  const [filteredElements, setFilteredElements] = useState(exampleElements);

  function filterElements(query) {
    setFilteredElements(elements.filter(
      (element) => element.toLowerCase().includes(query.trim().toLowerCase()),
    ));
  }

  const [filterTerm, setFilterTerm] = useDynamicSearch({
    onFilteredSearchSubmit: filterElements,
  });

  return (
    <div
      style={{
        minHeight: '200px',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'flex-start',
      }}
    >
      <FilteringTextInput
        {...args}
        onChange={(e) => setFilterTerm(e.target.value)}
        value={filterTerm}
      />
      {!filteredElements.length ? (
        <p style={{ textAlign: 'center', opacity: 0.5 }}>Nothing to display :(</p>
      ) : (
        <ul>
          {filteredElements.map((element) => <li key={element}>{element}</li>)}
        </ul>
      )}
    </div>
  );
}
Default.args = {
  title: 'Example',
  placeholder: 'Example',
};

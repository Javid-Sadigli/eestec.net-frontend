import React from 'react';
import PropTypes from 'prop-types';
import { useAutocomplete } from '@mui/material';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ErrorMessage from 'components/Message/components/ErrorMessage/ErrorMessage';
import { generateRandomId, isBeyondScreen } from 'utils/dom';

import 'components/Autocomplete/Autocomplete.css';
import 'components/TextInput/TextInput.css';

/**
 __Autocomplete__ is a dropdown element that allows the user to select one item from a list
 and filter the options by a substring.

 For a regular dropdown component
 check [`Dropdown`](./?path=/docs/components-dropdown).
 */
function Autocomplete({
  options,
  optionLabel,
  label,
  id = generateRandomId('autocomplete'),
  errorMessage,
  className,
  onChange,
}) {
  const {
    getRootProps,
    getInputLabelProps,
    getInputProps,
    getListboxProps,
    getOptionProps,
    groupedOptions,
  } = useAutocomplete({
    id,
    options,
    getOptionLabel: (option) => option[optionLabel],
    onChange: (_, option) => onChange(option),
  });

  const inputProps = getInputProps();
  const containerClassName = `autocomplete-container${inputProps.ref.current
  && isBeyondScreen(inputProps.ref.current, Math.min(302, options.length * 53 + 1)) ? ' beyond-screen' : ''}`;

  return (
    <div className={containerClassName + (className ? ` ${className}` : '')}>
      <div {...getRootProps()} className={`input-container${errorMessage ? ' with-error' : ''}`}>
        <input {...inputProps} />
        <label {...getInputLabelProps()} className={inputProps.value && 'filled'} htmlFor={id}>
          {label}
        </label>
        <ExpandMoreIcon sx={{ fontSize: '27px' }} className="expand-icon" />
        {errorMessage && <ErrorMessage>{errorMessage}</ErrorMessage>}
      </div>
      <ul {...getListboxProps()}>
        {groupedOptions.map((option, index) => (
          <li {...getOptionProps({ option, index })}><span>{option[optionLabel]}</span></li>
        ))}
      </ul>
    </div>
  );
}

Autocomplete.propTypes = {
  /**
   An array of Objects, each of which contains i.a. a property used for distinguishing options.
  */
  options: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.object])).isRequired,
  /**
   A string representing the name of the key which points into the object's unique name.
  */
  optionLabel: PropTypes.string.isRequired,
  /**
   Used for indicating what Autocomplete is for.
  */
  label: PropTypes.string.isRequired,
  errorMessage: PropTypes.string,
  className: PropTypes.string,
  /**
   A function that receives the chosen object (as a whole, not only its label) as an only argument.
  */
  onChange: PropTypes.func.isRequired,
  // eslint-disable-next-line react/require-default-props
  id: PropTypes.string,
};

Autocomplete.defaultProps = {
  errorMessage: null,
  className: null,
};

export default Autocomplete;

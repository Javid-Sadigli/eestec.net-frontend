import React from 'react';
import Autocomplete from './Autocomplete';

export default {
  title: 'Components/Autocomplete',
  component: Autocomplete,
  decorators: [(Story) => (
    <div style={{ height: '350px', width: '300px' }}>
      {Story()}
    </div>
  )],
};

function Template(args) {
  return <Autocomplete {...args} />;
}

const templateBasicArgs = {
  label: 'Choose a potato',
  optionLabel: 'name',
  // eslint-disable-next-line no-console
  onChange: (option) => console.log(option),
  options: [
    {
      key: 'rotten-potato',
      name: 'Rotten Potato',
    },
    {
      key: 'mashed-potato',
      name: 'Mashed Potato',
    },
    {
      key: 'boiled-potato',
      name: 'Boiled Potato',
    },
    {
      key: 'sweet-potato',
      name: 'Sweet Potato',
    },
    {
      key: 'regular-potato',
      name: 'Regular Potato',
    },
    {
      key: 'fries',
      name: 'Fries',
    },
    {
      key: 'baked-potato',
      name: 'Baked Potato',
    },
  ],
};

export const Basic = Template.bind({});
Basic.args = templateBasicArgs;

export const WithError = Template.bind({});
WithError.args = {
  ...templateBasicArgs,
  errorMessage: 'You chose a wrong potato',
};

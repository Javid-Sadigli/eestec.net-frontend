import React from 'react';
import Message from 'components/Message/Message';
import ErrorMessageComponent from 'components/Message/components/ErrorMessage/ErrorMessage';
import SuccessMessageComponent from 'components/Message/components/SuccessMessage/SuccessMessage';
import WarningMessageComponent from 'components/Message/components/WarningMessage/WarningMessage';
import BoltIcon from '@mui/icons-material/Bolt';

export default {
  title: 'Components/Message',
  component: Message,
  subcomponents: {
    WarningMessage: WarningMessageComponent,
    ErrorMessage: ErrorMessageComponent,
    SuccessMessage: SuccessMessageComponent,
  },
  argTypes: {
    Icon: { control: false },
  },
};

function Template(args) {
  return <Message {...args} />;
}

export const Default = Template.bind({});
Default.args = {
  Icon: BoltIcon,
  children: 'This is a message',
};

export function ErrorMessage(args) {
  return <ErrorMessageComponent {...args} />;
}
ErrorMessage.args = {
  children: 'This is an error message',
};

export function WarningMessage(args) {
  return <WarningMessageComponent {...args} />;
}
WarningMessage.args = {
  children: 'This is a warning message',
};

export function SuccessMessage(args) {
  return <SuccessMessageComponent {...args} />;
}
SuccessMessage.args = {
  children: 'This is a success message',
};

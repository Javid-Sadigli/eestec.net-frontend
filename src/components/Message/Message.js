import React from 'react';
import PropTypes from 'prop-types';

import 'components/Message/Message.css';

/**
 __Message__ is a small UI element designed to inform users about the status of a certain
 action or element. To simplify the usage in most of the cases
 `ErrorMessage`, `WarningMessage` and `SuccessMessage` should be used.
*/
function Message({ children, className, Icon }) {
  let classes = 'small message';
  classes += (className ? ` ${className}` : '');
  return (
    <p className={classes}>
      <Icon />
      <span>{children}</span>
    </p>
  );
}

Message.propTypes = {
  /**
   The text content of a message.
   */
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
  className: PropTypes.string,
  /**
   An imported element type of a
   [Material UI Icon](https://mui.com/material-ui/material-icons/).
   */
  Icon: PropTypes.elementType,
};

Message.defaultProps = {
  className: null,
  Icon: null,
};

export default Message;

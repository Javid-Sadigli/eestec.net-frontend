import React from 'react';
import DoneIcon from '@mui/icons-material/Done';
import PropTypes from 'prop-types';
import Message from 'components/Message/Message';

import 'components/Message/components/SuccessMessage/SuccessMessage.css';

function SuccessMessage({ children, className }) {
  let classes = 'success-message';
  classes += className ? ` ${className}` : '';
  return (
    <Message className={classes} Icon={DoneIcon}>
      {children}
    </Message>
  );
}

SuccessMessage.propTypes = {
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
  className: PropTypes.string,
};

SuccessMessage.defaultProps = {
  className: null,
};

export default SuccessMessage;

import React from 'react';
import PriorityHighIcon from '@mui/icons-material/PriorityHigh';
import PropTypes from 'prop-types';
import Message from 'components/Message/Message';

import 'components/Message/components/ErrorMessage/ErrorMessage.css';

function ErrorMessage({ children, className }) {
  let classes = 'error-message';
  classes += (className ? ` ${className}` : '');
  return (
    <Message className={classes} Icon={PriorityHighIcon}>
      {children}
    </Message>
  );
}

ErrorMessage.propTypes = {
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
  className: PropTypes.string,
};

ErrorMessage.defaultProps = {
  className: null,
};

export default ErrorMessage;

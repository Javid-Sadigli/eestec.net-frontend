import React from 'react';
import WarningIcon from '@mui/icons-material/Warning';
import PropTypes from 'prop-types';
import Message from 'components/Message/Message';

import 'components/Message/components/WarningMessage/WarningMessage.css';

function WarningMessage({ children, className }) {
  let classes = 'warning-message';
  classes += (className ? ` ${className}` : '');
  return (
    <Message className={classes} Icon={WarningIcon}>
      {children}
    </Message>
  );
}

WarningMessage.propTypes = {
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
  className: PropTypes.string,
};

WarningMessage.defaultProps = {
  className: null,
};

export default WarningMessage;

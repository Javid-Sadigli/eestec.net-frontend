import React, { useState, useRef } from 'react';
import Button from '@mui/material/Button';
import ButtonGroup from '@mui/material/ButtonGroup';
import ArrowDropDownIcon from '@mui/icons-material/ArrowDropDown';
import ClickAwayListener from '@mui/material/ClickAwayListener';
import Grow from '@mui/material/Grow';
import Paper from '@mui/material/Paper';
import Popper from '@mui/material/Popper';
import MenuItem from '@mui/material/MenuItem';
import MenuList from '@mui/material/MenuList';
import PropTypes from 'prop-types';
import { CircularProgress } from '@mui/material';

import './SplitButton.css';

/**
 __SplitButton__ is a variant of `Button` that comes with an expandable menu
 that can be used for extra button actions (used e.g. for managing members
 by administrators).
 */
function SplitButton({
  mainAction, extraActions, label, className,
}) {
  const [open, setOpen] = useState(false);
  const [isLoading, setIsLoading] = useState(false);
  const anchorRef = useRef(null);

  const disableLoading = () => setIsLoading(false);

  const handleClick = (clickAction) => {
    setIsLoading(true);
    setOpen(false);
    if (clickAction) {
      clickAction(disableLoading);
    }
  };

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }
    setOpen(false);
  };

  let classes = 'split-button';
  classes += className ? ` ${className}` : '';

  return (
    <div className={classes}>
      <ButtonGroup
        variant="contained"
        ref={anchorRef}
        aria-label={label}
        className="button-group"
      >
        <Button
          onClick={() => handleClick(mainAction.onClick)}
          disabled={isLoading}
          className="split-inner-button split-main-button"
          component={mainAction.component}
          to={mainAction.component ? mainAction.to : undefined}
        >
          <span>{mainAction.text}</span>
          {isLoading && <CircularProgress size={20} />}
        </Button>
        <Button
          size="small"
          aria-controls={open ? 'split-button-menu' : undefined}
          aria-expanded={open ? 'true' : undefined}
          aria-label={`${label} - select option`}
          aria-haspopup="menu"
          onClick={handleToggle}
          disabled={isLoading}
          className="split-inner-button split-secondary-button"
        >
          <ArrowDropDownIcon />
        </Button>
      </ButtonGroup>
      <Popper
        open={open}
        anchorEl={anchorRef.current}
        role={undefined}
        transition
        disablePortal
      >
        {({ TransitionProps, placement }) => (
          <Grow
            {...TransitionProps}
            style={{
              transformOrigin:
                placement === 'bottom' ? 'center top' : 'center bottom',
            }}
          >
            <Paper>
              <ClickAwayListener onClickAway={handleClose}>
                <MenuList id="split-button-menu" autoFocusItem>
                  {extraActions.map(({
                    text, onClick, Icon, component, to,
                  }) => (
                    <MenuItem
                      key={text}
                      onClick={onClick ? () => handleClick(onClick) : undefined}
                      component={component}
                      to={component ? to : undefined}
                    >
                      {Icon && <Icon />}
                      <span>{text}</span>
                    </MenuItem>
                  ))}
                </MenuList>
              </ClickAwayListener>
            </Paper>
          </Grow>
        )}
      </Popper>
    </div>
  );
}

SplitButton.propTypes = {
  /**
   Properties of the main button (the one which is not in the expanded menu).

   Properties description:
   <span className="with-indent">`onClick` is a function that gets executed
   when the user clicks on the button (not working if `Link` is used as the `component` prop
   (__note:__ this component passes a callback function as the parameter - this
   callback function disables the loading mode of the split button when executed,</span>
   <span className="with-indent">`text` is text content of the button,</span>
   <span className="with-indent">`Icon` is an imported element type of a
   [Material UI Icon](https://mui.com/material-ui/material-icons/),</span>
   <span className="with-indent">`component` is the component that should be used instead of
   a regular button (the only use case is using react-router's `Link` here),</span>
   <span className="with-indent">`to` is the URL that the action should redirect the user to
   if `Link` was specified as the `component` prop.</span>
 */
  mainAction: PropTypes.shape({
    onClick: PropTypes.func,
    text: PropTypes.string.isRequired,
    Icon: PropTypes.elementType,
    component: PropTypes.elementType,
    to: PropTypes.string,
  }).isRequired,
  /**
   An array of properties describing the actions of the buttons
   that are in the split button's expandable menu.

   A single property description:
   <span className="with-indent">`onClick` is a function that gets executed
   when the user clicks on the button (not working if `Link` is used as the `component` prop
   (__note:__ this component passes a callback function as the parameter - this
   callback function disables the loading mode of the split button when executed,</span>
   <span className="with-indent">`text` is text content of the button,</span>
   <span className="with-indent">`Icon` is an imported element type of a
   [Material UI Icon](https://mui.com/material-ui/material-icons/),</span>
   <span className="with-indent">`component` is the component that should be used instead of
   a regular button (the only use case is using react-router's `Link` here),</span>
   <span className="with-indent">`to` is the URL that the action should redirect the user to
   if `Link` was specified as the `component` prop.</span>
   */
  extraActions: PropTypes.arrayOf(PropTypes.shape({
    onClick: PropTypes.func.isRequired,
    text: PropTypes.string.isRequired,
    Icon: PropTypes.elementType,
    component: PropTypes.elementType,
    to: PropTypes.string,
  })).isRequired,
  /**
   Used for accessibility purposes.
  */
  label: PropTypes.string,
  className: PropTypes.string,
};

SplitButton.defaultProps = {
  label: 'split button',
  className: null,
};

export default SplitButton;

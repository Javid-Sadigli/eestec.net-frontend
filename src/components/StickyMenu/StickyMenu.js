import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import StickyMenuElement, { stickyMenuElementPropTypes } from 'components/StickyMenu/components/StickyMenuElement';
import useIsPinned from 'hooks/useIsPinned';

import './StickyMenu.css';

/**
 __StickyMenu__ is a menu appearing at the top of a parent container. It has a sticky position.

 The sticky menu can have n levels of depth, based on which the end height of the menu is
 calculated. Additionally a `rightElement` can be provided which is being displayed on the
 right side of the sticky menu - it can be any node, in most of the cases it would be
 a button or a piece of text.

 `StickyMenu` works recursively. If the `elements` of the StickyMenu have any `subElements`
 specified, further `StickyMenuElement` components are being rendered as children of their parents.

 For now `StickyMenu` is used on the entity pages: `Profile`, `Branch`, `Event`.

 __NOTE:__ In the current implementation the `StickyMenu` elements are not displayed at all if there
 is only one element present which does not have at least 2 `subElements`. This is to avoid
 redundancy, as such a situation in the UI does not require a menu to navigate with.

 __NOTE 2:__ On mobile screens, only the first level of elements is displayed.
*/
function StickyMenu({ elements, depth, rightElement }) {
  const stickyMenuRef = useRef();
  const isPinned = useIsPinned(stickyMenuRef);
  const shouldNotDisplayElements = elements.length === 1
    && elements?.[0]?.subElements?.length < 2;
  let classes = 'sticky-menu';
  classes += !isPinned ? ' pinned' : '';
  classes += rightElement ? ' with-right-element' : '';

  const rightElementComponent = (
    <div className="sticky-menu-right-element">
      {rightElement || null}
    </div>
  );

  let height = null;
  if (shouldNotDisplayElements) {
    if (rightElement) {
      classes += ' only-right-element';
    } else {
      classes += ' sticky-menu-invisible';
    }
  } else {
    height = depth * 40 + 8;
  }

  return (
    <div
      className={classes}
      style={height ? { height } : null}
      ref={stickyMenuRef}
    >
      {!shouldNotDisplayElements && (
        <div className="sticky-menu-elements">
          {elements
            /*
             if the subElements should be there but the array is empty,
             do not display the element since that means that there is no data to be shown
             in such a section (e.g. Members section that has no subcategories)
            */
            .filter((element) => !(element.subElements && !element.subElements.length))
            .map((element) => (
              <StickyMenuElement
                title={element.title}
                target={element.target}
                subElements={element.subElements}
                key={element.title}
                scrollOffset={height + 10}
              />
            ))}
        </div>
      )}
      {rightElementComponent}
    </div>
  );
}

StickyMenu.propTypes = {
  /**
   Elements of the menu. See the `StickyMenuElement` tab for more information about their props.
  */
  elements: PropTypes.arrayOf(PropTypes.shape(stickyMenuElementPropTypes)).isRequired,
  /**
   The maximum depth of the menu being the maximum number of subelements,
   starting the count from the root node. E.g.
   <span className="with-indent">if among `elements` there are no
   objects with a `subElements` non-empty array, the depth is `1`,</span>
   <span className="with-indent">if among `elements` there is at least one
   object with a `subElements` non-empty array, the depth is at
   least `2` (depending if the `subElements` have their own sub elements).</span>

   FUTURE: _It can be thought through whether this prop should be calculated automatically or not._
  */
  depth: PropTypes.number,
  /**
   Any node that should appear on the right side of the sticky menu.
   For now the only use case is [`Button`](./?path=/docs/components-button)
  */
  rightElement: PropTypes.node,
};

StickyMenu.defaultProps = {
  depth: 2,
  rightElement: null,
};

export default StickyMenu;

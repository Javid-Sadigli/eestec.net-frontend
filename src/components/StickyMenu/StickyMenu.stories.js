import React, { useRef } from 'react';
import StickyMenuElement from 'components/StickyMenu/components/StickyMenuElement';
import StickyMenu from 'components/StickyMenu/StickyMenu';
import Heading from 'components/Heading/Heading';
import HeadlinedContent from 'components/HeadlinedContent/HeadlinedContent';
import Button from 'components/Button/Button';
import { loremIpsum } from '../../../.storybook/helpers';

export default {
  title: 'Components/StickyMenu',
  component: StickyMenu,
  subcomponents: { StickyMenuElement },
  parameters: {
    layout: 'fullscreen',
  },
  argTypes: {
    elements: { control: false },
    rightElement: { control: false },
  },
};

function Template({ depth, rightElement }) {
  const section1Ref = useRef(null);
  const section2Ref = useRef(null);
  const section3Ref = useRef(null);
  const section2PartARef = useRef(null);
  const section2PartBRef = useRef(null);
  const section3PartAARef = useRef(null);

  const elements = [
    {
      title: 'Section 1',
      target: section1Ref,
    },
    {
      title: 'Section 2',
      target: section2Ref,
      subElements: [
        {
          title: 'Part A',
          target: section2PartARef,
        },
        {
          title: 'Part B',
          target: section2PartBRef,
        },
      ],
    },
    {
      title: 'Section 3',
      target: section3Ref,
      subElements: [
        {
          title: 'Part AA',
          target: section3PartAARef,
        },
      ],
    },
  ];

  return (
    <div
      style={{
        width: '100%',
        height: '100%',
        boxSizing: 'border-box',
      }}
      className="sticky-menu-example"
    >
      <StickyMenu
        depth={depth}
        rightElement={rightElement}
        elements={elements}
      />
      <HeadlinedContent title="Section 1" variant="h1" fullWidthText innerRef={section1Ref}>
        <p>{loremIpsum}</p>
      </HeadlinedContent>
      <Heading variant="h1" innerRef={section2Ref} lineAfter>
        Section 2
      </Heading>
      <HeadlinedContent title="Part A" variant="h2" fullWidthText innerRef={section2PartARef}>
        <p>{loremIpsum}</p>
      </HeadlinedContent>
      <HeadlinedContent title="Part B" variant="h2" fullWidthText innerRef={section2PartBRef}>
        <p>{loremIpsum}</p>
      </HeadlinedContent>
      <Heading variant="h1" innerRef={section3Ref} lineAfter>
        Section 3
      </Heading>
      <HeadlinedContent title="Part AA" variant="h2" fullWidthText innerRef={section3PartAARef}>
        {[...Array(12).keys()].map((num) => (
          <p key={`paragraph-${num}`}>{loremIpsum}</p>
        ))}
      </HeadlinedContent>
    </div>
  );
}

export const Default = Template.bind({});

export const WithRightElement = Template.bind({});
WithRightElement.args = {
  rightElement: (
    <Button
      // eslint-disable-next-line no-alert
      onClick={() => alert('You clicked the right element button!')}
      className="primary-button"
    >
      Right button
    </Button>
  ),
};

import React from 'react';
import PropTypes from 'prop-types';
import scroll from 'utils/scroll';
import LinkButton from 'components/LinkButton/LinkButton';

/**
 __StickyMenuElement__ is a component rendered by `StickyMenu`, representing a single element (link)
 that scrolls to the `target` and can contain `subElements` that scroll to sub sections of that
 element.
 */
function StickyMenuElement({
  title, target, subElements, depth, scrollOffset,
}) {
  const scrollToTarget = () => scroll(
    target,
    null,
    scrollOffset,
    1000,
    'easeOutBack',
    1,
  );
  let classes = 'sticky-menu-element';
  if (depth > 0) {
    classes += ` sticky-menu-element-depth-${depth}`;
  }
  return (
    <div className={classes}>
      <LinkButton onClick={scrollToTarget} className="sticky-menu-element-title">
        {title}
      </LinkButton>
      <div className="sticky-menu-subelements">
        {subElements.map((subElement) => (
          <StickyMenuElement
            title={subElement.title}
            target={subElement.target}
            key={subElement.title}
            subElements={subElement.subElements}
            depth={depth + 1}
            scrollOffset={scrollOffset}
          />
        ))}
      </div>
    </div>
  );
}

export const stickyMenuElementPropTypes = {
  /**
   The title of the link.
  */
  title: PropTypes.string.isRequired,
  /**
   A `ref` to the target node that the link should scroll to.
  */
  target: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.shape({ current: PropTypes.instanceOf(Element) }),
  ]).isRequired,
  /**
   An array of `StickyMenuElement` objects.
  */
  // eslint-disable-next-line react/forbid-prop-types
  subElements: PropTypes.array,
  /**
   The current level of depth of the `StickyMenu` that this particular element is a part of.
   Used to properly assign a `font-size` to the element.
  */
  depth: PropTypes.number,
  /**
   Since `StickyMenu` is... sticky, it takes some space on the top of the screen.
   This property adds offset to the scroll, so that this extra space can be taken into consideration
   while scrolling and so that the top part of the target node is not covered by the sticky menu.
  */
  scrollOffset: PropTypes.number,
};

StickyMenuElement.propTypes = stickyMenuElementPropTypes;

StickyMenuElement.defaultProps = {
  subElements: [],
  depth: 0,
  scrollOffset: 0,
};

export default StickyMenuElement;

import React from 'react';
import RadioButton from 'components/RadioButton/RadioButton';
import { useState } from '@storybook/addons';

export default {
  title: 'Components/RadioButton',
  component: RadioButton,
  argTypes: {
    checked: { control: false },
    label: { control: false },
    id: { control: false },
  },
  parameters: {
    docs: {
      source: {
        type: 'code',
      },
    },
  },
};

export function Default(args) {
  const [chosenOption, setChosenOption] = useState({
    name: 'Option 1',
    slug: 'option-1',
  });
  const items = [...Array(5).keys()].map((num) => ({
    name: `Option ${num + 1}`,
    slug: `option-${num + 1}`,
  }));
  return items.map((item) => (
    <RadioButton
      {...args}
      label={item.name}
      key={item.slug}
      id={`${item.slug}-radio-button`}
      checked={item.slug === chosenOption.slug}
      onChange={() => {
        if (chosenOption.slug !== item.slug) {
          setChosenOption(item);
        }
      }}
    />
  ));
}

import React from 'react';
import PropTypes from 'prop-types';
import { generateRandomId } from 'utils/dom';

import './RadioButton.css';

/**
 __RadioButton__ is an element that allows the users to make
 a single selection of one of the options.

 __FUTURE:__ This component can be expanded into a more extensive component
 like `RadioButtonGroup` which would render all the radio buttons for all the given options
 and handle all logic of checking the radio button, unchecking others itd. internally.
*/
function RadioButton({
  checked, label, onChange, className, id = generateRandomId('radio-button'),
}) {
  return (
    <div className={`radio-button-container ${className}`}>
      <label htmlFor={id}>
        <input type="radio" checked={checked} id={id} onChange={onChange} />
        <span className="radio-button" />
        <span className="radio-button-label">{label}</span>
      </label>
    </div>
  );
}

RadioButton.propTypes = {
  label: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  checked: PropTypes.bool,
  className: PropTypes.string,
  // eslint-disable-next-line react/require-default-props
  id: PropTypes.string,
};
RadioButton.defaultProps = {
  checked: false,
  className: '',
};

export default RadioButton;

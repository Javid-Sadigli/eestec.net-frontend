import React, { useState } from 'react';
import PropTypes from 'prop-types';
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';
import ErrorMessage from 'components/Message/components/ErrorMessage/ErrorMessage';
import { generateRandomId } from 'utils/dom';
import { ClickAwayListener } from '@mui/material';

import 'components/Dropdown/Dropdown.css';

/**
 __Dropdown__ is a component which allows to select one of given options.
 It accepts objects as parameters and let the developer specify which property of the objects
 is to be used as the one presented in the dropdown.

 For a `Dropdown` enhanced with the capability of filtering options via substring
 check [`Autocomplete`](./?path=/docs/components-autocomplete).
*/
function Dropdown({
  options,
  optionLabel,
  optionKey,
  label,
  onSelectClick,
  disabled,
  id = generateRandomId('dropdown'),
  errorMessage,
  isSkeleton,
  className,
}) {
  const [isOpen, setIsOpen] = useState(false);

  let dropdownClass = 'select-wrapper';
  dropdownClass += className ? ` ${className}` : '';
  dropdownClass += errorMessage ? ' with-error' : '';
  dropdownClass += isSkeleton ? ' skeleton skeleton-dropdown' : '';

  const renderOptionButtons = () => (
    <>
      <ClickAwayListener onClickAway={() => setIsOpen(false)}>
        <button
          id={id}
          type="button"
          className="select-button"
          onClick={() => setIsOpen((prevState) => !prevState)}
          aria-expanded={isOpen}
          disabled={disabled}
        >
          {isSkeleton ? <div className="dropdown-skeleton-filler" /> : (
            <>
              <span>{label}</span>
              <ExpandMoreIcon sx={{ fontSize: '27px' }} className={isOpen ? 'reverse-expand' : 'expand-icon'} />
            </>
          )}
        </button>
      </ClickAwayListener>
      <div className={isOpen ? 'option-container' : 'closed-options'}>
        {options.map((option, index) => (option[optionLabel] !== label
          ? (
            <button
              type="button"
              onClick={() => onSelectClick(option)}
              key={optionKey ? option[optionKey] : `${id}-${index}`}
              className={isOpen ? 'option-button' : 'stacked-option-button'}
            >
              {option[optionLabel]}
            </button>
          ) : null))}
      </div>
    </>
  );

  return (
    <div className={dropdownClass}>
      <div className="custom-select">
        {renderOptionButtons()}
      </div>
      {errorMessage && <ErrorMessage>{errorMessage}</ErrorMessage>}
    </div>
  );
}

Dropdown.propTypes = {
  /**
   The function that is being executed on a click of one of the options.
   It takes the whole option object as an argument, not just the label.
  */
  onSelectClick: PropTypes.func.isRequired,
  /**
   Objects that are the options of the dropdown.
  */
  options: PropTypes.arrayOf(PropTypes.oneOfType([PropTypes.object])).isRequired,
  /**
   The property of the objects within the `options` prop that is to be shown in the dropdown.
   E.g. if you have objects like `{ name: 'Option 1', slug: 'option-1' }` in the `options` prop,
   most probably you want to set `'name'` as the `optionLabel`.
  */
  optionLabel: PropTypes.string.isRequired,
  /**
   Used as the `key` React property when options are being mapped through and rendered.
   If empty, the index is used - it is certainly recommended to be specified.
   Can be the same as `optionLabel` (as probably the options you provide have unique labels).
  */
  optionKey: PropTypes.string,
  /**
   The label that is being displayed on the top of the dropdown (the only one visible when dropdown
   is not expanded).

   **NOTE:** To be able to serve different use cases, this label **does not** updates
   automatically, you need to handle it by yourself in the `onSelectClick` function.
  */
  label: PropTypes.string.isRequired,
  errorMessage: PropTypes.string,
  className: PropTypes.string,
  // eslint-disable-next-line react/require-default-props
  id: PropTypes.string,
  disabled: PropTypes.bool,
  /**
   Used for indicating whether the dropdown is in a loading state.
  */
  isSkeleton: PropTypes.bool,
};

Dropdown.defaultProps = {
  errorMessage: null,
  className: null,
  optionKey: '',
  disabled: false,
  isSkeleton: false,
};

export default Dropdown;

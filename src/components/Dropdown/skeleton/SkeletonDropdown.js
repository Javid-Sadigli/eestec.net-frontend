import React from 'react';
import Dropdown from 'components/Dropdown/Dropdown';

import 'skeleton.css';
import './SkeletonDropdown.css';

function SkeletonDropdown() {
  return (
    <Dropdown
      onSelectClick={() => {}}
      optionLabel=""
      options={[]}
      label=""
      isSkeleton
      disabled
    />
  );
}

export default SkeletonDropdown;

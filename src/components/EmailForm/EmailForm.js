import React, { useState } from 'react';
import PropTypes from 'prop-types';

import 'components/EmailForm/EmailForm.css';

function EmailForm({
  onSubmit,
  baseComponentClass,
  toEmailText,
  buttonText,
}) {
  const [formData, setFormData] = useState({
    email: '',
    message: '',
  });

  const formOnChangeHandler = (e) => {
    setFormData({ ...formData, [e.target.name]: e.target.value });
  };
  const onSubmitHandler = (e) => {
    e.preventDefault();

    onSubmit(e, formData);

    setFormData({ email: '', message: '' });
  };
  return (
    <form
      className={`${baseComponentClass}-form-area`}
      onSubmit={(e) => onSubmitHandler(e)}
    >
      <div className={`${baseComponentClass}-email`}>
        <input
          type="email"
          className={`${baseComponentClass}-form-input ${baseComponentClass}-form-email`}
          name="email"
          placeholder="Email*"
          value={formData.email}
          onChange={(e) => formOnChangeHandler(e)}
        />
        {toEmailText && (
          <p className={`${baseComponentClass}-form-to-text`}>
            To:
            {' '}
            {toEmailText}
          </p>
        )}
      </div>
      <div className={`${baseComponentClass}-message`}>
        <textarea
          className={`${baseComponentClass}-form-input ${baseComponentClass}-form-message`}
          name="message"
          placeholder="Message*"
          value={formData.message}
          onChange={(e) => formOnChangeHandler(e)}
        />
        <button type="submit" className={`${baseComponentClass}-form-submit`}>{buttonText}</button>
      </div>
    </form>
  );
}

EmailForm.propTypes = {
  onSubmit: PropTypes.func.isRequired,
  baseComponentClass: PropTypes.string,
  buttonText: PropTypes.string,
  toEmailText: PropTypes.string,
};

EmailForm.defaultProps = {
  buttonText: 'SEND',
  baseComponentClass: 'contact',
  toEmailText: null,
};

export default EmailForm;

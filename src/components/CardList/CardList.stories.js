import React from 'react';
import { withRouter } from 'storybook-addon-react-router-v6';
import EventCard from 'components/EventCard/EventCard';
import PersonCard from 'components/PersonCard/PersonCard';
import BranchCard from 'views/public/Branches/components/BranchCard/BranchCard';
import CardComponent from 'components/Card/Card';
import CardList from './CardList';
import { cardListItemMapper, splitButtonMockedProps } from '../../../.storybook/helpers';

const elementTypeMapper = {
  Card: CardComponent,
  EventCard,
  PersonCard,
  BranchCard,
};

export default {
  title: 'Components/Cards/CardList',
  component: CardList,
  decorators: [withRouter],
  argTypes: {
    items: {
      control: false,
    },
    emptyMessage: {
      defaultValue: 'No cards found!',
    },
    cardType: {
      options: ['Card', 'PersonCard', 'EventCard', 'BranchCard'],
      control: { type: 'radio' },
      defaultValue: 'Card',
    },
    loading: {
      defaultValue: false,
    },
    customCardButton: {
      options: ['None', 'SplitButton'],
      control: { type: 'radio' },
      defaultValue: 'None',
    },
    showMoreButton: {
      options: ['Disable', 'Enable'],
      control: { type: 'radio' },
      defaultValue: 'Disable',
    },
  },
};

function Template({
  cardType, customCardButton, showMoreButton, ...args
}) {
  const items = [...Array(8).keys()].map(cardListItemMapper[cardType]);
  return (
    <CardList
      {...args}
      items={items}
      cardType={elementTypeMapper[cardType]}
      customCardButton={
        (customCardButton === 'None' || cardType === 'Card')
          ? null : splitButtonMockedProps
      }
      showMoreButton={showMoreButton === 'Disable' ? null : {
        visibilityCondition: true,
        // eslint-disable-next-line no-alert
        onClick: () => alert('Now more cards would be loaded'),
        isLoading: false,
        text: 'Show more',
      }}
    />
  );
}

export const Stacked = Template.bind({});

export const Horizontal = Template.bind({});
Horizontal.args = {
  horizontal: true,
};

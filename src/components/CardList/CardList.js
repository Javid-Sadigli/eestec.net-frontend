import React from 'react';
import Grid from '@mui/material/Grid';
import PropTypes from 'prop-types';
import { CircularProgress } from '@mui/material';
// eslint-disable-next-line import/no-named-as-default
import Card from 'components/Card/Card';
import Button from 'components/Button/Button';
import Draggable from 'components/Draggable/Draggable';

import 'components/CardList/CardList.css';

/**
 __CardList__ is a listing of Card elements that can be seen e.g. on the Events page or
 on Profile pages. It accepts different types of Cards and places them in a Grid
 or in one line.
 */
export default function CardList({
  items,
  emptyMessage,
  className,
  loading,
  cardButtonsText,
  backgroundImg,
  cardType,
  showMoreButton,
  gridSettings,
  horizontal,
  customCardButton,
}) {
  const {
    columns, xs, sm, md, lg, xl, xxl, spacing,
  } = gridSettings;

  if (loading) {
    return (
      <div className="card-loader">
        <CircularProgress size={70} />
      </div>
    );
  }

  if (!items.length) {
    return (
      <p className="no-cards dimmed">
        {emptyMessage}
      </p>
    );
  }

  const CardToRender = cardType;

  const showMoreButtonContent = (showMoreButton?.visibilityCondition && !!items.length) ? (
    <div className="show-more-button-wrapper">
      <Button
        className="secondary-button white uppercase"
        onClick={showMoreButton.onClick}
        isLoading={showMoreButton.isLoading}
      >
        {showMoreButton.text}
      </Button>
    </div>
  ) : null;

  let classes = horizontal ? 'horizontal-cards-container' : 'cards-container';
  classes += className ? ` ${className}` : '';
  if (xxl) {
    classes += ` grid-xxl-${Math.floor((xxl / (columns || 12)) * 100)}`;
  }

  return (
    <>
      {backgroundImg && (
        <img src={backgroundImg.img} alt={backgroundImg.alt} className="background-icon" />
      )}
      {horizontal ? (
        <Draggable className={classes}>
          {items.map((item) => (
            <CardToRender
              item={item}
              buttonText={cardButtonsText}
              key={item.slug}
              customCardButton={customCardButton}
            />
          ))}
          {showMoreButtonContent}
        </Draggable>
      ) : (
        <>
          <Grid container columns={columns} spacing={spacing} className={classes}>
            {items.map((item) => (
              <Grid item xs={xs} sm={sm} md={md} lg={lg} xl={xl} key={item.slug}>
                <CardToRender
                  item={item}
                  buttonText={cardButtonsText}
                  customCardButton={customCardButton}
                />
              </Grid>
            ))}
          </Grid>
          {showMoreButtonContent}
        </>
      )}
    </>
  );
}

CardList.propTypes = {
  /**
   An array of items (objects) that should be presented as cards within the list.
   The structure of those objects differs depending on the `cardType`.
   */
  // eslint-disable-next-line react/forbid-prop-types
  items: PropTypes.array.isRequired,
  /**
   If loading is true, a spinner is displayed.
  */
  loading: PropTypes.bool.isRequired,
  className: PropTypes.string,
  /**
   A message to be displayed instead of cards if the length of `items` is 0.
  */
  emptyMessage: PropTypes.string.isRequired,
  /**
   The text to be displayed on the button of the cards.
  */
  cardButtonsText: PropTypes.string,
  /**
   An image to be displayed as a background between cards.
  */
  backgroundImg: PropTypes.shape({
    img: PropTypes.string.isRequired,
    alt: PropTypes.string.isRequired,
  }),
  /**
   An elementType indicating what type of a Card should a given list contain.
  */
  cardType: PropTypes.elementType,
  /**
   Properties describing the button that appears if the number of items exceeds
   the pagination limit. The pagination limit comes together with the data from
   the back end.

   Properties description:
   <span className="with-indent">`visibilityCondition` is a boolean value representing
   if more items are available to fetch,</span>
   <span className="with-indent">`onClick` is a function that gets more items,</span>
   <span className="with-indent">`isLoading` is a boolean value that indicated if the
   new items are still loading,</span>
   <span className="with-indent">`text` is text content of the button.</span>
  */
  showMoreButton: PropTypes.shape({
    visibilityCondition: PropTypes.bool.isRequired,
    onClick: PropTypes.func.isRequired,
    isLoading: PropTypes.bool.isRequired,
    text: PropTypes.string.isRequired,
  }),
  /**
   An object containing some of the configuration props of
   [Material UI Grid](https://mui.com/material-ui/react-grid/).
  */
  gridSettings: PropTypes.shape({
    xs: PropTypes.number,
    sm: PropTypes.number,
    md: PropTypes.number,
    lg: PropTypes.number,
    xl: PropTypes.number,
    xxl: PropTypes.number,
    columns: PropTypes.number,
    spacing: PropTypes.number,
  }),
  /**
   A boolean value that indicates if the cards should be presented as a grid and
   be wrapped (`false`) or should be placed in one line and be scrollable and draggable
  */
  horizontal: PropTypes.bool,
  /**
   If the default [`Button`](/?path=/docs/components-button) should be
   replaced with another type of a button, with `customCardButton` we can specify
   an elementType of such a custom button and properties that this button should receive.

   For now the properties shape is constructed in a way that fits
   [`SplitButton`](/?path=/docs/components-button--splitbutton).
   In the future it can be modified.

   __For now this property does not work with a regular `Card`, only with its expanded versions.__
   */
  customCardButton: PropTypes.shape({
    element: PropTypes.elementType.isRequired,
    props: PropTypes.shape({
      label: PropTypes.string,
      mainAction: PropTypes.shape({
        text: PropTypes.string,
        component: PropTypes.elementType,
        onClick: PropTypes.func,
        Icon: PropTypes.elementType,
      }).isRequired,
      extraActions: PropTypes.arrayOf(PropTypes.shape({
        text: PropTypes.string,
        component: PropTypes.elementType,
        onClick: PropTypes.func,
        Icon: PropTypes.elementType,
      })),
    }),
  }),
};

CardList.defaultProps = {
  backgroundImg: null,
  cardButtonsText: undefined,
  cardType: Card,
  showMoreButton: null,
  gridSettings: {
    xs: 6,
    sm: 4,
    md: 4,
    lg: 4,
    xl: 3,
    xxl: 2,
    columns: 12,
    spacing: 1,
  },
  horizontal: false,
  customCardButton: null,
  className: null,
};

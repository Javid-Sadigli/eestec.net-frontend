import React, { useState, memo } from 'react';
import {
  ZoomableGroup,
  ComposableMap,
  Geographies,
  Geography,
} from 'react-simple-maps';
import PropTypes from 'prop-types';
import ReactTooltip from 'react-tooltip';
import SeeMoreButton from 'components/SeeMoreButton/SeeMoreButton';
import './MapComponent.css';
import { CircularProgress } from '@mui/material';

const geoUrl = require('components/Map/data/eestec_non_yeeted_actually_no_Israel_unyeet_Kosovo.geojson');

const mapWidth = '100%';
const mapHeight = '100%';

const isKosovo = (name) => name === 'Kosovo';
const isSerbia = (name) => name === 'Serbia';

function MapChart({ topComponent, buttonRef, buttonTitle }) {
  const [tooltipContent, setTooltipContent] = useState('');
  const [geoLoaded, setGeoLoaded] = useState(false);

  // Display Kosovo as region not separate from Serbia
  // Because it's disputed and not universally recognized
  // And we don't want to embarrass ourselves
  const [hoverKosovo, setHoverKosovo] = useState(false);
  // Disable onClick panning
  const handleFilter = ({ constructor: { name } }) => name !== 'MouseEvent';

  return (
    <div className="map-chart-wrapper">
      {topComponent}
      {!geoLoaded && (
        <div className="hidden-loader">
          <CircularProgress size={110} />
        </div>
      )}
      <ComposableMap
        data-tip=""
        style={{ width: mapWidth, height: mapHeight }}
        projectionConfig={{
          scale: 950,
          rotation: [-11, 0, 0],
          center: [20, -230],
        }}
      >
        <ZoomableGroup
          filterZoomEvent={handleFilter}
          translateExtent={[[0, -mapHeight], [mapWidth, mapHeight]]}
        >
          <Geographies geography={geoUrl}>
            {({ geographies }) => {
              if (!geoLoaded) {
                setGeoLoaded(true);
              }
              return geographies.map((geo) => (
                <Geography
                  key={geo.rsmKey}
                  geography={geo}
                  onMouseEnter={() => {
                    const { NAME, BRANCHES } = geo.properties;
                    setTooltipContent(`${NAME} — ${BRANCHES} ${BRANCHES !== 1 ? 'branches' : 'branch'}`);
                    // if Kosovo, don't display number of branches
                    if (isKosovo(NAME)) {
                      setTooltipContent(NAME);
                    }
                    // if Serbia hover over Kosovo
                    if (isSerbia(NAME)) {
                      setHoverKosovo(true);
                    }
                  }}
                  onMouseLeave={() => {
                    setTooltipContent('');
                    if (hoverKosovo) {
                      setHoverKosovo(false);
                    }
                  }}
                  style={{
                    default: {
                      // change Kosovo color when Serbia is hovered
                      fill: hoverKosovo && isKosovo(geo.properties.NAME) ? 'var(--primary-red)' : '#D6D6DA',
                      outline: 'none',
                      stroke: 'var(--background-black)',
                      strokeDasharray: isKosovo(geo.properties.NAME) ? 6 : 0,
                      strokeWidth: isKosovo(geo.properties.NAME) ? 0.7 : 0.1,
                      transition: 'all 300ms',
                    },
                    hover: {
                      fill: geo.properties.BRANCHES ? 'var(--primary-red)' : 'darkgray',
                      outline: 'none',
                      transition: 'all 300ms',
                    },
                    pressed: {
                      fill: '#E42',
                      outline: 'none',
                    },
                  }}
                />
              ));
            }}
          </Geographies>
        </ZoomableGroup>
      </ComposableMap>
      <ReactTooltip>{ tooltipContent }</ReactTooltip>
      {buttonRef && (
        <SeeMoreButton
          target={buttonRef}
          label={buttonTitle}
        />
      )}
    </div>
  );
}

MapChart.propTypes = {
  topComponent: PropTypes.node,
  buttonRef: PropTypes.shape({ current: PropTypes.instanceOf(Element) }),
  buttonTitle: PropTypes.string,
};

MapChart.defaultProps = {
  topComponent: null,
  buttonRef: null,
  buttonTitle: null,
};

export default memo(MapChart);

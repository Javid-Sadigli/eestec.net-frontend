import React, { useContext } from 'react';
import Modal from 'components/Modal/Modal';
import Button from 'components/Button/Button';
import ModalContext from 'context/ModalContext';
import SingleContactBoard from 'views/public/Contact/components/ContactBoard/SingleContactBoard/SingleContactBoard';
import { CopyrightsModal } from 'hooks/useEntityEdit';
import personPlaceholder from 'assets/img/member-placeholder.jpg';
import ModalContextDecorator from '../../../.storybook/decorators/ModalContextDecorator';

export default {
  title: 'Components/Modal',
  component: Modal,
  subcomponents: { SingleContactBoard, CopyrightsModal },
  decorators: [ModalContextDecorator],
  parameters: {
    layout: 'fullscreen',
  },
  argTypes: {
    withWrapper: {
      table: {
        disable: true,
      },
    },
    modalContent: {
      defaultValue: 'Hey, I am the content of the modal!',
      description: 'The node that is to be put in the modal (set by the context\'s '
        + '`setModalContent` function. Examples of `modalContent` can be'
        + '`CopyrightsModal` or `SingleContactBoard`.',
      table: {
        type: { summary: 'node' },
        defaultValue: { summary: '' },
      },
      control: {
        type: 'text',
      },
    },
  },
};

function Template({ withWrapper, modalContent }) {
  const { setModalContent } = useContext(ModalContext);

  return (
    <Button
      onClick={() => setModalContent(
        withWrapper ? (
          <p
            style={{
              background: 'var(--primary-white)',
              padding: '5rem',
              borderRadius: '2rem',
              color: '#000',
            }}
          >
            {modalContent}
          </p>
        ) : modalContent,
      )}
      className="primary-button"
    >
      Open a modal
    </Button>
  );
}

export const Default = Template.bind({});
Default.args = {
  withWrapper: true,
};

export const BoardInfo = Template.bind({});
BoardInfo.args = {
  withWrapper: false,
  modalContent: (
    <SingleContactBoard
      modalData={{
        img: personPlaceholder,
        desc: ['I do stuff', 'And other stuff'],
        name: 'John Doe',
        occupation: 'The ultimate EESTECer',
        alt: 'John Doe',
      }}
    />
  ),
};
BoardInfo.argTypes = {
  modalContent: { control: false },
};

export const CopyrightsInfo = Template.bind({});
CopyrightsInfo.args = {
  withWrapper: false,
  modalContent: (
    <CopyrightsModal
      // eslint-disable-next-line no-console
      onAgree={() => console.log('You did agree!')}
      // eslint-disable-next-line no-console
      onDisagree={() => console.log('You did agree!')}
    />
  ),
};
CopyrightsInfo.argTypes = {
  modalContent: { control: false },
};

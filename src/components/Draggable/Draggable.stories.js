import React from 'react';
import Draggable from 'components/Draggable/Draggable';
import { generateExampleItems } from '../../../.storybook/helpers';

export default {
  title: 'Components/Draggable',
  component: Draggable,
  args: {
    children: generateExampleItems(10),
    onScrollStart: null,
    onScrollEnd: null,
  },
  argTypes: {
    children: { control: false },
    onScrollEnd: { control: false },
    onScrollStart: { control: false },
    innerRef: { control: false },
  },
};

function Template({ horizontal, vertical, ...args }) {
  let classes = '';
  classes += horizontal ? ' draggable-sb-helper' : '';
  classes += vertical ? ' draggable-sb-helper-vertical' : '';
  return (
    <div className={classes}>
      <Draggable {...args} horizontal={horizontal} vertical={vertical} />
    </div>
  );
}

export const Horizontal = Template.bind({});
Horizontal.args = {
  vertical: false,
  horizontal: true,
};

export const Vertical = Template.bind({});
Vertical.args = {
  vertical: true,
  horizontal: false,
};

export const Both = Template.bind({});
Both.args = {
  vertical: true,
  horizontal: true,
};

export const NoSnap = Template.bind({});
NoSnap.args = {
  vertical: false,
  horizontal: true,
  snap: false,
};

import React, {
  useCallback, useEffect, useMemo, useRef,
} from 'react';
import ScrollContainer from 'react-indiana-drag-scroll';
import PropTypes from 'prop-types';
import { animateDraggable } from 'utils/scroll/animateScroll';
import { getElementFullDimension, getMaxScrollLeft } from 'utils/dom';

import './Draggable.css';

const isTouchscreenDevice = () => navigator.maxTouchPoints > 0 && window.matchMedia('(pointer: coarse)').matches;

/**
 __Draggable__ is a wrapper for elements which allows scrolling by dragging the div with a mouse
 in case the elements overflow and require scrolling.

 It is used e.g. in the horizontal
 [`CardList`](./?path=/docs/components-cards-cardlist--horizontal).

 __NOTE:__ `Draggable` has snapping implemented which makes a correct to the end position of
 the scroll done by a user, so that the scroll does not stop in half of an element.
 Snapping _was not_ implemented for vertical version of `Draggable` yet.
*/
function Draggable({
  children,
  className,
  vertical,
  horizontal,
  hideScrollbars,
  onScrollEnd,
  onScrollStart,
  innerRef,
  snap,
}) {
  const ref = useRef();
  // Use ref instead of normal React useState as the calculations have to be synchronous
  const shouldStartSnapping = useRef(true);
  const timeoutRef = useRef();
  const lastTargetPositionRef = useRef();
  const isTouchscreen = useMemo(isTouchscreenDevice, []);

  // If innerRef props is provided, use it for all calculations, otherwise use the ref variable
  const workRef = useMemo(() => innerRef || ref, [innerRef]);
  // Imitate normal setState behavior function for the ref
  const setShouldStartSnapping = useCallback((newState) => {
    shouldStartSnapping.current = newState;
  }, [shouldStartSnapping]);

  // clear timeout on unmount
  useEffect(() => (() => {
    if (timeoutRef.current) {
      clearTimeout(timeoutRef.current);
    }
  }), [timeoutRef]);

  // TODO: Try placing it into useCallback
  const handleScrollEnd = () => {
    if (onScrollEnd) { // Execute onScrollEnd if specified
      onScrollEnd();
    } else if (snap && horizontal && !isTouchscreen) {
      // scrollLeft is the starting scroll position of the screen
      const { scrollLeft } = workRef.current;

      /*
        react-indiana-drag-scroll is sometimes eager to repeat scrolling so prevent re-running
        the scroll if the current value of scrollLeft is ok
      */
      if (lastTargetPositionRef.current != null && scrollLeft === lastTargetPositionRef.current) {
        return;
      }

      /*
        Snap only horizontally - TODO: Add support for vertical snapping if ever needed.
        Do not snap on mobile devices as the native CSS snapping can be used there without issues.

        NOTE: Snapping is done in JS because CSS snapping combined with the
        mouse dragging functionality DOES NOT work well on some browsers (e.g. Chrome).
      */

      /*
        Snap scroll has started. This is set to true so that this function is not executed again
        on the scroll end (onScrollEnd is executed both for user triggered scrolling and for
        programmatic scrolling such as the one from this function here.)
       */
      setShouldStartSnapping(false);
      let targetPosition;

      // maxScrollLeft is the highest possible value that scrollLeft can get (indicates full scroll)
      const maxScrollLeft = getMaxScrollLeft(workRef.current);
      const numberOfChildren = workRef.current.children.length;
      const lastChildWidth = getElementFullDimension(
        workRef.current.children[numberOfChildren - 1],
      ).width;

      /*
        If scrollLeft is closer to the maximum value than half of the width of the last child,
        then snap to the right
       */
      if (scrollLeft >= maxScrollLeft - Math.floor(lastChildWidth / 2)) {
        targetPosition = maxScrollLeft;
      } else {
        // Otherwise continue with calculating the desired scrollLeft position

        /*
          scrollPosition is a counter that in the end will have a value of widths um of all children
          that should be on the left from the final scroll position.
        */
        let scrollPosition = 0;
        for (let i = 0; i <= numberOfChildren; i += 1) {
          const childWidth = getElementFullDimension(workRef.current.children[i]).width;
          if (scrollLeft > scrollPosition + childWidth) {
            /*
              If the starting scroll position has a bigger value than current scrollPosition
              add the width of the child with index i to scrollPosition and go to the next child.
            */
            scrollPosition += childWidth;
          } else {
            /*
              Calculate whether the child that scrollLeft is in the area of
              should be snapped to the left or right
            */
            targetPosition = scrollLeft >= scrollPosition + Math.floor(childWidth / 2)
              ? scrollPosition + childWidth : scrollPosition;
            break;
          }
        }
      }
      // once again just in case check if scrolling is even needed
      lastTargetPositionRef.current = targetPosition;
      if (scrollLeft === targetPosition) {
        setShouldStartSnapping(true);
        return;
      }

      /*
        The programmatic scrolling processing is over so set the state back to false after
        the animation is done
      */
      if (timeoutRef.current) {
        clearTimeout(timeoutRef.current);
      }
      animateDraggable(targetPosition, 100, workRef, shouldStartSnapping, 'linear');
      timeoutRef.current = setTimeout(() => {
        workRef.current.scrollLeft = targetPosition;
        setShouldStartSnapping(true);
      }, 100);
    }
  };

  let classes = 'draggable';
  classes += className ? ` ${className}` : '';
  classes += vertical ? ' draggable-vertical' : '';
  classes += horizontal ? ' draggable-horizontal' : '';
  classes += !snap ? ' never-snap' : '';
  classes += isTouchscreen ? ' is-touchscreen' : '';

  return (
    <ScrollContainer
      className={classes}
      draggingClassName={`${classes} drag-inactive`}
      vertical={vertical}
      horizontal={horizontal}
      hideScrollbars={hideScrollbars}
      nativeMobileScroll={snap && isTouchscreen}
      onEndScroll={shouldStartSnapping.current && handleScrollEnd}
      onStartScroll={onScrollStart}
      innerRef={innerRef || ref}
    >
      {children}
    </ScrollContainer>
  );
}

Draggable.propTypes = {
  children: PropTypes.arrayOf(PropTypes.node).isRequired,
  className: PropTypes.string,
  /**
   A boolean variable indicating if the dragging should be possible vertically.
  */
  vertical: PropTypes.bool,
  /**
   A boolean variable indicating if the dragging should be possible horizontally.
  */
  horizontal: PropTypes.bool,
  /**
   A boolean variable indicating if the scrollbars of the container should be hidden or visible.
  */
  hideScrollbars: PropTypes.bool,
  /**
   A function executed when a user finishes dragging or scrolling.

   __NOTE:__ If this function is specified, the native snapping mechanism of `Draggable` is not
   executed after the scroll.
  */
  onScrollEnd: PropTypes.func,
  /**
   A function executed when a user starts dragging or scrolling.
   */
  onScrollStart: PropTypes.func,
  /**
   A ref that is passed to the actual wrapping `div` container
   */
  innerRef: PropTypes.shape({ current: PropTypes.instanceOf(Element) }),
  /**
   A boolean variable indicating if the container should snap the scroll
   position at the end of scrolling.
  */
  snap: PropTypes.bool,
};

Draggable.defaultProps = {
  className: null,
  vertical: false,
  horizontal: true,
  hideScrollbars: false,
  onScrollEnd: undefined,
  onScrollStart: undefined,
  innerRef: null,
  snap: true,
};

export default Draggable;

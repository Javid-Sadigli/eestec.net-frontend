import React from 'react';
import PropTypes from 'prop-types';
import { createParagraphs } from 'utils/misc';

import './Partner.css';

/**
 __Partner__ is a modal element displayed after clicking on one of the PartnersSlider elements.
 */
function Partner({
  name, img, alt, description,
}) {
  let classes = 'partner-modal';
  classes += description ? '' : ' no-description';

  return (
    <div className={classes}>
      <img src={img} alt={alt} />
      {createParagraphs(description, name, null, /\n|\\n/)}
    </div>
  );
}

Partner.propTypes = {
  img: PropTypes.string.isRequired,
  alt: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  description: PropTypes.string,
};

Partner.defaultProps = {
  description: null,
};

export default Partner;

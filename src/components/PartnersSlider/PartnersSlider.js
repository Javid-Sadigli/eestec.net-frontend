import React, {
  useContext, useEffect,
} from 'react';
import ModalContext from 'context/ModalContext';
import PartnersContext from 'context/PartnersContext';
import Partner from 'components/PartnersSlider/components/Partner/Partner';
import PartnerService from 'api/service/partners';
import ToastContext from 'context/ToastContext';
import logger from 'utils/logger';
import CarouselComponent from 'components/Carousel/Carousel';
import CarouselButtonBase from 'components/Carousel/components/CarouselButtonBase';

import 'components/PartnersSlider/PartnersSlider.css';

function PartnersSlider() {
  const { setModalContent } = useContext(ModalContext);
  const { partners, setPartners } = useContext(PartnersContext);
  const { setToastContent } = useContext(ToastContext);

  useEffect(() => {
    if (!partners.length) {
      PartnerService.getPartners()
        .then((response) => {
          setPartners(response.map(({
            name, logo, slug, description,
          }) => ({
            name,
            logo,
            description,
            slug,
          })));
        })
        .catch((err) => {
          // eslint-disable-next-line no-console
          logger.log(err);
          setToastContent('An error occurred while loading partners data', 'error');
        });
    }
  }, [partners, setPartners, setToastContent]);

  const setModal = (name, logo, description, slug) => {
    setModalContent(
      <Partner
        alt={name}
        img={logo}
        description={description}
        slug={slug}
        name={name}
      />,
    );
  };

  // let classes = 'slide-wrapper partners-slider';

  return (
    <CarouselComponent containerClassName="partners-carousel">
      {partners.map(({
        logo, name, description, slug,
      }) => (
        <CarouselButtonBase
          className="partners-button-base"
          key={slug}
          onClick={() => {
            setModal(name, logo, description, slug);
          }}
        >
          <img src={logo} alt={name} />
        </CarouselButtonBase>
      ))}
    </CarouselComponent>
  );
}

export default PartnersSlider;

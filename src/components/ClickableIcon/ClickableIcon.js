import React from 'react';
import PropTypes from 'prop-types';

import { generateRandomId } from 'utils/dom';

import 'components/ClickableIcon/ClickableIcon.css';

/**
 __ClickableIcon__ is a type of a button which is being displayed as an icon with a
 transparent background.

 __DEPRECATED in favor of [Material UI IconButton](https://mui.com/material-ui/react-button/#icon-button)__
*/
function ClickableIcon({
  onClick,
  icon,
  className,
  name,
  id = generateRandomId('clickable-icon'),
  title,
}) {
  const Icon = icon;
  return icon && (
    <button
      id={id}
      type="button"
      name={name}
      title={title}
      onClick={onClick}
      className={`clickable-icon${className ? ` ${className}` : ''}`}
    >
      <Icon sx={{ verticalAlign: 'bottom' }} />
    </button>
  );
}

ClickableIcon.propTypes = {
  onClick: PropTypes.func.isRequired,
  /**
   An imported element type of a
   [Material UI Icon](https://mui.com/material-ui/material-icons/).
  */
  icon: PropTypes.elementType.isRequired,
  className: PropTypes.string,
  /**
   For accessibility.
  */
  name: PropTypes.string,
  /**
   For accessibility.
  */
  title: PropTypes.string,
  // eslint-disable-next-line react/require-default-props
  id: PropTypes.string,
};

ClickableIcon.defaultProps = {
  className: null,
  name: null,
  title: 'Clickable icon',
};

export default ClickableIcon;

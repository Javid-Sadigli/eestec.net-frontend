import React from 'react';
import ClickableIcon from 'components/ClickableIcon/ClickableIcon';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';

export default {
  title: 'Components/ClickableIcon',
  component: ClickableIcon,
  args: {
    // eslint-disable-next-line no-alert
    onClick: () => alert('You cliked a ClickableIcon!'),
    icon: ArrowForwardIosIcon,
  },
  argTypes: {
    icon: {
      controls: false,
    },
  },
};

function Template(args) {
  return <ClickableIcon {...args} />;
}

export const Default = Template.bind({});

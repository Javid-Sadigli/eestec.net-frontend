import React from 'react';
import CheckboxInput from 'components/CheckboxInput/CheckboxInput';
import SkeletonCheckboxes from 'components/CheckboxInput/skeleton/SkeletonCheckboxes';
import { useArgs } from '@storybook/addons';

export default {
  title: 'Components/CheckboxInput',
  component: CheckboxInput,
  subcomponents: { SkeletonCheckboxes },
  decorators: [(Story) => (
    <div style={{ width: '200px' }}>
      {Story()}
    </div>
  )],
};

function Template({ onChange, ...args }) {
  const [{ checked, disabled }, updateArgs] = useArgs();
  const handleClick = () => {
    if (!disabled) {
      updateArgs({ checked: !checked });
    }
  };
  return <CheckboxInput {...args} onChange={handleClick} />;
}

export const Default = Template.bind({});
Default.args = {
  checked: true,
  label: 'Checkbox Input',
};

export const Skeleton = Template.bind({});
Skeleton.args = {
  checked: true,
  disabled: true,
  label: '',
  isSkeleton: true,
};

export function SkeletonGroup({ length }) {
  return <SkeletonCheckboxes length={length} />;
}
SkeletonGroup.args = {
  length: 8,
};
SkeletonGroup.parameters = {
  controls: {
    exclude: /[^length]/g,
  },
};

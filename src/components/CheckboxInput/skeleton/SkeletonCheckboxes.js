import React from 'react';
import PropTypes from 'prop-types';
import CheckboxInput from 'components/CheckboxInput/CheckboxInput';

import 'skeleton.css';
import './SkeletonCheckboxes.css';

function SkeletonCheckboxes({ length }) {
  return (
    Array(length).fill(null).map((item, index) => (
      <CheckboxInput
        label=""
        // eslint-disable-next-line react/no-array-index-key
        key={`skeleton-checkbox-${index}`}
        onChange={() => {}}
        checked
        disabled
        isSkeleton
      />
    ))
  );
}

SkeletonCheckboxes.propTypes = {
  length: PropTypes.number,
};

SkeletonCheckboxes.defaultProps = {
  length: 1,
};

export default SkeletonCheckboxes;

import React from 'react';
import PropTypes from 'prop-types';

import { generateRandomId } from 'utils/dom';

import 'components/CheckboxInput/CheckboxInput.css';
import { getRandomInt } from 'utils/misc';

/**
 __CheckboxInput__ allows the user to make a selection of multiple items displayed in a column.
 */
function CheckboxInput({
  checked,
  label,
  onChange,
  className,
  disabled,
  isSkeleton,
  id = generateRandomId('checkbox-input'),
}) {
  let classes = 'checkbox-input-container';
  classes += className ? ` ${className}` : '';
  classes += isSkeleton ? ' skeleton-checkbox' : '';
  classes += disabled ? ' disabled' : '';

  const renderLabel = () => {
    if (!isSkeleton) {
      return <span className="checkbox-label">{label}</span>;
    }
    const randomMargin = getRandomInt(0, 75);
    return (
      <div
        className="skeleton skeleton-inner-checkbox"
        style={{
          marginRight: `${randomMargin}%`,
        }}
      />
    );
  };

  return (
    <div className={classes}>
      <label htmlFor={id}>
        <input type="checkbox" checked={checked} id={id} onChange={onChange} disabled={disabled} />
        <span className="checkbox" />
        {renderLabel()}
      </label>
    </div>
  );
}

CheckboxInput.propTypes = {
  checked: PropTypes.bool.isRequired,
  label: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  className: PropTypes.string,
  // eslint-disable-next-line react/require-default-props
  id: PropTypes.string,
  disabled: PropTypes.bool,
  /**
   Used for indicating whether the checkbox is in a loading state.
  */
  isSkeleton: PropTypes.bool,
};

CheckboxInput.defaultProps = {
  className: null,
  disabled: false,
  isSkeleton: false,
};

export default CheckboxInput;

import React, { useRef } from 'react';
import PropTypes from 'prop-types';
import { ButtonBase } from '@mui/material';

/**
 __CarouselButtonBase__ is used to wrap items of `Carousel` and enable them
 to act as buttons and come with a `onClick` behavior.
 */
function CarouselButtonBase({ className, onClick, children }) {
  const moved = useRef(false);
  const move = () => {
    moved.current = true;
  };
  const resetMove = () => {
    moved.current = false;
  };

  const handleClick = () => {
    if (!moved.current) {
      onClick();
    }
  };

  return (
    <ButtonBase
      className={className}
      onClick={handleClick}
      onMouseDown={resetMove}
      onTouchStart={resetMove}
      onMouseMove={move}
      onTouchMove={move}
    >
      {children}
    </ButtonBase>
  );
}

CarouselButtonBase.propTypes = {
  className: PropTypes.string,
  onClick: PropTypes.func.isRequired,
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
    PropTypes.node,
    PropTypes.arrayOf(PropTypes.node),
  ]).isRequired,
};

CarouselButtonBase.defaultProps = {
  className: undefined,
};

export default CarouselButtonBase;

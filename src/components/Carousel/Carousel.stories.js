import React from 'react';
import { Carousel } from 'components/Carousel/Carousel';
import CarouselButtonBase from 'components/Carousel/components/CarouselButtonBase';
import { generateExampleItems } from '../../../.storybook/helpers';

export default {
  title: 'Components/Carousel',
  component: Carousel,
  subcomponents: { CarouselButtonBase },
  argTypes: {
    children: { control: false },
  },
  parameters: {
    layout: 'fullscreen',
  },
  args: {
    nativeSnapping: false,
  },
};

function Template(args) {
  return <Carousel {...args} />;
}

export const Default = Template.bind({});
Default.args = {
  children: generateExampleItems(10, 'carousel-item'),
};
Default.argTypes = {
  arrows: { control: false },
};

export const WithoutAutoscroll = Template.bind({});
WithoutAutoscroll.args = {
  children: generateExampleItems(10, 'carousel-item'),
  autoscroll: false,
};
WithoutAutoscroll.argTypes = {
  autoscroll: { control: false },
};

export const WithArrows = Template.bind({});
WithArrows.args = {
  children: generateExampleItems(10, 'carousel-item'),
  arrows: true,
  autoscroll: false,
};
WithArrows.argTypes = {
  autoscroll: { control: false },
};

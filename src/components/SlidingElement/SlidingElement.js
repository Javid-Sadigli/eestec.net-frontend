import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';

import useOnScreen from 'hooks/useOnScreen';

import 'components/SlidingElement/SlidingElement.css';

/**
 __SlidingElement__ takes advantage of the `useOnScreen` hook in order to slide the element from
 our of the vieport into the screen when the user reached to a scroll destination where
 this element should appear.
*/
function SlidingElement({ children, slideRight }) {
  const contentRef = useRef(null);
  const isVisible = useOnScreen(contentRef);
  const [wasEverVisible, setWasEverVisible] = useState(false);

  useEffect(() => {
    if (isVisible) {
      setWasEverVisible(true);
    }
  }, [isVisible]);

  let classes = 'sliding-element';
  classes += slideRight ? ' right' : ' left';
  classes += (isVisible || wasEverVisible) ? ' visible' : '';

  return (
    <div ref={contentRef} className={classes}>
      {children}
    </div>
  );
}

SlidingElement.propTypes = {
  /**
   Any amount of nodes that should be inside the sliding element wrapper.
  */
  children: PropTypes.element.isRequired,
  /**
   If true, the content will slide in from the right side of the viewport.
  */
  slideRight: PropTypes.bool,
};

SlidingElement.defaultProps = {
  slideRight: false,
};

export default SlidingElement;

import React from 'react';
import SlidingElement from 'components/SlidingElement/SlidingElement';
import HeadlinedContent from 'components/HeadlinedContent/HeadlinedContent';
import { loremIpsum } from '../../../.storybook/helpers';

export default {
  title: 'Components/SlidingElement',
  component: SlidingElement,
  decorators: [(Story) => (
    <div style={{ overflow: 'hidden' }} className="section">
      {Story()}
    </div>
  )],
  parameters: {
    layout: 'fullscreen',
  },
};

function Template(args) {
  return (
    <SlidingElement {...args} />
  );
}

export const Left = Template.bind({});
Left.args = {
  children: [
    <SlidingElement>
      <HeadlinedContent
        title="Sliding Element - Left"
        variant="h1"
        fullWidthText
      >
        <p>
          {loremIpsum}
        </p>
      </HeadlinedContent>
    </SlidingElement>,
  ],
  slideRight: false,
};

export const Right = Template.bind({});
Right.args = {
  children: [
    <SlidingElement>
      <HeadlinedContent
        title="Sliding Element - Right"
        variant="h1"
        fullWidthText
        splitRight
      >
        <p>
          {loremIpsum}
        </p>
      </HeadlinedContent>
    </SlidingElement>,
  ],
  slideRight: true,
};

import React from 'react';
import PropTypes from 'prop-types';

import 'components/Heading/Heading.css';

export const headingVariants = ['h1', 'h2', 'h3', 'h4', 'h5', 'h6'];

const VariantMapping = {
  h1: 'h1',
  h2: 'h2',
  h3: 'h3',
  h4: 'h4',
  h5: 'h5',
  h6: 'h6',
};

const VariantClassMapping = {
  h1: 'heading-h1',
  h2: 'heading-h2',
  h3: 'heading-h3',
  h4: 'heading-h4',
  h5: 'heading-h5',
  h6: 'heading-h6',
};

/**
 __Heading__ is... a heading.
 */
function Heading({
  children, variant, styleVariant, className, id, lineBefore, lineAfter, innerRef,
}) {
  const HeadingType = VariantMapping[variant];
  let classes = `heading ${styleVariant ? VariantClassMapping[styleVariant] : VariantClassMapping[variant]}`;
  classes += lineBefore ? ' line-before' : '';
  classes += lineAfter ? ' line-after' : '';
  classes += className ? ` ${className}` : '';

  return (
    <HeadingType ref={innerRef} className={classes} id={id}>{children}</HeadingType>
  );
}

Heading.propTypes = {
  /**
   Contents of the heading. Can be a plain string or a `span`.
   */
  children: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
  /**
   HTML variant of the heading - from `h1` to `h6` depending on the meaning.
   */
  variant: PropTypes.oneOf(headingVariants).isRequired,
  /**
   If the looks of the heading should be different from the chosen `variant`, the proper
   styling should be set using this property.
   */
  styleVariant: PropTypes.oneOf(headingVariants),
  className: PropTypes.string,
  lineBefore: PropTypes.bool,
  lineAfter: PropTypes.bool,
  id: PropTypes.string,
  /**
   A reference to be passed to the actual HTML heading node.
   */
  innerRef: PropTypes.shape({ current: PropTypes.instanceOf(Element) }),
};

Heading.defaultProps = {
  lineBefore: false,
  lineAfter: false,
  styleVariant: null,
  className: null,
  id: null,
  innerRef: null,
};

export default Heading;

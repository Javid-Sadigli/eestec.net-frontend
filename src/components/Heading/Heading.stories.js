import React from 'react';
import Heading from 'components/Heading/Heading';

export default {
  title: 'Components/Heading',
  component: Heading,
  argTypes: {
    innerRef: {
      control: false,
    },
  },
};

export function Default(args) {
  return <Heading {...args} />;
}
Default.args = {
  variant: 'h1',
  lineAfter: true,
  children: 'Heading',
};

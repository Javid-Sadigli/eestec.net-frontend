import React from 'react';
import Toast from 'components/Toast/Toast';
import ToastContextDecorator from '../../../.storybook/decorators/ToastContextDecorator';

export default {
  title: 'Components/Toast',
  component: Toast,
  decorators: [ToastContextDecorator],
  argTypes: {
    type: {
      options: ['info', 'success', 'warning', 'error'],
      control: { type: 'radio' },
      defaultValue: 'info',
      description: 'Indicates what type of information is being displayed.',
      table: {
        type: { summary: ['\'info\'', ' \'success\'', ' \'warning\'', ' \'error\''] },
        defaultValue: { summary: '\'info\'' },
      },
    },
    content: {
      defaultValue: 'Hey, I am a Toast!',
      table: {
        type: { summary: 'string' },
        defaultValue: { summary: '' },
      },
      control: {
        type: 'text',
      },
    },
    autoHideDuration: {
      defaultValue: null,
      control: false,
      description: 'The amount of ms after which the Toast will hide automatically',
      table: {
        defaultValue: { summary: 6000 },
        type: { summary: 'number' },
      },
    },
    vertical: {
      defaultValue: 'bottom',
      control: false,
      description: 'The absolute vertical positioning of the Toast',
      table: {
        defaultValue: { summary: '\'bottom\'' },
        type: { summary: ['\'bottom\'', ' \'top\''] },
      },
    },
    horizontal: {
      defaultValue: 'right',
      control: false,
      description: 'The absolute horizontal positioning of the Toast',
      table: {
        defaultValue: { summary: '\'right\'' },
        type: { summary: ['\'right\'', ' \'left\''] },
      },
    },
    open: {
      defaultValue: true,
      control: false,
      description: 'Indicates if the Toast should be visible',
      table: {
        defaultValue: { summary: false },
        type: { summary: 'boolean' },
      },
    },
  },
};

function Template(args) {
  return <Toast {...args} />;
}

export const Info = Template.bind({});

export const Success = Template.bind({});
Success.args = {
  type: 'success',
  content: 'Hey, I am a successful Toast!',
};

export const Warning = Template.bind({});
Warning.args = {
  type: 'warning',
  content: 'Hey, I am a warning Toast!',
};

export const Error = Template.bind({});
Error.args = {
  type: 'error',
  content: 'Hey, I am an error Toast!',
};

import React from 'react';
import NavLink from 'components/NavLink/NavLink';
import { withRouter } from 'storybook-addon-react-router-v6';
import HeaderNavLinkComponent from 'layouts/MainLayout/components/Header/components/HeaderNavLink/HeaderNavLink';
import FooterNavLinkComponent from 'layouts/MainLayout/components/Footer/components/FooterNavLink/FooterNavLink';

export default {
  title: 'Components/Link variants/NavLink',
  component: NavLink,
  decorators: [withRouter],
  argTypes: {
    match: {
      control: {
        type: 'boolean',
      },
      table: {
        disable: true,
      },
      defaultValue: false,
    },
    to: { control: false },
  },
};

function Template({ match, to, ...args }) {
  return <NavLink {...args} to={match ? '/' : to} />;
}

export const Default = Template.bind({});
Default.args = {
  to: '/path?=/story/components/linkvariants-navlink',
  children: 'Link',
};

export const Active = Template.bind({});
Active.args = {
  to: '/path?=/story/components/linkvariants-navlink',
  children: 'Link',
  match: true,
};

export function HeaderNavLink(args) {
  return <HeaderNavLinkComponent {...args} />;
}
HeaderNavLink.args = {
  to: '/path?=/story/components/linkvariants-navlink',
  children: 'Header Link',
  // eslint-disable-next-line no-console
  onClickHandler: () => console.log('You clicked the HeaderNavLink!'),
};

export function FooterNavLink(args) {
  return <div className="storybook-footernavlink"><FooterNavLinkComponent {...args} /></div>;
}
FooterNavLink.args = {
  to: '/path?=/story/components/linkvariants-navlink',
  children: 'Footer Link',
  // eslint-disable-next-line no-console
  onClickHandler: () => console.log('You clicked the FooterNavLink!'),
};

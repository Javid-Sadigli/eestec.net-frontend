import React, { useRef } from 'react';
import SeeMoreButton from 'components/SeeMoreButton/SeeMoreButton';

export default {
  title: 'Components/SeeMoreButton',
  component: SeeMoreButton,
  argTypes: {
    target: { control: false },
  },
};

export function Default({ label }) {
  const ref = useRef();
  return (
    <div style={{ height: '1800px' }}>
      <div style={{ height: '900px' }}>
        <SeeMoreButton label={label} target={ref} />
      </div>
      <div style={{ height: '900px' }} ref={ref}>
        <p>Target destination is here.</p>
      </div>
    </div>
  );
}
Default.args = {
  label: 'See more',
};

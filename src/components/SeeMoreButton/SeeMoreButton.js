import React from 'react';
import PropTypes from 'prop-types';
import IconButton from '@mui/material/IconButton';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';

import scroll from 'utils/scroll';

import 'components/SeeMoreButton/SeeMoreButton.css';

/**
 __SeeMoreButton__ allows to quickly scroll to the given target (specified by ref)
 by clicking the button.
*/
function SeeMoreButton({ target, label }) {
  const scrollToTarget = () => scroll(
    target,
    null,
    0,
    1000,
    'easeOutBack',
    1,
  );

  return (
    <IconButton
      className="icon-button see-more-button"
      aria-label="scroll down"
      onClick={scrollToTarget}
      disableRipple
    >
      {label && <p>{label}</p>}
      <KeyboardArrowDownIcon className="see-more-icon" />
    </IconButton>
  );
}

SeeMoreButton.propTypes = {
  /**
   A ref to the element that the button's action is supposed to scroll to.
  */
  target: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.shape({ current: PropTypes.instanceOf(Element) }),
  ]).isRequired,
  /**
   An optional label above the button's icon.
  */
  label: PropTypes.string,
};

SeeMoreButton.defaultProps = {
  label: null,
};

export default SeeMoreButton;

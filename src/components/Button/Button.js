import React, { useState } from 'react';
import PropTypes from 'prop-types';
import CircularProgress from '@mui/material/CircularProgress';

import 'components/Button/Button.css';

export function createRipple(event) {
  const button = event.currentTarget;
  const rect = button.getBoundingClientRect();

  const circle = document.createElement('span');
  circle.classList.add('ripple');
  const diameter = Math.max(button.clientWidth, button.clientHeight);
  const radius = diameter / 2;

  circle.style.width = `${diameter}px`;
  circle.style.height = `${diameter}px`;
  circle.style.left = `${event.clientX - rect.left - radius}px`;
  circle.style.top = `${event.clientY - rect.top - radius}px`;

  button.appendChild(circle);
  setTimeout(() => {
    circle.remove();
  }, 400);
}

/**
 __Button__ is the main button, used in most of the cases on the website.

 If you are looking for a `Button` that works as a link but looks exactly the same
 as this on here, check out
 [ButtonLink](./?path=/docs/components-linkvariants-buttonlink).
 */
function Button({
  className, icon, children, onClick, isLoading, id, type, disabled,
}) {
  const [horizontalSpinnerPadding, setHorizontalSpinnerPadding] = useState({});

  function handleClick(e) {
    e.preventDefault();
    createRipple(e);
    setHorizontalSpinnerPadding({
      paddingLeft: `${(e.target.clientWidth - 37) / 2}px`,
      paddingRight: `${(e.target.clientWidth - 37) / 2}px`,
    });
    onClick();
  }

  const Icon = icon;
  const iconToRender = icon && <Icon sx={{ fontSize: '17px', verticalAlign: 'bottom' }} />;

  return isLoading ? (
    <CircularProgress size={37} sx={horizontalSpinnerPadding} className={className} />
  ) : (
    <button
      // eslint-disable-next-line react/button-has-type
      type={type}
      className={`${className}${disabled ? ' disabled' : ''}`}
      onClick={handleClick}
      id={id}
      disabled={disabled}
    >
      {children}
      {iconToRender}
    </button>
  );
}

Button.propTypes = {
  /**
   Classes for implemented variants are i.a. `"primary-button"`,
   `"secondary-button"`, `"inverted"`, `"round"`.
  */
  className: PropTypes.string,
  disabled: PropTypes.bool,
  /**
   An imported element type of a
   [Material UI Icon](https://mui.com/material-ui/material-icons/).
  */
  icon: PropTypes.elementType,
  /**
   Can be a string text or an element<br />(e.g. `<span>...</span>`)
  */
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.element,
  ]),
  onClick: PropTypes.func.isRequired,
  isLoading: PropTypes.bool,
  id: PropTypes.string,
  /**
   `'button'` for regular buttons, `'submit'` for forms
  */
  type: PropTypes.oneOf(['button', 'submit']),
};

Button.defaultProps = {
  className: '',
  icon: null,
  children: null,
  isLoading: false,
  id: null,
  type: 'button',
  disabled: false,
};

export default Button;

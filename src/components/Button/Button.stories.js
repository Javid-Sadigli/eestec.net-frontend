/* eslint-disable no-alert */
import React from 'react';
import ArrowForwardIosIcon from '@mui/icons-material/ArrowForwardIos';
import SplitButtonComponent from 'components/SplitButton/SplitButton';
import LinkButtonComponent from 'components/LinkButton/LinkButton';
import Button from './Button';
import { splitButtonMockedProps } from '../../../.storybook/helpers';

export default {
  title: 'Components/Button',
  component: Button,
  subcomponents: { SplitButton: SplitButtonComponent, LinkButton: LinkButtonComponent },
  argTypes: {
    icon: {
      control: false,
    },
  },
};

function Template(args) {
  return <Button {...args} />;
}

export const Primary = Template.bind({});
Primary.args = {
  className: 'primary-button',
  onClick: () => alert('I am a primary button!'),
  children: 'Button',
};

export const Secondary = Template.bind({});
Secondary.args = {
  className: 'secondary-button',
  onClick: () => alert('I am a secondary button!'),
  children: 'Button',
};

export const WithIcon = Template.bind({});
WithIcon.args = {
  className: 'primary-button',
  onClick: () => alert('I am a button with an icon!'),
  children: 'Button',
  icon: ArrowForwardIosIcon,
};

export const Inverted = Template.bind({});
Inverted.args = {
  className: 'inverted',
  onClick: () => alert('I am an inverted button!'),
  children: 'Button',
};

export const Round = Template.bind({});
Round.args = {
  className: 'primary-button round',
  onClick: () => alert('I am a round button!'),
  children: 'Button',
};

export function SplitButton(args) {
  return (
    <div style={{ minHeight: '150px' }}>
      <SplitButtonComponent {...args} />
    </div>
  );
}
SplitButton.args = splitButtonMockedProps.props;
SplitButton.parameters = {
  controls: {
    exclude: /.*/g,
  },
};

export function LinkButton(args) {
  return (
    <LinkButtonComponent {...args} />
  );
}
LinkButton.args = {
  onClick: () => alert('Look! You clicked a link that got executed like a button!'),
  children: 'I may look like a link but I am actually a button!',
  title: 'ButtonLink',
};
LinkButton.parameters = {
  controls: {
    exclude: /.*/g,
  },
};

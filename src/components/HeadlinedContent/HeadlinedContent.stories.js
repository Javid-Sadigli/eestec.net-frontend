import React from 'react';
import HeadlinedContent from 'components/HeadlinedContent/HeadlinedContent';
import { headingVariants } from 'components/Heading/Heading';
import personPlaceholder from 'assets/img/member-placeholder.jpg';
import { loremIpsum } from '../../../.storybook/helpers';

export default {
  title: 'Components/HeadlinedContent',
  component: HeadlinedContent,
  args: {
    children: <p>{loremIpsum}</p>,
    variant: 'h1',
  },
  decorators: [(Story) => (
    <div className="section">{Story()}</div>
  )],
  parameters: {
    layout: 'fullwidth',
  },
  argTypes: {
    children: { control: false },
    innerRef: {
      control: false,
    },
    variant: {
      options: headingVariants,
    },
    styleVariant: {
      options: headingVariants,
    },
  },
};

function Template(args) {
  return <HeadlinedContent {...args} />;
}

export const Left = Template.bind({});
Left.args = {
  title: 'Left',
};

export const Right = Template.bind({});
Right.args = {
  title: 'Right',
  splitRight: true,
};

export const FullWidth = Template.bind({});
FullWidth.args = {
  title: 'Full width',
  fullWidthText: true,
};

export const WithImage = Template.bind({});
WithImage.args = {
  title: 'With an image',
  img: {
    imgId: 'headlined-content-image',
    imgSrc: personPlaceholder,
    imgAlt: 'The image of a headlined content',
  },
};

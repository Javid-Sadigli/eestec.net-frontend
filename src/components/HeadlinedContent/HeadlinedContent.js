import React from 'react';
import PropTypes from 'prop-types';
import Heading, { headingVariants } from 'components/Heading/Heading';

import 'components/HeadlinedContent/HeadlinedContent.css';

/**
 __HeadlinedContent__ is a reusable component to format sections that consist of a heading
 and a paragraph of text or other elements.
 */
function HeadlinedContent({
  children,
  splitRight,
  title,
  variant,
  styleVariant,
  img,
  fullWidthText,
  className,
  headingClassName,
  innerRef,
}) {
  return (
    <div className={`split ${className || ''}`} ref={innerRef}>
      <Heading
        className={`${(splitRight ? 'split-title-right' : 'split-title-left')} ${headingClassName}`}
        variant={variant}
        styleVariant={styleVariant}
        lineBefore={splitRight}
        lineAfter={!splitRight}
      >
        {title}
      </Heading>
      <div className={splitRight ? 'split-right' : 'split-left'}>
        <div className={img || fullWidthText ? null : 'half'}>
          {children}
        </div>
        {img && (<img id={img.imgId} src={img.imgSrc} alt={img.imgAlt} />)}
      </div>
    </div>
  );
}

HeadlinedContent.propTypes = {
  /**
   The text content of the headlined content.
  */
  children: PropTypes.node.isRequired,
  /**
   By default the heading and text are displayed on the left side of the screen.
   If this value is `true`, they are displayed on the right.
  */
  splitRight: PropTypes.bool,
  /**
   The heading text.
  */
  title: PropTypes.string.isRequired,
  /**
   If there should be any image next to the headlined content, this object's properties
   describe its src, id and alt text.
  */
  img: PropTypes.shape(
    {
      imgId: PropTypes.string,
      imgSrc: PropTypes.string,
      imgAlt: PropTypes.string,
    },
  ),
  /**
   The class name passed to the internal `Heading` component
  */
  headingClassName: PropTypes.string,
  /**
   A ref that is passed to the actual wrapping `div` container
   */
  innerRef: PropTypes.shape({ current: PropTypes.instanceOf(Element) }),
  className: PropTypes.string,
  /**
   HTML variant of the heading - from `h1` to `h6` depending on the meaning.
  */
  variant: PropTypes.oneOf(headingVariants).isRequired,
  /**
   If the looks of the heading should be different from the chosen `variant`, the proper
   styling should be set using this property.
  */
  styleVariant: PropTypes.oneOf(headingVariants),
  /**
   If the text should be full width and not only kept to the side constrained by `splitRight`
   property, this value has to be set to `true`. The headlined content is NOT full width
   when it has an accompanying `img`.
  */
  fullWidthText: PropTypes.bool,
};

HeadlinedContent.defaultProps = {
  splitRight: false,
  img: null,
  styleVariant: null,
  fullWidthText: false,
  headingClassName: '',
  className: null,
  innerRef: null,
};

export default HeadlinedContent;

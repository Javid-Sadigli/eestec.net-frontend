import { useContext, useEffect, useRef } from 'react';
import AuthContext from 'context/AuthContext';

/**
 __useResetDataOnLogout__ can be declared in views which content should change
 depending on users' login status. It checks if a user has logged out and
 resets data such as listings or admin props.
 */
function useResetDataOnLogout({ listingSetters = [], adminPropsSetters = [] }) {
  const wasEverLoggedIn = useRef(false);
  const { loggedIn } = useContext(AuthContext);

  useEffect(() => {
    if (loggedIn) {
      wasEverLoggedIn.current = true;
    } else if (wasEverLoggedIn.current) {
      // set an empty array to any listing using the listingSetters
      listingSetters.forEach((setter) => setter([]));
      // set FALSE as edit rights to each adminPropsSetter
      adminPropsSetters.forEach((setter) => setter(false));
    }
    // Exhaustive deps just do not make sense here and we can safely omit it
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [loggedIn]);
}

export default useResetDataOnLogout;

import { Meta } from "@storybook/addon-docs/blocks";

<Meta title="Hooks/useEntityEdit" />

# `useEntityEdit`

The hook provides an interface for editing entity pages
(such as Event, Branch, Profile and others in the future).
It handles the logic of enabling editing, loading and
data submission of different entity elements separately.
The hook stores, updates and returns the form data of the edited entity.

It accepts `apiServiceCall` - a two argument function which should include a call to BE
(defined in [`api`](./?path=/story/docs-api-calls--page)), where the first argument is the form
data to be sent and the other is an array of additional arguments that should be passed further to the
API service function. It also accepts the state of the entity that is the subject of
alteration and a setter function of this state. Lastly, if any function is to be executed in case of
a successful edit submission, it can be passed as `successCallback`.

In all the pages that present any type of entity on a website (Branch, Profile, Event, Team, Project etc.)
this hook should be used to handle **all** edit actions of the entity.

All the variables returned from this hook can be used nicely with
[`ConditionalInput`](./?path=/story/components-conditionalinput--text-conditional-input).

## Example

```js
const [entityData, setEntityData] = useState();

useEffect(() => {
  EntityService.getEntity(entitySlug)
    .then((response) => setEntityData(response)) // response = { name: 'LC Waikiki' }
    .catch((err) => logger.log(err));
}, []);

const {
  setEntityEditFormData, // setter function that accepts a key and a new value for edit
  toggleIsBeingEdited, // setter function that accepts a key of a property which editing state is to be toggled
  onEntityEditSubmit, // a function that accepts a key of a property which is changed for submission
  isEntityEditLoading, // an object containing information about the loading state of all properties
  isEditingDisabled, // indicates whether editing is disabled due to loading
  isBeingEdited, // an object containing formData keys and boolean values of edit status
  entityEditFormData, // the current altered data, not yet submitted
  isAnyBeingEdited, // indicates whether any of the fields is in an editing state
  resetEntityEditFormData, // a function that resets all keys to initial values
} = useEntityEdit({
  apiServiceCall: (formData, requestArgs) => EntityService.editEntity(
    entitySlug,
    formData,
    ...requestArgs,
  ),
  data: entityData,
  setData: setEntityData,
});

return (
 ...
 <EntitySection
   title="Name"
   className="entity-section"
 >
   <EditIcons
     className="entity-section-edit-icons"
     isBeingEdited={isBeingEdited?.name}
     onEditSubmit={() => onEntityEditSubmit(['name'])}
     onEditCancel={resetEntityEditFormData}
     onEditStart={() => toggleIsBeingEdited('name')}
     isLoading={isEntityEditLoading?.name}
     shouldDisplay
     shouldBeVisible={!isAnyBeingEdited}
   />
   <ConditionalInput
     onChange={(e) => setEntityEditFormData('name', e.target.value)}
     staticComponent={{
       element: Div,
       props: {
         className: 'description-conditional-input',
       },
     }}
     editableContent={entityEditFormData?.name}
     content={<p>{entityData.name}</p>}
     condition={isBeingEdited?.name}
     inputComponent={{
       element: TextInput,
       props: {
         className: 'name-conditional-input',
       },
     }}
     isAllowedToEdit
     isInputDisabled={isEditingDisabled}
   />
 </EntitySection>
 ...
);
```

## Input

| Parameter         | Type       | Required | Defaults   | Description                                                                                                                                                                                                                                                                                                                                                                                                           |
| :---------------- | :--------- | :------- | :--------- | :-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `apiServiceCall`  | `function` | **Yes**  | n/a        | An API calling function (defined within [`API services`](./?path=/story/docs-api-calls--page)) that is making PUT/PATCH requests to edit the entity. It should accept **two arguments** - `formData` and an array of extra arguments (`requestArgs`) that are then spread as the rest of the arguments of the service function.                                                                                       |
| `data`            | `object`   | **Yes**  | n/a        | An object containing all the information about the entity. It is usually just the main data point of a view that is calling this hook.                                                                                                                                                                                                                                                                                |
| `setData`         | `function` | **Yes**  | n/a        | A function that sets the new data after a successful edit of the entity.                                                                                                                                                                                                                                                                                                                                              |
| `successCallback` | `function` | No       | n/a        | A function that is getting called optionally after getting a successful response from the edit request. It should accept one argument `responseData` which contains the new state of information about the entity. It can be used if any additional operations have to performed using the response data (e.g. for `Profile` editing, the received data has to be also used to update the global state of user data). |

## Output

| Parameter                  | Type               | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| :------------------------- | :----------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `entityEditFormData`       | `object`           | An object representing the current form of the data (with keys the same as input data object), not yet submitted. After submission both data and this form data is being updated and equal.                                                                                                                                                                                                                                                                                                                                                                                                                                               |
| `setEntityEditFormData`    | `function`         | A function that allows editing the state of the form data. It accepts two arguments - the key that should be edited and the new value for that key.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| `isBeingEdited`            | `bool`             | An object representing the current editing status of the form data (with keys the same as input data object and form data). A key has a value of `true` when an editing status is enabled for this key (this is useful e.g for [`ConditionalInput`](./?path=/story/components-conditionalinput--text-conditional-input)                                                                                                                                                                                                                                                                                                                   |
| `toggleIsBeingEdited`      | `function`         | A function that changes the editing status for the given keys. It accepts one argument - the key that we want to change the status of, and it negates the current edit state.                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
| `onEntityEditSubmit`       | `function`         | A function that handles the submission of the updated form data. It accepts four arguments: 1. an array of `keys` that are updated (this information can be easily given since every submit button is only active when a single field is in edit state and this value is required to properly show which key(s) are the one we update). 2. `shouldCheckCopyrights` (default: `false`) which gives information whether `CopyrightsModal` should be displayed, 3. `pictureValue` (default: `null`) if an input we are changing is a picture and lastly 4. `requestArgs` that should be passed to API service call and were described above. |
| `resetEntityEditFormData`  | `function`         | A function that takes no arguments. It resets the current status of `formData` to be the same as data (so as if nothing was edited) and is setting the status of edit state and loading state of all keys to `false`.                                                                                                                                                                                                                                                                                                                                                                                                                     |
| `isEntityEditLoading`      | `object`           | An object representing the current loading status of the form data (with keys the same as input data object and form data). A key has a value of `true` when a loading status is enabled for this key (this is useful e.g for [`ConditionalInput`](./?path=/story/components-conditionalinput--text-conditional-input) and its `EditIcons`.                                                                                                                                                                                                                                                                                               |
| `isEditingDisabled`        | `bool`             | A boolean value indicating whether editing of this entity is disabled as e.g. the loading state is on for at least one of the keys and a request is being processed.                                                                                                                                                                                                                                                                                                                                                                                                                                                                      |
| `isAnyBeingEdited`         | `bool`             | A boolean value indicating whether at least one of the keys of the entity edit form data is in an editing state (which means that no other fields should be editable at this very moment).                                                                                                                                                                                                                                                                                                                                                                                                                                                |

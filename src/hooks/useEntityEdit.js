import React, {
  useCallback,
  useContext, useEffect, useMemo, useState,
} from 'react';
import { niceBytes, setAllKeysToValue } from 'utils/misc';
import ToastContext from 'context/ToastContext';
import ModalContext from 'context/ModalContext';
import Button from 'components/Button/Button';
import PropTypes from 'prop-types';
import logger from 'utils/logger';

const MAX_IMG_FILESIZE = 2097152; // in bytes 2097152

/**
 __CopyrightsModal__ is an internal component, used inside the `useEntityEdit` hook.
 It is called automatically if a user tries to upload a picture and `shouldCheckCopyrights`
 has been set in the calling function.
*/
export function CopyrightsModal({ onAgree, onDisagree }) {
  return (
    <div className="copyrights-modal">
      <p>Make sure that you have rights for the picture you are uploading.</p>
      <p>
        By uploading a picture you acknowledge that in case of any legal
        issues which your picture is a subject of, your branch will be the responsible entity.
      </p>
      <Button onClick={onDisagree} className="secondary-button">
        Cancel
      </Button>
      <Button onClick={onAgree} className="primary-button">
        Proceed
      </Button>
    </div>
  );
}

CopyrightsModal.propTypes = {
  onAgree: PropTypes.func.isRequired,
  onDisagree: PropTypes.func.isRequired,
};

/**
 __useEntityEdit__ provides an interface for editing entity pages (such as Event, Branch,
 Profile and others in the future). It handles the logic of enabling editing, loading
 and data submission of different entity elements separately.
 The hook stores, updates and returns the form data of the edited entity.
*/
function useEntityEdit({
  apiServiceCall, data, setData, successCallback,
}) {
  const { setToastContent } = useContext(ToastContext);
  const { setModalContent } = useContext(ModalContext);

  /*
    Below states will store objects with keys of the entity formData and the
    status of editing and loading. Set to null in the beginning in order
    to make it easy to skip the execution of useEffect of this custom hook on mount.
  */
  const [formData, setFormData] = useState(null);
  const [isBeingEdited, setIsBeingEdited] = useState(null);
  const [isEntityEditLoading, setIsEntityEditLoading] = useState(null);

  // The actual submission of the data
  const submitData = useCallback((keys, pictureValue = null, requestArgs = null) => {
    const newStateData = {};
    if (pictureValue) {
      /*
        We edit only one picture at the same time, so we use keys[0]
        to just get the first and only element of the keys array
      */
      newStateData[keys[0]] = pictureValue;
      // Set the loading status of the picture key to show it in the UI
      setIsBeingEdited((prevState) => ({
        ...prevState,
        [keys[0]]: !prevState[keys[0]],
      }));
    } else {
      keys.forEach((key) => {
        // Set to the value from the form and replace multiple newlines with one
        newStateData[key] = formData[key].replace(/\n\s*\n/g, '\n');
      });
    }

    // Set the loading status of the desired keys to show it in the UI
    setIsEntityEditLoading((prevState) => {
      const newEntityEditLoadingState = { ...prevState };
      if (pictureValue) {
        newEntityEditLoadingState[keys[0]] = true;
      } else {
        keys.forEach((key) => {
          // Alter the new state containing loading information to true for edited key
          newEntityEditLoadingState[key] = true;
        });
      }
      return newEntityEditLoadingState;
    });

    // Call the API service
    apiServiceCall(
      newStateData,
      /*
        Set the extra requestArgs that will be then spread into function's arguments.
        For now only one is needed. If requestArgs was not provided, return single true.

        In fact the requestArgs indicate in the service function whether this particular call
        contains an image to format the data properly. Usually just one argument is needed,
        set to true to indicate that this particular call contains an image.
        Yet sometimes entities can edit both square picture and the banner picture,
        therefore a specific combination of extra request args has to be set. And this
        specific combination is provided via requestArgs argument of the hook.
      */
      !pictureValue ? [false] : (requestArgs || [true]),
    )
      .then((response) => {
        // Spread old data and then new in order to make sure no data is lost
        const responseData = {
          ...data,
          ...response,
        };
        const toastContent = [
          'The update was successful.',
          'success',
          6000,
        ];
        if (keys.includes('email') && data.email !== newStateData.email) {
          // Set a longer toast duration if the information is longer
          toastContent[2] = 12000;
          // Set the toast message to include the info about verifying the new email address.
          toastContent[0] += ` A verification link was sent to ${newStateData.email} for you to confirm your new email address. Until you do it, your account will be considered unverified.`;
        }
        setData(responseData);
        setFormData(responseData);
        setToastContent(...toastContent);
        // Execute successCallback if such was provided
        if (successCallback) {
          successCallback(responseData);
        }
      })
      .catch((err) => {
        logger.log(err);
        let errorMessage = err?.data?.message;
        if (!errorMessage) {
          errorMessage = 'Unfortunately, we encountered an unidentified error while updating your data.';
          if (pictureValue) {
            errorMessage += '  Try reducing the filesize of the image you were trying to upload.';
          }
        }
        setToastContent(
          errorMessage,
          'error',
        );
        // Set form data to the original one if there was a failure
        setFormData(data);
      })
      .finally(() => {
        // Whatever happens, set edit and loading statuses back to false
        setIsEntityEditLoading((prevState) => {
          const newState = { ...prevState };
          keys.forEach((key) => {
            newState[key] = false;
          });
          return newState;
        });
        setIsBeingEdited((prevState) => {
          const newState = { ...prevState };
          keys.forEach((key) => {
            newState[key] = false;
          });
          return newState;
        });
      });
  }, [
    apiServiceCall,
    data,
    setData,
    setIsEntityEditLoading,
    setIsBeingEdited,
    setFormData,
    formData,
    setToastContent,
    successCallback,
  ]);

  /*
    When the data property of the custom hook is not empty set the initial values
    of the hook to reflect the proper keys. This cannot be done on mount since sometimes
    the data property is null initially and stops being null after being fetched from BE.

    This may not be the best solution to this situation as it causes a redundant re-render
    but I couldn't find anything better that would be both simple and working as expected.
  */
  useEffect(() => {
    if (data && !isBeingEdited && !isEntityEditLoading) {
      setFormData(data);
      setIsBeingEdited(setAllKeysToValue(data, false));
      setIsEntityEditLoading(setAllKeysToValue(data, false));
    }
  }, [
    data,
    isBeingEdited,
    isEntityEditLoading,
    setFormData,
    setIsBeingEdited,
    setIsEntityEditLoading,
  ]);

  // A boolean value that indicates whether editing is disabled due to loading
  const isEditingDisabled = useMemo(
    () => isEntityEditLoading !== null && Object.values(isEntityEditLoading).some((value) => value),
    [isEntityEditLoading],
  );

  // A boolean value that indicates whether any of the fields is in an editing state
  const isAnyBeingEdited = useMemo(
    () => isBeingEdited !== null && Object.values(isBeingEdited).some((value) => value),
    [isBeingEdited],
  );

  const resetFormData = () => {
    setFormData(data);
    setIsBeingEdited(setAllKeysToValue(data, false));
    setIsEntityEditLoading(setAllKeysToValue(data, false));
  };

  const onEntityEditSubmit = (
    key,
    shouldCheckCopyrights = false,
    pictureValue = null,
    requestArgs = null,
  ) => {
    // Handle too big images
    if (pictureValue?.size > MAX_IMG_FILESIZE) {
      setToastContent(
        `The image that you're trying to upload exceeds the 2 MB limit (${niceBytes(pictureValue.size)}).`,
        'error',
      );
      return;
    }
    if (!shouldCheckCopyrights) {
      // Do not bother with a CopyrightsModal if there is no need for that
      submitData(key, pictureValue, requestArgs);
    } else {
      setModalContent(
        <CopyrightsModal
          onAgree={() => {
            setModalContent(null);
            submitData(key, pictureValue, requestArgs);
          }}
          onDisagree={() => {
            setModalContent(null);
          }}
        />,
      );
    }
  };

  return {
    // Simplify setting the form data to update only the keys we want
    setEntityEditFormData: (key, value) => setFormData((prevState) => ({
      ...prevState,
      [key]: value,
    })),
    // Simplify setting the isBeingEdited data to update only the keys we want
    toggleIsBeingEdited: (key) => setIsBeingEdited((prevState) => ({
      ...prevState,
      [key]: !prevState[key],
    })),
    // Submit the alteration to BE
    onEntityEditSubmit,
    // Object containing formData keys and boolean values of loading
    isEntityEditLoading,
    // Object containing formData keys and boolean values of edit status
    isBeingEdited,
    // The current altered data, not yet submitted
    entityEditFormData: formData,
    // Indicates whether editing is disabled due to loading
    isEditingDisabled,
    // Indicates whether any of the fields is in an editing state
    isAnyBeingEdited,
    // Reset all to initial values
    resetEntityEditFormData: resetFormData,
  };
}

export default useEntityEdit;

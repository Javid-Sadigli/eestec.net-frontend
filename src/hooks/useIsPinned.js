import { useState, useEffect } from 'react';

/**
 __useIsPinned__ allows checking the status of a DOM element that has its position set to `sticky`.
*/
export default function useIsPinned(
  ref,
  noIntersectionCallback = () => {},
  intersectionCallback = () => {},
) {
  const [isPinned, setPinned] = useState(false);

  useEffect(() => {
    const observer = new IntersectionObserver(
      ([entry]) => {
        // take the ratio of 1 or greater to see if the element is fully visible now
        const intersectionResult = entry.intersectionRatio >= 1;
        // set the state of pinned according to the intersection
        setPinned(intersectionResult);
        if (intersectionResult) {
          // do something when there is the intersection
          intersectionCallback();
        } else {
          // do something when there is no intersection
          noIntersectionCallback();
        }
      },
      { threshold: [1] },
    );

    observer.observe(ref.current);
    // Remove the observer as soon as the component is unmounted
    return () => { observer.disconnect(); };
  }, [setPinned, ref, intersectionCallback, noIntersectionCallback]);

  return isPinned;
}

import { useState, useEffect } from 'react';

/**
 __useOnScreen__ uses the IntersectionObserver API to check if
 an element of a reference is visible on screen.
*/
export default function useOnScreen(ref) {
  const [isOnScreen, setIsOnScreen] = useState(false);

  useEffect(() => {
    const observer = new IntersectionObserver(
      ([entry]) => setIsOnScreen(entry.isIntersecting),
    );

    observer.observe(ref.current);
    // Remove the observer as soon as the component is unmounted
    return () => { observer.disconnect(); };
  }, [ref, setIsOnScreen]);

  return isOnScreen;
}

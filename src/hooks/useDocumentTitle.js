import { useEffect } from 'react';

/**
 __useDocumentTitle__ sets a title of the `head` of the tab to the given page for a
 `view` it is declared at.
 */
const useDocumentTitle = (title, isHomepage = false) => {
  useEffect(() => {
    document.title = isHomepage || !title ? 'EESTEC' : `${title} - EESTEC`;
  }, [title, isHomepage]);
};

export default useDocumentTitle;

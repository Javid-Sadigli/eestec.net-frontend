import { useEffect, useState } from 'react';
import MemberService from 'api/service/members';
import logger from 'utils/logger';

/**
  __useMembers__ is used for handling the members API endpoint and apply a front end logic
 to the result. This hook is given any member-like endpoint from the back end. The hook handles
 fetching the data, formatting/mapping it if necessary and operating with the "next page"
 API links for fetching more data.
*/
function useMembers(initialEndpoint, responseMapper = null) {
  // An array where the data is store
  const [members, setMembers] = useState([]);
  // Indicates what the status of fetching is
  const [membersLoading, setMembersLoading] = useState(true);
  // Stores the endpoint for fetching the initial data and more data
  const [endpoint, setEndpoint] = useState(initialEndpoint);

  useEffect(() => {
    // Creates an AbortController which aborts ongoing requests if component is unmounted
    const abortController = new AbortController();

    /*
      Only fetch the data when `membersLoading` is true and the received endpoint is not null
      This is also triggered on mount of the component that implements this hook
    */
    if (membersLoading && endpoint) {
      MemberService.getMembers(endpoint, abortController.signal)
        .then((response) => {
          // If any was provided, use the responseMapper to format data
          const responseData = responseMapper ? responseMapper(response) : response.data;
          // Signal the end of fetching process
          setMembersLoading(false);
          // Add the new data to the current one
          setMembers((prevState) => ([
            ...prevState,
            ...responseData,
          ]));
          // Set the endpoint for future calls to the actual next page endpoint
          setEndpoint(response?.next_page_url);
        })
        .catch((err) => {
          /*
            Some endpoints for members are hit while the user is not authenticated.
            If such an error occurs, do not log it as it is the desired behavior.
            Instead just signal the end of fetching process
          */
          if (err?.status === 403) {
            setMembersLoading(false);
          } else if (err?.message !== 'canceled' && err?.status !== 404) {
            logger.log(err);
            setMembersLoading(false);
          }
        });
    }

    // during unmount of the component abort the ongoing requests to avoid memory leak
    return () => abortController.abort();
  }, [membersLoading, endpoint, responseMapper]);

  return {
    // Gives the information whether the pagination is exhausted
    moreMembersAvailable: !!endpoint,
    // An array with the fetched data
    members,
    // A function to modify the fetched data
    setMembers,
    // Indicates what the status of fetching is
    membersLoading: membersLoading && members.length < 1,
    // Indicates what the status of fetching MORE members is
    moreMembersLoading: membersLoading,
    // Used to signal start of fetching
    getMoreMembers: () => setMembersLoading(true),
  };
}

export default useMembers;

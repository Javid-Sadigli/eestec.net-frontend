import { Meta } from "@storybook/addon-docs/blocks";

<Meta title="Hooks/useForm" />

# `useForm`

The hook provides an interface for dealing with multi-input forms,
managing input state, validating the input, storing errors and finally submitting the form.

It accepts an array of keys of the form that are initially filed with empty strings values,
an object of validators which keys should not include other keys than the ones defined in the
first argument's array of keys and which values are arrays containing all validating functions.
Lastly it accepts an `onSubmit` function that gets executed on the form submit.

## Validators

It is **not required** for each of the form keys to have its validator.
Validators are always functions that accept one argument (the value from the `formData`
associated with the desired key) and return `null` if validation is successful or a string
containing an error message otherwise.

**All the existing validating functions are defined in `utils/validators` and if you**
**need to handle a new validation type, add an appropriate function to that utils file.**

## Example

```js
const {
  formData, // an object with form's keys and values
  errors, // an object with form's keys and errors as values
  setErrors, // allow manipulating errors from outside the hook
  validators, // the same as input validators, not used in this example
  isFormLoading, // indicates whether the form is loading
  setFormLoading, // allow manipulating the loading state from outside the hook, not used in this example
  handleSubmit, // this must be used to handle validations before submitting
  handleInputChange, // accepts a key and a new value for this key of the formData
} = useForm({
  formKeys: ['email', 'password'],
  validators: {
    email: [validateNotEmpty, validateEmail],// functions from utils/validators
    password: [
      // functions from utils/validators
      validateNotEmpty,
      (password) => validateMinLength(password, 8),
      (password) => validateSamePasswords(password, formData.passwordConfirmation),
    ],
  },
  onSubmit: () => {
    ExampleService.doSomething(formData)
      .then(() => {
        console.log('success!');
      })
      .catch(({ data }) => {
        // use the hook's setErrors function to set errors from BE if such exist
        setErrors({
          email: data.errors?.email?.[0],
          password: data.errors?.password?.[0],
        })
      });
  },
});

return (
  <form>
    <TextInput
      label="Email"
      type="email"
      id="email"
      autoComplete="username"
      value={formData.email}
      onValueChange={(e) => handleInputChange('email', e.target.value)}
      errorMessage={errors?.email}
    />
    <TextInput
      label="Password"
      type="password"
      id="password"
      autoComplete="current-password"
      value={formData.password}
      onValueChange={(e) => handleInputChange('password', e.target.value)}
      errorMessage={errors?.password}
    />
    <Button
      type="submit"
      className="primary-button"
      isLoading={isFormLoading}
      onClick={handleSubmit}
    >
      Do something
    </Button>
  </form>
);
```

## Input

| Parameter    | Type       | Required | Defaults   | Description                                                                                                                                                                                                            |
| :----------- | :--------- | :------- | :--------- | :--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `formKeys`   | `array`    | **Yes**  | n/a        | An array of keys to be used in the form                                                                                                                                                                                |
| `validators` | `object`   | No       | `{}`       | An object containing the validators for the form fields. Keys within this object cannot be outside of the scope from `formKeys` and the values are one-argument functions returning either `null` or an error message. |
| `onSubmit`   | `function` | **Yes**  | n/a        | A function that should be executed on the form submit                                                                                                                                                                  |

## Output

| Parameter           | Type               | Description                                                                                                                                                                                          |
| :------------------ | :----------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `formData`          | `object`           | An object containing the form data.                                                                                                                                                                  |
| `errors`            | `object` \| `null` | An object containing the keys that are no different than the ones of the formData. The values are string error messages. `null` if no errors.                                                        |
| `setErrors`         | `function`         | A function that takes two arguments - the key of the form and the new error value associated with that key. Sets the new value of an error for the specified key.                                    |
| `isFormLoading`     | `bool`             | `true` if the form is loading (validation and request), `false` otherwise.                                                                                                                           |
| `setFormLoading`    | `function`         | A function that takes one boolean argument and sets the new state indicating whether the form is in a loading state.                                                                                 |
| `handleSubmit`      | `function`         | A function that takes no arguments. It handles the loading, validation and errors, so it is crucial to use it and provide the submission logic via input's `onSubmit`.                               |
| `handleInputChange` | `function`         | A function that takes two arguments - the key of the form and the new value associated with that key. Sets the new value of the form data for the specified key.                                     |
| `validators`        | `object`           | The form validators returned in the same way they were provided - in order to allow further manipulation if they were declared inline.                                                               |

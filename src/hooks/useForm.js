import { useEffect, useState } from 'react';

import validateFields from 'utils/validators';

/**
 __useForm__ provides an interface for dealing with multi-input forms,
 managing input state, validating the input, storing errors and finally submitting the form.
*/
function useForm({ formKeys, validators = {}, onSubmit }) {
  // Initialize a state object with all the keys given, setting the values to an empty string
  const [formData, setFormData] = useState(
    formKeys.reduce((acc, curr) => {
      acc[curr] = '';
      return acc;
    }, {}),
  );

  const [errors, setErrors] = useState(null);
  const [isFormLoading, setFormLoading] = useState(false);

  const handleInputChange = (key, value) => {
    // Set the new state, editing only the given key
    setFormData((prevState) => ({
      ...prevState,
      [key]: value,
    }));
  };

  const handleSubmit = () => {
    setFormLoading(true);
    const errorsFound = validateFields(validators, formData);
    if (errorsFound) {
      setErrors(errorsFound);
    } else {
      // Set errors to null to remove old errors on success
      setErrors(null);
      // Only submit the form if no errors were found
      onSubmit();
    }
  };

  useEffect(() => {
    // Disable the form loading if errors were found
    if (errors) {
      setFormLoading(false);
    }
  }, [errors, setFormLoading]);

  return {
    // The form data with all the keys and values
    formData,
    // An object with formData keys as keys and errors as values
    errors,
    // Return the function to allow manipulating errors (e.g. based on BE responses)
    setErrors,
    // A boolean value indicating whether the form is loading
    isFormLoading,
    // Return the function to allow manipulating the form loading state
    setFormLoading,
    // A function which gets executed on form submit
    // (handles the loading, validation and errors, so it is crucial to use it)
    handleSubmit,
    // A function allowing formData manipulation
    handleInputChange,
    // Return the input validators to allow further manipulation if they were declared inline
    validators,
  };
}

export default useForm;

import { useCallback, useState } from 'react';
import { compareAsc } from 'date-fns';
import EventService from 'api/service/events';
import { mapFiltersToSearchParams } from 'views/public/Events/utils/filters';
import logger from 'utils/logger';

/**
 __useEvents__ is used for handling the events API endpoint and apply a front end logic
 to the result. The hook handles re-fetching the data, formatting/mapping it if necessary
 and operating with the "next page" API links for fetching more past events.

 TODO: This hook really needs rebuilding. After BE split the past and active events...
 TODO: ...useEvents should be made somehow similar to useMembers
*/
function useEvents(filters = null) {
  // A state for storing event data
  const [events, setEvents] = useState({
    application: [],
    upcoming: [],
    past: [],
    inProgress: [],
  });
  // A state for storing event loading... state
  const [eventsLoading, setEventsLoading] = useState({
    application: true,
    upcoming: true,
    past: true,
    inProgress: true,
  });
  const [morePastEventsLoading, setMorePastEventsLoading] = useState(false);
  // morePastEventsLink is null if there is no more events to fetch
  const [morePastEventsLink, setMorePastEventsLink] = useState(null);

  const toggleEventsLoading = useCallback(
    // This function can be called without arguments. It then just sets all to false
    (areLoading = false) => {
      // Only toggle the loading state if it differs from the current one
      setEventsLoading((prevState) => (prevState.application === areLoading ? prevState : {
        application: areLoading,
        upcoming: areLoading,
        past: areLoading,
        inProgress: areLoading,
      }));
    },
    [setEventsLoading],
  );

  // This function splits the response into four different type of events depending on their dates
  const formatAndSetEvents = useCallback(
    (unorderedEvents, shouldAdd = false) => {
      const today = new Date();
      const formattedEvents = {
        application: [],
        upcoming: [],
        past: [],
        inProgress: [],
      };
      unorderedEvents?.active_events?.forEach((eventInstance) => {
        const startDate = new Date(eventInstance.start_date);
        const applicationDeadline = new Date(eventInstance.application_deadline);
        // Add to in progress events if an active event start date is older than the current date
        if (compareAsc(startDate, today) < 1) {
          formattedEvents.inProgress.push(eventInstance);
          return;
        }
        // Add to upcoming if an active event's application deadline is older than the current date
        if (compareAsc(applicationDeadline, today) < 0) {
          formattedEvents.upcoming.push(eventInstance);
          return;
        }
        // Otherwise add to open for applications
        formattedEvents.application.push(eventInstance);
      });
      unorderedEvents?.past_events.data?.forEach((eventInstance) => {
        formattedEvents.past.push(eventInstance);
      });

      // If shouldAdd is true, the results should be concatenated with the current state
      setEvents((prevState) => (shouldAdd ? {
        application: [
          ...prevState.application,
          ...formattedEvents.application,
        ],
        upcoming: [
          ...prevState.upcoming,
          ...formattedEvents.upcoming,
        ],
        past: [
          ...prevState.past,
          ...formattedEvents.past,
        ],
        inProgress: [
          ...prevState.inProgress,
          ...formattedEvents.inProgress,
        ],
      } : formattedEvents));
      setMorePastEventsLink(unorderedEvents.past_events.next_page_url);
    },
    [setEvents, setMorePastEventsLink],
  );

  const loadMorePastEvents = () => {
    // if there is actually a link for that, fetch more past events
    if (morePastEventsLink) {
      setMorePastEventsLoading(true);
      EventService.getEvents({
        customGetEventsLink: morePastEventsLink,
        filters: filters ? mapFiltersToSearchParams(filters).api.toString() : undefined,
      }).then((response) => {
        setMorePastEventsLoading(false);
        formatAndSetEvents(response, true);
      })
        .catch((err) => {
          logger.log(err);
          setMorePastEventsLoading(false);
        });
    }
  };

  return {
    // A function that splits the response into
    // four different type of events depending on their dates
    formatAndSetEvents,
    // A function that changes the loading state of events
    toggleEventsLoading,
    // An object with four different types of events and their data
    events,
    // An object with four different types of events and their loading state
    eventsLoading,
    // A function used to set the loading state of different types of events
    setEventsLoading,
    // A function that attempts to fetch "more past events"
    loadMorePastEvents,
    // A boolean value indicating whether more past events are loading
    morePastEventsLoading,
    // A boolean value indicating whether there are any more past events ready to be fetched
    morePastEventsAvailable: !!morePastEventsLink,
  };
}

export default useEvents;

import { useEffect, useRef, useState } from 'react';

/**
 __useDebouncedSearch__ allows performing dynamic search with a delay/debounce.
*/
function useDebouncedSearch({ onSearchSubmit, onFilteredSearchSubmit, initialValue }) {
  const debounceTimer = useRef();
  const [debouncedSearchTerm, setDebouncedSearchTerm] = useState(() => initialValue || '');

  // If onSearchSubmit was not specified, use onFilteredSearchSubmit to search with an empty input.
  const onClearSearchSubmit = onSearchSubmit || onFilteredSearchSubmit;

  useEffect(() => () => {
    if (debounceTimer.current) {
      clearTimeout(debounceTimer.current); // reset the timeout on unmount
    }
  }, [debounceTimer]);

  const setValue = (val) => {
    if (debounceTimer.current) {
      clearTimeout(debounceTimer.current); // reset the timeout each time the input value is changed
    }
    const search = val ? onFilteredSearchSubmit : onClearSearchSubmit;
    setDebouncedSearchTerm(val);
    // only perform the actual search after 0.7s from the last state change
    debounceTimer.current = setTimeout(search, 700, val);
  };

  // if debouncedSearchTerm is undefined so was not specified, return it as an empty string
  return [debouncedSearchTerm, setValue];
}

export default useDebouncedSearch;

import { useEffect, useState } from 'react';

/**
 __useDynamicSearch__ allows performing dynamic search (e.g. for filtering) on every change
 if an input.
 */
function useDynamicSearch({ onSearchSubmit, onFilteredSearchSubmit }) {
  const [searchTerm, setSearchTerm] = useState('');

  useEffect(() => {
    // If onSearchSubmit was not specified, use onFilteredSearchSubmit to search with an empty input
    const onClearSearchSubmit = onSearchSubmit || onFilteredSearchSubmit;
    const search = searchTerm ? onFilteredSearchSubmit : onClearSearchSubmit;
    search(searchTerm);
  }, [searchTerm, onSearchSubmit, onFilteredSearchSubmit]);

  return [searchTerm, setSearchTerm];
}

export default useDynamicSearch;

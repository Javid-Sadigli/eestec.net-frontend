/* eslint-disable camelcase */

import React, { useEffect, useMemo, useState } from 'react';
import { useParams, useNavigate, Link } from 'react-router-dom';
import EventService from 'api/service/events';
import Button from 'components/Button/Button';
import EntityHeader from 'components/EntityHeader/EntityHeader';
import { format } from 'date-fns';
import StickyMenu from 'components/StickyMenu/StickyMenu';
import EntitySection from 'components/EntitySection/EntitySection';
import CardList from 'components/CardList/CardList';
import PersonCard from 'components/PersonCard/PersonCard';
import { membersGridSettings } from 'utils/grid';
import useEventRefs from 'views/public/Event/hooks/useEventRefs';
import { pluralize, removeNbsp, createParagraphs } from 'utils/misc';
import useDocumentTitle from 'hooks/useDocumentTitle';
import useMembers from 'hooks/useMembers';
import CircularProgress from '@mui/material/CircularProgress';
import Stack from '@mui/material/Stack';
import Tooltip from '@mui/material/Tooltip';
import VrpanoIcon from '@mui/icons-material/Vrpano';
import CameraAlt from '@mui/icons-material/CameraAlt';
import useEntityEdit from 'hooks/useEntityEdit';
import ConditionalInput from 'components/ConditionalInput/ConditionalInput';
import Div from 'utils/componentsWrappers/Div';
import TextInput from 'components/TextInput/TextInput';
import EditIcons from 'components/EditIcons/EditIcons';
import ImageFileInput from 'components/ImageFileInput/ImageFileInput';
import logger from 'utils/logger';
import { getBranchTypeAndName } from 'utils/branches';
import useResetDataOnLogout from 'hooks/useResetDataOnLogout';

import './Event.css';

const APPLICATION_FEATURE_ENABLED = false;

// formatOrganizingBranch uses a branch data to create a link to its page
const formatOrganizingBranch = ({ name, slug, branch_type }) => (
  <Link
    key={`branch-${slug}`}
    to={`/cities/${slug}`}
  >
    {getBranchTypeAndName(name, branch_type)}
  </Link>
);

// formatOrganizingEntity uses an entity (team/project) data to create a link to its page
// FUTURE: This should be a link when we have teams & projects
const formatOrganizingEntity = ({ name }) => (
  <span key={`entitiy-${name}`}>
    {name}
  </span>
);

// formatOrganizers uses the data about organizers of the event and puts it into an element
// which displays the organizers in a text and tooltip (the latter if the number of organizers
// is too big to display in a user-friendly way)
const formatOrganizers = (branches, entities) => {
  const jointLength = branches.length + entities.length;
  // just return nothing if there are no organizers
  if (!jointLength) {
    return null;
  }

  let result = [];
  if (jointLength > 3) {
    // if joint length is bigger than 3, put max 2 organizers (ideally one per each (branch/entity))
    // into the text and display the rest within the tooltip

    // extract the first elements of both arrays
    const [firstBranch, ...otherBranches] = branches;
    const [firstEntity, ...otherEntities] = entities;
    // push the first branch or the second of the entities if there are no branches whatsoever
    result.push(formatOrganizingBranch(firstBranch)
      || formatOrganizingEntity(otherEntities.shift()));
    // push the first entity or the second of the branches if there are no entities whatsoever
    result.push(formatOrganizingEntity(firstEntity)
      || formatOrganizingBranch(otherBranches.shift()));

    return (
      <>
        {result.reduce((prev, curr) => [prev, ', ', curr])}
        <Tooltip
          arrow
          title={(
            <div>
              {otherBranches.map(({ name, branch_type, slug }) => (
                <p key={`header-branch-tooltip-${slug}`}>
                  {getBranchTypeAndName(name, branch_type)}
                </p>
              ))}
              {otherEntities.map(({ name }) => (
                <p key={`header-branch-tooltip-${name}`}>
                  {name}
                </p>
              ))}
            </div>
          )}
        >
          <span>{` and ${otherEntities.length + otherBranches.length} more`}</span>
        </Tooltip>
      </>
    );
  }
  // if there are no more than 3 organizers, just display as text all of them
  result = [
    ...branches.map((branch) => formatOrganizingBranch(branch)),
    ...entities.map((entity) => formatOrganizingEntity(entity)),
  ];

  return result.reduce((prev, curr) => [prev, ', ', curr]);
};

/**
 __Event__ is a view used to display the Event page
 */
function Event() {
  const navigate = useNavigate();
  const { eventSlug } = useParams();
  const [eventData, setEventData] = useState();
  const [hasEditRights, setHasEditRights] = useState(false);
  const participants = useMembers(`/events/${eventSlug}/participants`);
  const organizers = useMembers(`/events/${eventSlug}/organizers`);
  useResetDataOnLogout({
    listingSetters: [
      participants.setMembers,
      organizers.setMembers,
    ],
    adminPropsSetters: [setHasEditRights],
  });
  const {
    setEntityEditFormData,
    toggleIsBeingEdited,
    onEntityEditSubmit,
    isEntityEditLoading,
    isEditingDisabled,
    isBeingEdited,
    entityEditFormData,
    isAnyBeingEdited,
    resetEntityEditFormData,
  } = useEntityEdit({
    apiServiceCall: (formData, requestArgs) => EventService.editEvent(
      eventSlug,
      formData,
      ...requestArgs,
    ),
    data: eventData,
    setData: setEventData,
  });
  const eventRefs = useEventRefs();
  useDocumentTitle(eventData?.name);

  const numberOfOrganizers = useMemo(() => (
    eventData ? eventData.entities.length + eventData.branches.length : 0
  ), [eventData]);

  // this effect fetches the event data on the page mount
  useEffect(() => {
    // Creates an AbortController which aborts ongoing requests if component is unmounted
    const abortController = new AbortController();

    // Get branch information
    EventService.getEvent(eventSlug)
      .then((response) => {
        setEventData({
          ...response.event,
          description: removeNbsp(response.event.description),
          branches: response.event.branches.map((branch) => ({
            name: branch.name,
            slug: branch.slug,
            branch_type: branch?.branch_type,
            url: `/cities/${branch.slug},`,
          })),
          entities: response.event.entities.map((entity) => ({
            name: entity.name,
            slug: entity.slug,
          })),
        });
        setHasEditRights(response.can_edit);
      })
      .catch((err) => {
        if (err?.status !== 404) {
          logger.log(err);
        }
        navigate('/404', { replace: true });
      });

    // during unmount of the component abort the ongoing requests to avoid memory leak
    return () => abortController.abort();
  }, [navigate, eventSlug, setHasEditRights, setEventData]);

  // stickyMenuElements are used to organize data for the sticky menu
  // TODO: maybe it would be possible to create a hook like useStickyMenu
  //  to have all the logic behind stickyMenu and needed refs in one place
  const stickyMenuElements = [
    {
      title: 'Description',
      target: eventRefs.description,
    },
    {
      title: 'Organizers',
      target: eventRefs.organizers,
      isEmpty: !organizers.members.length,
    },
    {
      title: 'Participants',
      target: eventRefs.participants,
      isEmpty: !participants.members.length,
    },
  ].filter((element) => !element.isEmpty);

  // format entity header details fields
  const detailsFields = [
    {
      title: '',
      value: '',
    },
    {
      title: 'Dates',
      value: '',
    },
  ];
  if (eventData) {
    detailsFields[0].title = pluralize('Organizer', numberOfOrganizers);
    detailsFields[0].value = formatOrganizers(eventData.branches, eventData.entities);
    detailsFields[1].value = `${format(new Date(eventData.start_date), 'dd.MM.yyyy')} - ${format(new Date(eventData.end_date), 'dd.MM.yyyy')}`;
  }

  const stickyMenuButton = (
    APPLICATION_FEATURE_ENABLED && new Date(eventData?.application_deadline) > new Date()
  ) ? (
    <Button
      onClick={() => logger.log('TODO: Application')}
      className="primary-button"
    >
      Apply now
    </Button>
    ) : null;

  return (
    <div className="entity-wrapper" id="event">
      <EntityHeader
        title={eventData?.name}
        backgroundImage={eventData?.banner_path}
        subtitle={eventData?.event_type.name}
        detailsFields={detailsFields}
        blurred={!!eventData}
        className="event-header"
      >
        {(!!eventData && hasEditRights) && (
          <>
            <ImageFileInput
              onChange={(file) => onEntityEditSubmit(['banner_path'], true, file, [true, false])}
              title="Change the banner picture (max. 2MB)"
              disabled={isEditingDisabled || isAnyBeingEdited}
              isLoading={isEntityEditLoading?.banner_path}
              Icon={VrpanoIcon}
            />
            <ImageFileInput
              onChange={(file) => onEntityEditSubmit(['profile_picture_path'], true, file, [false, true])}
              title="Change the square picture (max. 2MB)"
              disabled={isEditingDisabled || isAnyBeingEdited}
              isLoading={isEntityEditLoading?.profile_picture_path}
              Icon={CameraAlt}
            />
          </>
        )}
      </EntityHeader>
      {eventData && (
        <StickyMenu
          elements={stickyMenuElements}
          rightElement={stickyMenuButton}
        />
      )}
      <div className="entity-inner-content">
        {eventData ? (
          <>
            <EntitySection
              title="Description"
              className="entity-section description"
              innerRef={eventRefs.description}
            >
              <>
                <EditIcons
                  className="entity-section-edit-icons description-icons"
                  isBeingEdited={isBeingEdited?.description}
                  onEditSubmit={() => onEntityEditSubmit(['description'])}
                  onEditCancel={resetEntityEditFormData}
                  onEditStart={() => toggleIsBeingEdited('description')}
                  isLoading={isEntityEditLoading?.description}
                  shouldDisplay={hasEditRights}
                  shouldBeVisible={!isAnyBeingEdited}
                />
                <ConditionalInput
                  onChange={(e) => setEntityEditFormData('description', e.target.value)}
                  staticComponent={{
                    element: Div,
                    props: {
                      className: 'description-conditional-input',
                    },
                  }}
                  editableContent={entityEditFormData?.description}
                  content={createParagraphs(eventData.description, 'event-description')}
                  condition={isBeingEdited?.description}
                  inputComponent={{
                    element: TextInput,
                    props: {
                      multiline: true,
                      placeholder: 'Tell us about this event!',
                      className: 'description-conditional-input',
                    },
                  }}
                  isAllowedToEdit={hasEditRights}
                  isInputDisabled={isEditingDisabled}
                />
                <Stack
                  direction={{ xs: 'column', sm: 'row' }}
                  justifyContent="space-around"
                  alignItems="center"
                  spacing={1}
                >
                  <div className="event-description-details">
                    <p>Max participants</p>
                    <p>{eventData.max_participants}</p>
                  </div>
                  <div className="event-description-details">
                    <p>Application deadline</p>
                    <p>{eventData.application_deadline}</p>
                  </div>
                  <div className="event-description-details">
                    <p>Participation fee</p>
                    <p>
                      {eventData.participation_fee === 0 ? 'FREE' : `€${eventData.participation_fee}`}
                    </p>
                  </div>
                </Stack>
              </>
            </EntitySection>
            <EntitySection
              title="Organizing Committee"
              innerRef={eventRefs.organizers}
              className="members-section organizers"
              headingClassName="card-subheading"
              visibilityCondition={!!organizers.members.length}
            >
              <CardList
                loading={organizers.membersLoading}
                emptyMessage="No Organizing Committee found for this event"
                items={organizers.members}
                showMoreButton={{
                  visibilityCondition: organizers.moreMembersAvailable,
                  onClick: organizers.getMoreMembers,
                  isLoading: organizers.membersLoading,
                  text: 'Show more organizers',
                }}
                cardType={PersonCard}
                gridSettings={membersGridSettings}
              />
            </EntitySection>
            <EntitySection
              title="Participants"
              innerRef={eventRefs.participants}
              className="members-section participants"
              headingClassName="card-subheading"
              visibilityCondition={!!participants.members.length}
            >
              <CardList
                loading={participants.membersLoading}
                emptyMessage="No participants found for this event"
                items={participants.members}
                showMoreButton={{
                  visibilityCondition: participants.moreMembersAvailable,
                  onClick: participants.getMoreMembers,
                  isLoading: participants.membersLoading,
                  text: 'Show more participants',
                }}
                cardType={PersonCard}
                gridSettings={membersGridSettings}
              />
            </EntitySection>
          </>
        ) : (
          <div className="entity-loader">
            <CircularProgress size={70} />
          </div>
        )}
      </div>
    </div>
  );
}

export default Event;

import { useRef } from 'react';

function useEventRefs() {
  /*
    TODO: This is disgusting. A single useRef can serve many references if you use useRef([])
    See here: https://stackoverflow.com/questions/54633690/how-can-i-use-multiple-refs-for-an-array-of-elements-with-hooks
  */
  const description = useRef(null);
  const participants = useRef(null);
  const organizers = useRef(null);

  return {
    description,
    participants,
    organizers,
  };
}

export default useEventRefs;

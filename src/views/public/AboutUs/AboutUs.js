import React, { useRef } from 'react';
import { Link } from 'react-router-dom';

import HeadlinedContent from 'components/HeadlinedContent/HeadlinedContent';
import SlidingElement from 'components/SlidingElement/SlidingElement';
import SeeMoreButton from 'components/SeeMoreButton/SeeMoreButton';
import useDocumentTitle from 'hooks/useDocumentTitle';
import Testimonials from 'views/public/AboutUs/components/Testimonials/Testimonials';
import History from 'views/public/AboutUs/components/History/History';
import historyData from 'utils/history';
import scroll from 'utils/scroll';
import mockTestimonials from 'utils/testimonials';

import 'views/public/AboutUs/AboutUs.css';

/**
 __AboutUs__ is a view used to display information in the About Us section
 */
function AboutUs() {
  const aboutUsRef = useRef(null);
  const ourHistoryRef = useRef(null);
  const testimonialsRef = useRef(null);
  const scrollToAboutUsSection = () => scroll(
    aboutUsRef,
    null,
    0,
    1000,
    'easeOutBack',
    1,
  );

  useDocumentTitle('About us');

  return (
    <>
      <div className="top-tabs-wrapper">
        <div className="about-us-top-tabs">
          <div>
            <Link to="#about-us" onClick={scrollToAboutUsSection}>
              <h1>
                <span className="light">About</span>
                <span>EESTEC</span>
              </h1>
            </Link>
          </div>
          <div>
            <Link to="/join-us"><span>Join us</span></Link>
          </div>
        </div>
      </div>
      <div className="section" id="about-us" ref={aboutUsRef}>
        <div>
          <SlidingElement>
            <HeadlinedContent
              title="Who are we?"
              variant="h2"
              fullWidthText
            >
              <p>
                We are EESTEC
                <br />
                Electrical Engineering STudents&apos; European assoCiation
              </p>
              <p>
                We are an apolitical, non-governmental and non-profit organization for EECS
                students at universities, institutes and schools of technology.
              </p>
            </HeadlinedContent>
          </SlidingElement>
          <SlidingElement slideRight>
            <HeadlinedContent
              title="What is our aim?"
              variant="h2"
              fullWidthText
              splitRight
            >
              <p>
                Our aim as EESTEC is developing international contacts as well as
                encouraging the exchange of ideas among EECS students.
              </p>
            </HeadlinedContent>
          </SlidingElement>
          <SlidingElement>
            <HeadlinedContent
              title="How do we achieve these goals?"
              variant="h2"
              fullWidthText
            >
              <p>
                We achieve our goals through professional workshops,
                cultural student exchanges and publications.
              </p>
            </HeadlinedContent>
          </SlidingElement>
          <SlidingElement slideRight>
            <HeadlinedContent
              title="What else do we do?"
              variant="h2"
              fullWidthText
              splitRight
            >
              <p>
                With our activities we create opportunities for
                students to develop their academic, professional and social lives.
              </p>
            </HeadlinedContent>
          </SlidingElement>
        </div>
        <SeeMoreButton target={ourHistoryRef} label="Our history" />
      </div>
      <div className="section" id="our-history" ref={ourHistoryRef}>
        <History
          items={historyData}
          scrollButtonTarget={testimonialsRef}
        />
      </div>
      <div className="section" id="testimonials" ref={testimonialsRef}>
        <Testimonials items={mockTestimonials} />
      </div>
    </>
  );
}

export default AboutUs;

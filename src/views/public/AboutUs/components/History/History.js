import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import Heading from 'components/Heading/Heading';
import SeeMoreButton from 'components/SeeMoreButton/SeeMoreButton';
import useIsPinned from 'hooks/useIsPinned';
import { elementsOverlap } from 'utils/dom';
import { createParagraphs } from 'utils/misc';
import scroll, { getElementPosition } from 'utils/scroll';

import 'views/public/AboutUs/components/History/History.css';

// repositionHistoryElement manipulates the CSS classes of HistoryElements in order
// to cover and uncover these elements whenever needed
const repositionHistoryElement = (ref, isPinned, wasLastUnpinBelowHistory, lastElement) => {
  const previousElement = ref?.current.previousSibling;
  if (previousElement?.classList.contains('pinned')) {
    if (isPinned) {
      previousElement.classList?.add('covered');
      if (!lastElement && !wasLastUnpinBelowHistory) {
        scroll(ref, getElementPosition(previousElement), 0, 100, 'linear');
      }
    } else if (!wasLastUnpinBelowHistory) {
      previousElement.classList?.remove('covered');
    }
  }
  if (isPinned && wasLastUnpinBelowHistory && !lastElement) {
    ref?.current.classList.add('covered');
  }
};

/**
 __HistoryElement__ is a single record of history presented in the History section of About Us.
 */
function HistoryElement({
  title,
  img,
  text,
  even,
  lastElement,
  ourHistoryHeadingRef,
  setHeadingAndLastElementOverlap,
}) {
  const historyElementRef = useRef();
  const [wasLastUnpinBelowHistory, setWasLastUnpinBelowHistory] = useState(false);
  const isPinned = useIsPinned(historyElementRef, () => {
    // sets the last unpin of this element based on the position of the scroll of the user
    // as depending on this boolean value, the element should be either snapped or considered
    // covered by other history elements
    if (historyElementRef) {
      setWasLastUnpinBelowHistory(window.scrollY >= getElementPosition(historyElementRef.current));
    }
  });
  useEffect(() => {
    if (lastElement) {
      // make sure the OUR HEADING heading disappears after the last element is visible
      setHeadingAndLastElementOverlap(elementsOverlap(ourHistoryHeadingRef, historyElementRef));
    }
    // reposition the history element on change of pin state
    repositionHistoryElement(historyElementRef, isPinned, wasLastUnpinBelowHistory, lastElement);
  }, [
    isPinned,
    ourHistoryHeadingRef,
    historyElementRef,
    wasLastUnpinBelowHistory,
    lastElement,
    setHeadingAndLastElementOverlap,
  ]);

  return (
    <div
      className={`history${isPinned ? ' pinned' : ''}`}
      ref={historyElementRef}
    >
      <div className="history-text">
        <Heading variant="h3" lineAfter>{title}</Heading>
        <div className="history-paragraphs">
          {createParagraphs(text, title, null, '\n')}
        </div>
      </div>
      <div className="history-images">
        <img src={img} alt={title} className={even ? 'even' : 'odd'} />
      </div>
    </div>
  );
}

HistoryElement.propTypes = {
  title: PropTypes.string.isRequired,
  img: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired,
  even: PropTypes.bool,
  lastElement: PropTypes.bool,
  ourHistoryHeadingRef: PropTypes.shape({ current: PropTypes.instanceOf(Element) }),
  setHeadingAndLastElementOverlap: PropTypes.func,
};

HistoryElement.defaultProps = {
  even: false,
  lastElement: false,
  ourHistoryHeadingRef: null,
  setHeadingAndLastElementOverlap: null,
};

/**
 __History__ is a component rendering the history section of About us
*/
function History({ items, scrollButtonTarget }) {
  // headingAndLastElementOverlap state is used to indicate the display condition
  // of the see more button which should not be visible anymore after going to the
  // last element AND manipulate the CSS positioning of the heading
  const [headingAndLastElementOverlap, setHeadingAndLastElementOverlap] = useState(false);
  const ourHistoryHeadingRef = useRef();

  let headingClasses = 'our-history-heading';
  headingClasses += headingAndLastElementOverlap ? ' relative' : '';
  return (
    <>
      <Heading
        variant="h2"
        className={headingClasses}
        innerRef={ourHistoryHeadingRef}
      >
        Our history
      </Heading>
      {!headingAndLastElementOverlap && (
        <SeeMoreButton target={scrollButtonTarget} label="Testimonials" />
      )}
      <div className="history-elements">
        {items.map(({ img, text, title }, index) => (
          <HistoryElement
            img={img}
            text={text}
            title={title}
            key={title}
            even={index % 2 === 0}
            lastElement={index === items.length - 1}
            ourHistoryHeadingRef={ourHistoryHeadingRef}
            setHeadingAndLastElementOverlap={setHeadingAndLastElementOverlap}
          />
        ))}
      </div>
    </>
  );
}

History.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    img: PropTypes.string,
    title: PropTypes.string,
    text: PropTypes.string,
  })).isRequired,
  scrollButtonTarget: PropTypes.shape({ current: PropTypes.instanceOf(Element) }).isRequired,
};

export default History;

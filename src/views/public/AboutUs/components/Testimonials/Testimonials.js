import React from 'react';
import PropTypes from 'prop-types';
import CarouselComponent from 'components/Carousel/Carousel';
import Heading from 'components/Heading/Heading';

import 'views/public/AboutUs/components/Testimonials/Testimonials.css';
import { createParagraphs } from '../../../../../utils/misc';

/**
 __Testimonials__ renders the carousel with testimonials on the About Us page
*/
function Testimonials({ items }) {
  return (
    <>
      <Heading variant="h2" styleVariant="h1" lineAfter>
        Testimonials
      </Heading>
      <CarouselComponent
        autoscroll={false}
        spacing="NONE"
        scrollDuration={1000}
        nativeSnapping={false}
        arrows
        containerClassName="testimonials-carousel"
      >
        {items.map(({
          id, img, name, text, position,
        }) => (
          <div key={id} className="testimonial">
            <img src={img} alt={name} />
            <div className="testimonial-text">
              <h3>
                <span className="testimonial-name">{name}</span>
                <span className="testimonial-separator">  |  </span>
                <span className="testimonial-position">{position}</span>
              </h3>
              {createParagraphs(text, name, null, '\n')}
            </div>
          </div>
        ))}
      </CarouselComponent>
    </>
  );
}

Testimonials.propTypes = {
  items: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number,
    img: PropTypes.string,
    name: PropTypes.string,
    position: PropTypes.string,
    text: PropTypes.string,
  })).isRequired,
};

export default Testimonials;

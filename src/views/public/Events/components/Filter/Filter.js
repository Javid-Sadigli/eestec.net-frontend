import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import Dropdown from 'components/Dropdown/Dropdown';
import useDebouncedSearch from 'hooks/useDebouncedSearch';
import FilteringTextInput from 'components/FilteringTextInput/FilteringTextInput';
import CheckboxInput from 'components/CheckboxInput/CheckboxInput';
import IconButton from '@mui/material/IconButton';
import FilterListIcon from '@mui/icons-material/FilterList';
import CloseIcon from '@mui/icons-material/Close';
import SkeletonCheckboxes from 'components/CheckboxInput/skeleton/SkeletonCheckboxes';
import SkeletonDropdown from 'components/Dropdown/skeleton/SkeletonDropdown';

import 'views/public/Events/components/Filter/Filter.css';

/**
 __Filter__ is the filtering box visible on the Events page. It accepts filters
 and filter setting function, together with information whether filtering should be disabled,
 e.g. during an ongoing BE call which includes filters.
 */
export default function Filter({ filters, setFilters, disabled }) {
  const [isMobileFilterExpanded, setIsMobileFilterExpanded] = useState(false);
  const [eventSearchTerm, setEventSearchTerm] = useDebouncedSearch({
    onFilteredSearchSubmit: (searchTerm) => {
      setFilters((prevState) => ({
        ...prevState,
        name: searchTerm,
      }));
    },
    initialValue: filters?.name,
  });

  // this effect locks scrolling when filters menu is expanded on mobiles
  useEffect(() => {
    if (isMobileFilterExpanded) {
      document.body.classList.add('locked-scroll');
    } else {
      document.body.classList.remove('locked-scroll');
    }
  }, [isMobileFilterExpanded]);

  const toggleMobileFilterExpanded = () => setIsMobileFilterExpanded((prevState) => !prevState);

  // onTypeClick handles the click event on the checkboxes with event types
  const onTypeClick = (idx) => {
    setFilters((prevState) => {
      const newType = [...prevState.type];
      newType[idx] = {
        ...newType[idx],
        enabled: !newType[idx].enabled,
      };
      return ({
        ...prevState,
        type: newType,
      });
    });
  };

  // onTypeClick handles the click event on the dropdown with sorting methods
  const onDropdownClick = (chosenOption) => {
    setFilters((prevState) => ({
      ...prevState,
      sort: {
        chosenOption,
        options: prevState.sort.options,
      },
    }));
  };

  const checkboxButtons = filters ? filters.type.map((type, typeIdx) => (
    <CheckboxInput
      label={type.name}
      key={`checkbox-button-${type.id}`}
      id={`checkbox-button-${type.id}`}
      checked={type.enabled}
      disabled={disabled}
      onChange={() => onTypeClick(typeIdx)}
    />
  )) : <SkeletonCheckboxes length={10} />;

  const sortingDropdown = filters ? (
    <Dropdown
      options={filters ? filters.sort.options : []}
      optionLabel="displayName"
      optionKey="identifier"
      id="event-sorting-dropdown"
      label={filters ? filters.sort.chosenOption.displayName : ''}
      onSelectClick={onDropdownClick}
      disabled={disabled}
    />
  ) : <SkeletonDropdown />;

  let filterClasses = 'filter';
  filterClasses += isMobileFilterExpanded ? ' mobile-filter-expanded' : '';

  let filterOptionsClasses = 'filter-options';
  filterOptionsClasses += disabled ? ' filter-input-disabled' : '';

  return (
    <div className={filterClasses}>
      <div className="filter-search">
        <FilteringTextInput
          onChange={(e) => setEventSearchTerm(e.target.value)}
          value={eventSearchTerm}
          title="Search events"
          placeholder="Search events"
        />
        <IconButton
          onClick={toggleMobileFilterExpanded}
          disableRipple
          title="Filter events"
          className="filtering-icon"
        >
          {isMobileFilterExpanded ? <CloseIcon /> : <FilterListIcon />}
        </IconButton>
      </div>
      <div className="filter-inner">
        <p className="filter-title">Filters</p>
        <div className="filter-section types">
          <p className="filter-subtitle">Type</p>
          <div className={filterOptionsClasses}>
            {checkboxButtons}
          </div>
        </div>
        <div className="filter-section sorting">
          <p className="filter-subtitle">Sort by</p>
          <div className={filterOptionsClasses}>
            {sortingDropdown}
          </div>
        </div>
      </div>
    </div>
  );
}

Filter.propTypes = {
  filters: PropTypes.shape({
    type: PropTypes.arrayOf(PropTypes.shape({
      id: PropTypes.number.isRequired,
      name: PropTypes.string.isRequired,
    })),
    sort: PropTypes.shape({
      chosenOption: PropTypes.shape({
        displayName: PropTypes.string.isRequired,
        identifier: PropTypes.string.isRequired,
      }).isRequired,
      options: PropTypes.arrayOf(PropTypes.shape({
        displayName: PropTypes.string.isRequired,
        identifier: PropTypes.string.isRequired,
      })).isRequired,
    }),
    name: PropTypes.string,
  }),
  setFilters: PropTypes.func.isRequired,
  disabled: PropTypes.bool.isRequired,
};

Filter.defaultProps = {
  filters: null,
};

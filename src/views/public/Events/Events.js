import React, {
  useContext,
  useEffect,
  useRef,
  useState,
  useCallback,
} from 'react';
import EventService from 'api/service/events';
import CardList from 'components/CardList/CardList';
import Heading from 'components/Heading/Heading';
import useEvents from 'hooks/useEvents';
import useDocumentTitle from 'hooks/useDocumentTitle';
import EventCard from 'components/EventCard/EventCard';
import Filter from 'views/public/Events/components/Filter/Filter';
import {
  mapFiltersToSearchParams,
  mapSearchParamsToFilter,
} from 'views/public/Events/utils/filters';
import logger from 'utils/logger';
import { useSearchParams } from 'react-router-dom';
import useNavigationListener from 'hooks/useNavigationListener';
import ToastContext from 'context/ToastContext';
import { eventWithFiltersGridSettings } from 'utils/grid';

import 'views/public/Events/Events.css';

/**
 __Events__ is a view used to display the Events page
 */
function Events() {
  useDocumentTitle('Events');
  const isInitialMount = useRef(true);
  const { setToastContent } = useContext(ToastContext);
  const [searchParams, setSearchParams] = useSearchParams();

  const [eventTypes, setEventTypes] = useState([]);
  const [filters, setFilters] = useState(null);
  const [shouldSetSearchParams, setShouldSetSearchParams] = useState(true);
  const {
    loadMorePastEvents,
    formatAndSetEvents,
    events,
    eventsLoading,
    toggleEventsLoading,
    morePastEventsLoading,
    morePastEventsAvailable,
  } = useEvents(filters);

  const navigationListenerCallback = useCallback(({ search }) => {
    // if eventTypes is empty, the pop state comes from another route so do not execute
    // the navigation listener
    if (eventTypes.length > 0) {
      setShouldSetSearchParams(false);
      // set filters to match the new search params whenever browser buttons are used
      setFilters((prevState) => ({
        ...mapSearchParamsToFilter(eventTypes, new URLSearchParams(search)),
        name: prevState.name,
      }));
    }
  }, [eventTypes, setFilters, setShouldSetSearchParams]);
  useNavigationListener(navigationListenerCallback);

  /*
    TODO: Future - this can be done a bit better than it is now.
     Right now the call for getting events is executed only after we get the event types.
     While it makes some sense, we could save around 1-2 sec of the user experience if
     both of the calls are made simultaneously and wrapped in Promise.all/axios.all
  */
  useEffect(() => {
    if (eventTypes.length === 0) {
      EventService.getEventTypes()
        .then(({ data }) => {
          setFilters(mapSearchParamsToFilter(data, searchParams));
          setEventTypes(data);
        })
        .catch((err) => {
          logger.log(err);
          setToastContent(
            'Something went wrong with gathering the event types. Please reload the page.',
            'error',
          );
        });
    }
  }, [eventTypes, searchParams, setToastContent, setFilters, setEventTypes]);

  // this effect initiates events refetch on each change in filters
  useEffect(() => {
    toggleEventsLoading(true);
  }, [filters, toggleEventsLoading]);

  // this effect fetches events when filters are loaded/changed and event loading is set to true
  useEffect(() => {
    if (filters && eventsLoading.application) {
      const filterString = mapFiltersToSearchParams(filters);
      EventService.getEvents({
        filters: filterString.api.toString(),
      }).then((response) => {
        const currentParamsString = searchParams.toString();
        const newParamsString = filterString.searchParams.toString();
        if (isInitialMount.current) {
          // any successful load of an event means that the page fully mounted and next
          // executions of this effect will not be done on the initial mount, so set the
          // value to false
          isInitialMount.current = false;
        } else if (
          shouldSetSearchParams
          && newParamsString !== currentParamsString
          && !(newParamsString === 'sort=-start_date' && currentParamsString === '')
        ) {
          // set the new search params only if this call was not for an initial mount
          // and the current search params are different than the one from the filter
          setSearchParams(filterString.searchParams);
        } else {
          // otherwise enable future setting of search params
          setShouldSetSearchParams(true);
        }
        formatAndSetEvents(response);
      })
        .catch((err) => logger.log(err))
        .finally(toggleEventsLoading);
    }
  }, [
    searchParams,
    eventsLoading,
    filters,
    setSearchParams,
    toggleEventsLoading,
    shouldSetSearchParams,
    formatAndSetEvents,
  ]);

  return (
    <div id="events" className="events-filter-wrapper section">
      <div className="filter-wrapper">
        <Filter
          filters={filters}
          setFilters={setFilters}
          disabled={eventsLoading.application}
        />
      </div>
      <div className="events-wrapper">
        {(
          !Object.values(eventsLoading).some((eventLoading) => eventLoading)
          && !Object.values(events).some((eventValues) => !!eventValues.length)
        ) && (
          <p className="no-cards dimmed">
            There are no events matching your criteria.
          </p>
        )}
        {(eventsLoading.application || !!events.application.length) && (
          <div className="event-group">
            <Heading variant="h2" lineAfter>Open for application</Heading>
            <CardList
              items={events.application}
              emptyMessage="There are no events open for application"
              loading={eventsLoading.application}
              cardButtonsText="Apply now"
              cardType={EventCard}
              gridSettings={eventWithFiltersGridSettings}
            />
          </div>
        )}
        {(eventsLoading.upcoming || !!events.upcoming.length) && (
          <div className="event-group">
            <Heading variant="h2" lineAfter>Upcoming</Heading>
            <CardList
              items={events.upcoming}
              emptyMessage="There are no upcoming events"
              loading={eventsLoading.upcoming}
              cardType={EventCard}
              gridSettings={eventWithFiltersGridSettings}
            />
          </div>
        )}
        {(eventsLoading.inProgress || !!events.inProgress.length) && (
          <div className="event-group">
            <Heading variant="h2" lineAfter>In progress</Heading>
            <CardList
              items={events.inProgress}
              emptyMessage="There are no events in progress"
              loading={eventsLoading.inProgress}
              cardType={EventCard}
              gridSettings={eventWithFiltersGridSettings}
            />
          </div>
        )}
        {(eventsLoading.past || !!events.past.length) && (
          <div className="event-group">
            <Heading variant="h2" lineAfter>Past events</Heading>
            <CardList
              items={events.past}
              emptyMessage="There are no past events"
              loading={eventsLoading.past}
              cardType={EventCard}
              gridSettings={eventWithFiltersGridSettings}
              showMoreButton={{
                visibilityCondition: morePastEventsAvailable,
                onClick: loadMorePastEvents,
                isLoading: morePastEventsLoading,
                text: 'Show more past events',
              }}
            />
          </div>
        )}
      </div>
    </div>
  );
}

export default Events;

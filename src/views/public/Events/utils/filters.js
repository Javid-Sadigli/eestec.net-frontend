/*
 This file contains all logic behind events filtering on the Events page.

 In the future there may be a need to use this logic in different places,
 this file should be placed somewhere else then.
*/

// eventFilters are the default filters that could be just hardcoded
export const eventFilters = {
  sort: {
    chosenOption: {
      displayName: 'Date (newest first)',
      identifier: '-start_date',
    },
    options: [
      {
        displayName: 'Name (A-Z)',
        identifier: 'name',
      },
      {
        displayName: 'Name (Z-A)',
        identifier: '-name',
      },
      {
        displayName: 'Date (newest first)',
        identifier: '-start_date',
      },
      {
        displayName: 'Date (oldest first)',
        identifier: 'start_date',
      },
    ],
  },
  name: '',
};

// filterFormatter is an object which takes the filter type as a key and an appropriate
// filtering option as a value
const filterFormatter = {
  name: (v) => (v ? {
    key: 'filter[search]',
    value: v,
  } : null),
  sort: (v) => (v.chosenOption.identifier ? {
    key: 'sort',
    value: v.chosenOption.identifier,
  } : null),
  type: (v) => {
    const filter = [];
    v.forEach((value) => {
      if (value.enabled) {
        filter.push(value.id);
      }
    });
    return filter.length > 0 ? {
      key: 'filter[event_type_id]',
      value: filter.join(','),
    } : null;
  },
};

// mapFiltersToSearchParams takes the filters object and builds the searchParams query string
export const mapFiltersToSearchParams = (filters) => {
  const apiSearchParams = new URLSearchParams();
  const searchParams = new URLSearchParams();
  Object.entries(filters).forEach(([key, value]) => {
    const filter = filterFormatter[key](value);
    if (filter) {
      apiSearchParams.set(filter.key, filter.value);
      if (key !== 'name') {
        searchParams.set(key, filter.value);
      }
    }
  });
  return {
    // apiSearchParams are formatted in a way the BE will accept
    api: apiSearchParams,
    // searchParams are formatted in a way they should be presented in the page URLs
    searchParams,
  };
};

// mapSearchParamsToFilter takes the the searchParams query string and builds the filters object
export const mapSearchParamsToFilter = (eventTypes, searchParams) => {
  const mappedEventTypes = eventTypes.map((eventType) => ({
    ...eventType,
    enabled: false,
  }));
  if (!searchParams.toString()) {
    return {
      ...eventFilters,
      type: mappedEventTypes,
    };
  }
  // creating a deep copy so that nested properties are not shared
  const paramFilters = {
    ...JSON.parse(JSON.stringify(eventFilters)),
    type: mappedEventTypes,
  };

  const searchParamsFormatter = {
    // name: (v) => {
    //   paramFilters.name = v;
    // },
    sort: (v) => {
      const chosenOption = paramFilters.sort.options.find((option) => option.identifier === v);
      if (chosenOption) {
        paramFilters.sort.chosenOption = chosenOption;
      }
    },
    type: (v) => {
      const filteredTypes = v.split(',');
      if (filteredTypes.length > 0) {
        paramFilters.type.forEach((filter, index) => {
          if (filteredTypes.includes(filter.id.toString())) {
            paramFilters.type[index].enabled = true;
          }
        });
      }
    },
  };

  searchParams.forEach((value, key) => {
    const func = searchParamsFormatter[key];
    if (func) {
      func(value);
    }
  });

  return paramFilters;
};

/* eslint-disable camelcase */
import React, {
  useCallback, useEffect, useRef, useState,
} from 'react';
import BranchCard from 'views/public/Branches/components/BranchCard/BranchCard';
import useDocumentTitle from 'hooks/useDocumentTitle';
import BranchService from 'api/service/branches';
import CardList from 'components/CardList/CardList';
import { branchesGridSettings } from 'utils/grid';
import FilteringTextInput from 'components/FilteringTextInput/FilteringTextInput';
import useDynamicSearch from 'hooks/useDynamicSearch';
import MapComponent from 'components/Map/MapComponent';
import logger from 'utils/logger';

import './Branches.css';

// filter the branches based on its name, type and country using the user input
const filterBranch = ({ name, type, country }, query) => (
  name?.toLowerCase().includes(query)
  || type?.toLowerCase().includes(query)
  || country?.toLowerCase().includes(query)
);

/**
 __Branches__ is a view used to display the Cities page
 */
function Branches() {
  const citiesRef = useRef(null);
  const [branches, setBranches] = useState([]);
  const [filteredBranches, setFilteredBranches] = useState([]);
  const filterBranches = useCallback((query) => {
    const trimmedQuery = query.trim().toLowerCase();
    setFilteredBranches(branches.filter((branch) => filterBranch(branch, trimmedQuery)));
  }, [branches]);
  const [filterTerm, setFilterTerm] = useDynamicSearch({
    onFilteredSearchSubmit: filterBranches,
  });
  useDocumentTitle('Cities');

  // this effect fetches branches on mount
  useEffect(() => {
    BranchService.getBranches()
      .then((response) => {
        const formattedResponse = response.map(({
          name, slug, profile_picture_path, branch_type, country,
        }) => ({
          name,
          slug,
          backgroundImage: profile_picture_path,
          country,
          type: branch_type?.name,
        })).sort((a, b) => a.name.localeCompare(b.name));
        setBranches(formattedResponse);
        setFilteredBranches(formattedResponse);
      })
      .catch((err) => logger.log(err));
  }, [setBranches, setFilteredBranches]);

  return (
    <div id="branches">
      <MapComponent buttonRef={citiesRef} buttonTitle="Browse branches" />
      <div className="section" ref={citiesRef}>
        <h1 className="info-title">Cities</h1>
        {branches.length > 0 && (
          <FilteringTextInput
            value={filterTerm}
            onChange={(e) => setFilterTerm(e.target.value)}
            title="Filter cities"
            placeholder="Search cities"
          />
        )}
        <CardList
          loading={!branches.length}
          emptyMessage="No branches matching your criteria"
          items={filteredBranches}
          cardType={BranchCard}
          gridSettings={branchesGridSettings}
        />
      </div>
    </div>
  );
}

export default Branches;

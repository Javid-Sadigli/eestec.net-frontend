import React from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';

import 'views/public/Branches/components/BranchCard/BranchCard.css';

/**
 __BranchCard__ is a variant of [`Card`](./?path=/docs/components-cards-card)
 which is used for describing a branch - its name, type and picture.
*/
function BranchCard({ item }) {
  const {
    name,
    type,
    slug,
    backgroundImage,
  } = item;

  return (
    <Link
      className={`branch-card ${type.toLowerCase()}`}
      id={slug}
      style={backgroundImage ? { backgroundImage: `url(${backgroundImage})` } : null}
      to={`/cities/${slug}`}
    >
      <div className="branch-card-captions">
        <p>{name}</p>
        <p>{type}</p>
      </div>
    </Link>
  );
}

BranchCard.propTypes = {
  /**
   An object containing all the necessary information for a branch to be displayed
   as a `BranchCard`.
   If `backgroundImage` is not specified, a default placeholder picture is used.
  */
  item: PropTypes.shape({
    name: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
    slug: PropTypes.string.isRequired,
    backgroundImage: PropTypes.string,
  }).isRequired,
};

export default BranchCard;

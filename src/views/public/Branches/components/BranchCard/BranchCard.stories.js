/* eslint-disable no-alert */
import React from 'react';
import BranchCard from 'views/public/Branches/components/BranchCard/BranchCard';
import { withRouter } from 'storybook-addon-react-router-v6';

export default {
  title: 'Components/Cards/BranchCard',
  component: BranchCard,
  decorators: [withRouter],
  args: {
    item: {
      name: 'Waikiki',
      slug: 'waikiki',
      type: 'Local Committee',
    },
  },
};

function Template(args) {
  return (
    <BranchCard {...args} />
  );
}

export const Default = Template.bind({});

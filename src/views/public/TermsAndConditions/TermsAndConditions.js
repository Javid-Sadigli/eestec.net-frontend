import React from 'react';
import HeadlinedContent from 'components/HeadlinedContent/HeadlinedContent';
import useDocumentTitle from 'hooks/useDocumentTitle';
import 'views/public/TermsAndConditions/TermsAndConditions.css';

/**
 __TermsAndConditions__ is the view used to display the Terms And Conditions page
*/
export default function TermsAndConditions() {
  useDocumentTitle('Terms and Conditions');

  return (
    <div className="info-page">
      <h1 className="info-title">Terms and conditions</h1>
      <div className="section">
        <HeadlinedContent
          className="HeadlinedContent"
          title="Why general"
          variant="h1"
          fullWidthText
        >
          <p>
            In EESTEC we need to collect some data about you in order to serve
            you better by storing your preferences online and
            to personalise your experience on our website.
          </p>
          <ul>
            <li>
              <b>Personal and contact information</b>
              <li>
                What?
                <li>
                  Name, gender, e-mail, and phone number;
                  to be able to contact you and organise your presence in the network.
                </li>
              </li>
              <li>
                Why?
                <li>
                  So that we manage your presence and interact within the network.
                </li>
              </li>
            </li>
            <li>
              <b>EESTEC-related information</b>
              <li>
                What?
                <li>
                  Branch, role, and events attended.
                </li>
              </li>
              <li>
                Why?
                <li>
                  To keep EESTEC running fair and organised.
                </li>
              </li>
            </li>
            <li>
              <b>Audio/Video files</b>
              <li>
                What?
                <li>Pictures, videos, and recordings at events.</li>
              </li>
              <li>
                Why?
                <li>
                  For promotion of commercial & non-commercial nature,
                  and to keep good memories safe.
                </li>
              </li>
            </li>
          </ul>

        </HeadlinedContent>
        <HeadlinedContent
          className="HeadlinedContent"
          title="Want to learn more?
          "
          variant="h1"
          fullWidthText
        >
          <p>
            Inside the
            {' '}
            <a target="_blank" rel="noreferrer" href="https://docs.google.com/document/d/1YjqZxpQKtxB7Das-oqejtXeKp917svCvsrAsfqJZZLc/edit?usp=sharing">GDPR Guide</a>
            {' '}
            you can find more about our data collection policy.
          </p>
        </HeadlinedContent>
        <HeadlinedContent
          className="HeadlinedContent"
          title="Where is data saved"
          variant="h1"
          fullWidthText
        >
          <p>
            Individual’s information is saved inside of a European Union country as part of
            the Association’s IT infrastructure or in third-party (mainly Google’s)
            servers which are protected by the European Union’s regulations.
            Individuals will be informed if the Association’s IT infrastructure
            is moved outside of the European Union.
          </p>
        </HeadlinedContent>
        <HeadlinedContent
          className="HeadlinedContent"
          title="Who has access rights"
          variant="h1"
          fullWidthText
        >
          <p>
            Inside and outside of EESTEC different entities
            can have access to the data collected as described in this Terms & Conditions.
            Below you can find further details regarding the data rights.
          </p>
        </HeadlinedContent>
        <table>
          <tbody>
            <tr>
              <td colSpan="6" rowSpan="1">
                <p className="table-title"><span>Who to approach</span></p>
              </td>
            </tr>
            <tr>
              <td>
                <p><div className="table-cell left">Regarding…</div></p>
              </td>
              <td>
                <p><div className="table-cell">Right to Access Personal Data</div></p>
              </td>
              <td>
                <p><div className="table-cell">Right to be Forgotten</div></p>
              </td>
              <td>
                <p><div className="table-cell">Right of Data Portability</div></p>
              </td>
              <td>
                <p><div className="table-cell">Right to Rectification</div></p>
              </td>
              <td>
                <p><div className="table-cell">Right to Object</div></p>
              </td>
            </tr>
            <tr>
              <td>
                <p><div className="table-cell left">Board of the Association</div></p>
              </td>
              <td>
                <p><div className="table-cell">✔</div></p>
              </td>
              <td>
                <p><div className="table-cell">✔</div></p>
              </td>
              <td>
                <p><div className="table-cell">✔</div></p>
              </td>
              <td>
                <p><div className="table-cell">✔</div></p>
              </td>
              <td>
                <p><div className="table-cell">✔</div></p>
              </td>
            </tr>
            <tr>
              <td>
                <p><div className="table-cell left">Teams, Projects, Organising Committees</div></p>
              </td>
              <td>
                <p><div className="table-cell">✔</div></p>
              </td>
              <td>
                <p><div className="table-cell">✔</div></p>
              </td>
              <td>
                <p><div className="table-cell">✔</div></p>
              </td>
              <td>
                <p><div className="table-cell">✖</div></p>
              </td>
              <td>
                <p><div className="table-cell">✔</div></p>
              </td>
            </tr>
            <tr>
              <td>
                <p><div className="table-cell left">Third-Party software partners</div></p>
              </td>
              <td>
                <p><div className="table-cell">✔</div></p>
              </td>
              <td>
                <p><div className="table-cell">✖</div></p>
              </td>
              <td>
                <p><div className="table-cell">✔</div></p>
              </td>
              <td>
                <p><div className="table-cell">✖</div></p>
              </td>
              <td>
                <p><div className="table-cell">✖</div></p>
              </td>
            </tr>
            <tr>
              <td>
                <p><div className="table-cell left">Third-Party corporate partners</div></p>
              </td>
              <td>
                <p><div className="table-cell">✔</div></p>
              </td>
              <td>
                <p><div className="table-cell">✖</div></p>
              </td>
              <td>
                <p><div className="table-cell">✔</div></p>
              </td>
              <td>
                <p><div className="table-cell">✖</div></p>
              </td>
              <td>
                <p><div className="table-cell">✔</div></p>
              </td>
            </tr>
          </tbody>
        </table>
        <HeadlinedContent
          className="HeadlinedContent"
          title="How long is my data stored?"
          variant="h1"
          fullWidthText
        >
          <p>
            EESTEC/your branch will store an individual’s information for a
            limited period after the individual’s request.
            <br />
            <br />
            The suggested limited timeframe ranges from one (1) up to three (3) years,
            depending on the data scope.
          </p>
        </HeadlinedContent>
        <HeadlinedContent
          className="HeadlinedContent"
          title="What about any changes to the existing document?"
          variant="h1"
          fullWidthText
        >
          <p>
            If required by law, the data processors will
            provide you with notice of any changes in these Terms and Conditions
            or the other documentation referred to in them by posting an updated
            version of these Terms and Conditions to the mailing list or other
            such documentationon the website with a new effective date stated
            at the beginning of them.
          </p>
        </HeadlinedContent>
        <HeadlinedContent
          className="HeadlinedContent"
          title="Additional data collected?"
          variant="h1"
          fullWidthText
        >
          <p>
            In order for the website to function properly,
            cookies are being used. For more information about the EESTEC cookies,
            please see the
            {' '}
            <a target="_blank" rel="noreferrer" href="https://docs.google.com/document/d/1ZmaES94kSzoKout0X-Cq1ufcw_wvplxJclyvNPBMOJ8/edit?usp=sharing">Cookies Guide</a>
            .
          </p>
        </HeadlinedContent>
      </div>
    </div>
  );
}

import React from 'react';
import HeadlinedContent from 'components/HeadlinedContent/HeadlinedContent';
import useDocumentTitle from 'hooks/useDocumentTitle';

/**
 __Documents__ is the view used to display the Official Documents page
 */
function Documents() {
  useDocumentTitle('Official Documents');

  return (
    <div className="info-page">
      <h1 className="info-title">Official Documents</h1>
      <div className="section">
        <HeadlinedContent title="Statutes" variant="h1" fullWidthText>
          <p>
            The Statutes is the document which defines the most fundamental
            aspects of EESTEC, such as its aim. The seat of the Αssociation is
            located in Zurich (Switzerland).
          </p>
          <p>Current Statutes:</p>
          <p>
            <a href="https://drive.google.com/file/d/13zueJDMqlsONGRcgwp4GupQFfW6HAYY8/view">
              Translation of Statutes (English - May 2022)
            </a>
          </p>
        </HeadlinedContent>
        <HeadlinedContent title="Rules of Procedure" variant="h1" fullWidthText>
          <p>
            The Rules of Procedure (RoP) are made to guide the bodies of EESTEC
            in their work. They are a supplement to the Statutes and a summary
            of what has shown to be the most appropriate measure, in order to
            achieve the aim of EESTEC.
          </p>
          <p>Current Rules of Procedure:</p>
          <p>
            <a href="https://drive.google.com/file/d/19-_OESuR7yelqAtGpfxwzRkDcyE-P2Sx/view">
              Rules of Procedure (May 2023)
            </a>
          </p>
        </HeadlinedContent>
        <HeadlinedContent title="Strategic Plan" variant="h1" fullWidthText>
          <p>
            The Strategic Plan is a formalized road-map that describes where
            EESTEC is going over the next few years and how it is going to get
            there. The Strategic Plan brings focus and direction to our
            organization and sets clear guidelines for our future actions.
          </p>
          <p>
            <a href="https://drive.google.com/file/d/1EXmAN0p8ElFdCPVXzeTNNi-ma_4RUS_d/view">
              Strategic plan 2018-2023
            </a>
          </p>
        </HeadlinedContent>
        <HeadlinedContent title="Policy Papers" variant="h1" fullWidthText>
          <p>Policy Papers are collections of policies issued by the Board.</p>
          <p>
            <a href="https://drive.google.com/file/d/1hNf1ygFONIxE_-tMg_q2EHh8w0qrJ3f0/view">
              Brand Policy Paper
            </a>
          </p>
          <p>
            <a href="https://drive.google.com/file/d/1itzzoyxC0YxySeGhX1msA-QoYZ-OrD8h/view">
              Event Policy Paper (July 2023)
            </a>
          </p>
          <p>
            <a href="https://drive.google.com/file/d/1OayIlHNMA5rNL_1apApYwnxxeJf1DH3B/view">
              Guide - How to announce your event and choose participants
            </a>
          </p>
          <p>
            <a href="https://drive.google.com/file/d/1N775ewbvb7mD9_cK3QY4ekcOx0hdVkew/view">
              Regional Policy Paper
            </a>
          </p>
          <p>
            <a href="https://drive.google.com/file/d/11bVy1eIBSdQndTQP7spIXFzCk1u6w-Vo/view">
              EECS Policy Paper
            </a>
          </p>
          <p>
            <a href="https://drive.google.com/file/d/1wzkWdAFDIQtkN5EOM46Q2RbQPL7novwy/view">
              Structure Policy Paper (November 2023)
            </a>
          </p>
        </HeadlinedContent>
        <HeadlinedContent title="Swiss Civil Law" variant="h1" fullWidthText>
          <p>
            Our Association is registered in Zurich, therefore, we have to
            follow the Swiss Civil Law which is the foundation of our legal
            documents. Those must be always followed and complied with Swiss
            Civil Code.
          </p>
          <p>
            <a href="https://www.fedlex.admin.ch/eli/cc/24/233_245_233/en">
              Swiss Civil Code
            </a>
          </p>
        </HeadlinedContent>
      </div>
    </div>
  );
}

export default Documents;

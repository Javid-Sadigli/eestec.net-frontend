import React from 'react';
import { useNavigate } from 'react-router-dom';
import Heading from 'components/Heading/Heading';
import Button from 'components/Button/Button';
import useDocumentTitle from 'hooks/useDocumentTitle';

import 'views/public/NotFound/NotFound.css';

/**
 __NotFound__ is a view used to display the 404 page
 */
function NotFound() {
  const navigate = useNavigate();
  useDocumentTitle('Page not found');

  return (
    <div className="not-found-container">
      <Heading variant="h1" lineAfter lineBefore>404</Heading>
      <Heading variant="h2">Not found</Heading>
      <Button
        className="secondary-button not-found-button"
        onClick={() => navigate('/')}
      >
        Go to homepage
      </Button>
    </div>
  );
}

export default NotFound;

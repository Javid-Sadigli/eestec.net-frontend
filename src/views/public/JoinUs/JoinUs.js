import React, { useRef } from 'react';
import { Link } from 'react-router-dom';

import Heading from 'components/Heading/Heading';
import HeadlinedContent from 'components/HeadlinedContent/HeadlinedContent';
import LinkButton from 'components/LinkButton/LinkButton';
import useDocumentTitle from 'hooks/useDocumentTitle';
import scroll from 'utils/scroll';
import MapComponent from 'components/Map/MapComponent';

import 'views/public/JoinUs/JoinUs.css';

/**
 __JoinUs__ is a view used to display the Join Us page of the website
 */
function JoinUs() {
  const joinUsRef = useRef(null);
  const foundAnObserverRef = useRef(null);
  useDocumentTitle('Join us');
  const scrollToObserverFounding = () => scroll(
    foundAnObserverRef,
    null,
    0,
    1000,
    'easeOutBack',
    1,
  );

  return (
    <>
      <MapComponent
        topComponent={(
          <div className="join-us-tabs">
            <div><Link to="/about"><span>EESTEC</span></Link></div>
            <div className="join-us-tab"><span>Join Us</span></div>
          </div>
        )}
        buttonRef={joinUsRef}
      />
      <div className="join-us-section section">
        <div className="split-section">
          <h1 className="main-title" ref={joinUsRef}>Become a Member</h1>

          {/* First bullet point */}
          <HeadlinedContent
            title="Joining EESTEC"
            variant="h2"
          >
            <p>
              Do you want to improve your engineering skills?
              Do you want to gain soft-skills? Do you want to travel?
              Do you want to make new friends around Europe?
              Do you want to learn about new cultures?
            </p>
            <p>
              Do you think you can’t deal with those wishes at the same time?
            </p>
            <p>
              The answer is simple: Yes, you can!
              EESTEC is the right place to make your dreams come true,
              so what are you waiting for?
            </p>
          </HeadlinedContent>

          {/* Second bullet point */}
          <HeadlinedContent
            splitRight
            title="EESTEC for Students"
            variant="h2"
          >
            <p>
              EESTEC is open to every student in the fields of Electrical Engineering,
              Information Technology and similar. It has branches in many of the most
              important cities and technical universities around Europe and everyone
              can join a branch. If EESTEC does not have a committee in your city yet,
              you can
              {' '}
              <LinkButton onClick={scrollToObserverFounding}>
                found one yourself!
              </LinkButton>
            </p>
            <p>
              Once you have joined a branch, you become an EESTECer: from that moment,
              you will be able to attend local meetings, international workshops,
              exchanges, motivational weekends, trainings and other events; you will
              have the chance of making over 3000 friends all over Europe, gain
              professional experience and, of course, feel the EESTEC Spirit!
            </p>
          </HeadlinedContent>

          {/* Third bullet point */}
          <HeadlinedContent
            title="Culture and Traveling"
            variant="h2"
          >
            <p>
              At EESTEC events, students from different cultures and backgrounds come
              together to broaden their knowledge. The main purpose of these events is
              to acquire and improve skills that will help them throughout their career
              and life as well as network and learn the ways of different cultures.
            </p>
            <p>
              Working in an international students’ association will help you understand
              different cultures and attitudes. Nowadays, being able to work in an
              international environment is crucial for people who want to succeed
              and therefore you will gain important and job-relevant experience!
            </p>
          </HeadlinedContent>

          {/* Fourth bullet point */}
          <HeadlinedContent
            splitRight
            title="Skills and Experience"
            variant="h2"
          >
            <p>
              Being an EESTECer will not only open up new horizons, but it will also help
              you grow. Balancing the responsibilities of an organization with your
              university courses teaches you about time management and also what
              fields excite you most, both of which are valuable qualities in
              social and professional environments.
            </p>
            <p>
              EESTEC also means working in teams, therefore it will help you become
              a team player, enhance your communication skills and openness. You can
              get involved in international teams, develop your digital skills and
              upgrade your resume. Through workshops, you can start practising your
              hard skills and learn how to use new technologies.
            </p>
          </HeadlinedContent>

          {/* Fifth bullet point */}
          <HeadlinedContent
            title="Friends and Networking"
            variant="h2"
          >
            <p>
              To many of our members, EESTEC has become a building block for their entire
              future. It is where lifelong friendships and acquaintances have begun.
            </p>
            <p>
              EESTEC also gives you the opportunity to socialize with Alumni, seniors
              who have graduated that can advise you and become your mentors.
            </p>
          </HeadlinedContent>

          {/* Sixth bullet point */}
          <HeadlinedContent
            splitRight
            title="Joining a branch"
            variant="h2"
          >
            <p>
              In order to join EESTEC, you can check our map to find out if there&apos;s
              a branch in your city. If there isn&apos;t one, we strongly encourage you to
              found an Observer!
            </p>
            <p>
              Welcome to the EESTEC world!
            </p>
          </HeadlinedContent>
        </div>

        {/* Observer founding part */}
        <div className="found-an-observer" ref={foundAnObserverRef} id="found-observer">
          <h1 className="main-title" id="found-an-observer-title">Found an Observer</h1>
          <p>
            Do you want to join EESTEC but don&apos;t know how?
            Don&apos;t worry, we are here to help you!
          </p>
          <p>
            We will be more than happy to welcome you in this amazing adventure, but before you
            take the next step, don&apos;t forget to gather information and contact us to clarify
            any doubt you might have.
          </p>
          <p>
            Get to know EESTEC deeper as you observe,
            build up a team and get more tightened in time!
          </p>
          <p>
            An Observer can progress to higher levels: Junior Local Committee (JLC) and Local
            Committee (LC). Remember, every LC went through these steps, so never give up and let
            EESTEC power your future!
          </p>

          <Heading variant="h2" lineBefore lineAfter className="title-center how-to-found-observer-line">
            How to found an Observer
          </Heading>
          <p className="first-paragraph-after-title">
            The prerequisite is to have at least two members. Then, you will contact EESTEC
            International Vice-Chairman for External Affairs (VC-EA) via e-mail at
            vc-ea[at]eestec[dot]net, in order to get an Application Form that needs to be filled
            out. You will be notified by an email about whether your application is valid or not.
            If your application is accepted, you can use the title “EESTEC Observer –Your City“
            officially and start your journey in EESTEC! (for example, EESTEC Observer Karlsruhe,
            in Germany).
          </p>

          <Heading variant="h2" lineBefore lineAfter className="title-center why-found-observer-line">
            Why should you found an Observer
          </Heading>
          <p className="first-paragraph-after-title">
            Being the smallest section of our organization, Observers will get the chance to have an
            insight into how EESTEC functions and what can be the possible benefits of becoming an
            EESTECer.
          </p>
          <div>
            <p>
              Once you become a member, you will have
              taken the first step to a new world, where you can:
            </p>
            <div className="list-parent">
              <ul className="dashed">

                <li>Improve your engineering skills in local or international workshops</li>
                <li>
                  Gain soft-skills, such as communication and leadership tools, through trainings
                </li>
                <li>Learn about team spirit, also known as EESTEC Spirit</li>
                <li>
                  Gain contacts with professionals and companies, which you might benefit for your
                  future career
                </li>
                <li>Travel around Europe, learn about new cultures and make new friends</li>
                <li>Have the time of your life!</li>
              </ul>
            </div>
          </div>

          <div id="call-to-action">
            <p>
              All of these opportunities will be provided free of charge!
              <br />
              Remember that EESTEC Observer members can participate in all EESTEC events and could
              take advantage of anything that is offered to any other participants.
              <br />
              Welcome to EESTEC!
            </p>
          </div>
        </div>
      </div>
    </>
  );
}

export default JoinUs;

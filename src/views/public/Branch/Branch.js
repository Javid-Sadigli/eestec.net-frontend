import React, {
  useCallback, useContext, useEffect, useMemo, useState,
} from 'react';
import { Link, useNavigate, useParams } from 'react-router-dom';
import BranchService from 'api/service/branches';
import useDocumentTitle from 'hooks/useDocumentTitle';
import useEvents from 'hooks/useEvents';
import CardList from 'components/CardList/CardList';
import EventCard from 'components/EventCard/EventCard';
import useMembers from 'hooks/useMembers';
import PersonCard from 'components/PersonCard/PersonCard';
import { membersGridSettings } from 'utils/grid';
import StickyMenu from 'components/StickyMenu/StickyMenu';
import EntityHeader from 'components/EntityHeader/EntityHeader';
import EntitySection from 'components/EntitySection/EntitySection';
import { removeNbsp, createParagraphs } from 'utils/misc';
import useEntityEdit from 'hooks/useEntityEdit';
import CircularProgress from '@mui/material/CircularProgress';
import VrpanoIcon from '@mui/icons-material/Vrpano';
import EditIcons from 'components/EditIcons/EditIcons';
import ConditionalInput from 'components/ConditionalInput/ConditionalInput';
import Div from 'utils/componentsWrappers/Div';
import TextInput from 'components/TextInput/TextInput';
import Button from 'components/Button/Button';
import SplitButton from 'components/SplitButton/SplitButton';
import AlumniIcon from '@mui/icons-material/Elderly';
import ActiveIcon from '@mui/icons-material/SportsMartialArts';
import RemovePersonIcon from '@mui/icons-material/PersonRemove';
import AddPersonIcon from '@mui/icons-material/PersonAddAlt1';
import GroupAddIcon from '@mui/icons-material/GroupAdd';
import GroupRemoveIcon from '@mui/icons-material/GroupRemove';
import { BranchRoles } from 'utils/roles';
import {
  BranchApplicationStatus,
  getBranchApplicationStatus,
  getBranchTypeAndName,
  isBranchTerminated,
} from 'utils/branches';
import AuthContext from 'context/AuthContext';
import ToastContext from 'context/ToastContext';
import ImageFileInput from 'components/ImageFileInput/ImageFileInput';
import logger from 'utils/logger';
import useResetDataOnLogout from 'hooks/useResetDataOnLogout';
import useBranchRefs from './hooks/useBranchRefs';
// import { format } from 'date-fns';

import './Branch.css';

// TODO: The member transfering can be make much easier I believe if the oldRole
//  argument is taken from member PIVOT and not depending on the category where
//  the member with a button is. To be improved in the future
const transferMember = ({
  newRoleId,
  memberSlug,
  sourceMemberGroup,
  destinationMemberGroup,
  extraDestinationMemberGroup,
  shouldRemove,
  shouldAdd,
  extraGroupForRemove,
}) => {
  // make a copy of the source member state since splice mutates the array
  const sourceMemberGroupState = [...sourceMemberGroup.members];
  // find the index of the member we want to yeet from the source group
  const memberIndex = sourceMemberGroupState.findIndex((user) => user.slug === memberSlug);
  // apply the new pivot information to the member
  const member = {
    ...sourceMemberGroupState[memberIndex],
    pivot: {
      ...sourceMemberGroupState[memberIndex].pivot,
      branch_role_id: newRoleId,
    },
  };
  if (shouldRemove) {
    // if the member should be removed from the source group, delete them
    sourceMemberGroupState.splice(memberIndex, 1);
    // handle removal from any other groups that were specified (e.g. the board & other list)
    if (extraGroupForRemove) {
      extraGroupForRemove.group.setMembers(((prevState) => {
        const newState = [...prevState];
        newState.splice(extraGroupForRemove.index, 1);
        return newState;
      }));
    }
  } else {
    // if no removal is needed, just update the member with their pivot in the source group
    sourceMemberGroupState[memberIndex] = member;
  }
  // update the source member group
  sourceMemberGroup.setMembers(sourceMemberGroupState);
  if (shouldAdd) {
    // if the member should be added to any destination group, add them
    destinationMemberGroup.setMembers((prevState) => [
      member,
      ...prevState,
    ]);
    // handle addition to any other groups that were specified
    // (e.g. an alumni goes to the board - add them to the board and move to active members)
    if (extraDestinationMemberGroup) {
      extraDestinationMemberGroup.setMembers((prevState) => [
        member,
        ...prevState,
      ]);
    }
  } else {
    // if no addition is being done, update member information in the destination member group
    destinationMemberGroup.setMembers((prevState) => {
      const newState = [...prevState];
      const destinationMemberIndex = newState.findIndex(
        (user) => user.slug === memberSlug,
      );
      newState[destinationMemberIndex] = member;
      return newState;
    });
  }
};

// applicationResponseMapper is the special mapper of the outcome of useMembers
// defined outside of the component to avoid re-rendering
const applicationResponseMapper = (response) => response.map((applicationData) => ({
  ...applicationData.user,
  applicationId: applicationData.id,
}));

/**
 __Branch__ is a view used to display the Branch page
 */
function Branch() {
  const { userData, updateUserData } = useContext(AuthContext);
  const { setToastContent } = useContext(ToastContext);
  const navigate = useNavigate();
  const { branchSlug } = useParams();
  const [branchData, setBranchData] = useState();
  const [branchApplicationLoading, setBranchApplicationLoading] = useState(false);
  const [hasEditRights, setHasEditRights] = useState(false);
  const {
    loadMorePastEvents,
    formatAndSetEvents,
    events,
    eventsLoading,
    toggleEventsLoading,
    morePastEventsLoading,
    morePastEventsAvailable,
  } = useEvents();
  const activeMembers = useMembers(`/branches/${branchSlug}/active`);
  const boardMembers = useMembers(`/branches/${branchSlug}/board`);
  const alumniMembers = useMembers(`/branches/${branchSlug}/alumni`);
  const membershipApplications = useMembers(
    `/branches/${branchSlug}/applications`,
    applicationResponseMapper,
  );
  useResetDataOnLogout({
    listingSetters: [
      activeMembers.setMembers,
      alumniMembers.setMembers,
      membershipApplications.setMembers,
    ],
    adminPropsSetters: [setHasEditRights],
  });
  const {
    setEntityEditFormData,
    toggleIsBeingEdited,
    onEntityEditSubmit,
    isEntityEditLoading,
    isEditingDisabled,
    isBeingEdited,
    entityEditFormData,
    isAnyBeingEdited,
    resetEntityEditFormData,
  } = useEntityEdit({
    apiServiceCall: (formData, requestArgs) => BranchService.editBranch(
      branchSlug,
      formData,
      ...requestArgs,
    ),
    data: branchData,
    setData: setBranchData,
  });
  const branchRefs = useBranchRefs();
  const branchFullName = useMemo(
    () => (branchData ? getBranchTypeAndName(branchData.name, branchData.branch_type) : null),
    [branchData],
  );
  useDocumentTitle(branchFullName);

  const hasEvents = useMemo(() => (
    events.application.length > 0
    || events.upcoming.length > 0
    || events.inProgress.length > 0
    || events.past.length > 0
  ), [events]);

  const hasMembers = useMemo(() => (
    activeMembers.members.length > 0
    || alumniMembers.members.length > 0
    || membershipApplications.members.length > 0
  ), [alumniMembers.members, activeMembers.members, membershipApplications.members]);

  // format entity header details fields
  const detailsFields = useMemo(() => {
    if (!branchData) {
      return [];
    }
    return (
      branchData.isTerminated ? [
        {
          title: 'Founded in',
          value: branchData?.founded_in,
        },
      ] : [
        {
          title: 'Founded in',
          value: branchData?.founded_in,
        },
        {
          title: 'Number of members',
          value: branchData?.member_no,
        },
      ]
    );
  }, [branchData]);

  // getTransferMemberArguments is used while changing the role of a member, returns
  // the proper arguments that should be used while changing the roles in order to put the
  // member into the right new group
  const getTransferMemberArguments = useCallback((userSlug, oldRole, newRole) => {
    const rolesMapping = new Map([
      [BranchRoles.BOARD, boardMembers],
      [BranchRoles.ACTIVE_MEMBER, activeMembers],
      [BranchRoles.ALUMNI, alumniMembers],
    ]);

    let extraArgs = {};
    if (oldRole === BranchRoles.BOARD) {
      extraArgs = {
        extraDestinationMemberGroup: null,
        shouldRemove: true,
        shouldAdd: false,
      };
    } else if (newRole === BranchRoles.BOARD) {
      if (oldRole === BranchRoles.ACTIVE_MEMBER) {
        extraArgs = {
          extraDestinationMemberGroup: null,
          shouldRemove: false,
          shouldAdd: true,
        };
      } else if (oldRole === BranchRoles.ALUMNI) {
        extraArgs = {
          extraDestinationMemberGroup: activeMembers,
          shouldRemove: true,
          shouldAdd: true,
        };
      }
    } else {
      extraArgs = {
        extraDestinationMemberGroup: null,
        shouldRemove: true,
        shouldAdd: true,
      };
      if (newRole === BranchRoles.ALUMNI) {
        const memberIndex = boardMembers.members.findIndex((user) => user.slug === userSlug);
        if (memberIndex >= 0) {
          extraArgs.extraGroupForRemove = {
            group: boardMembers,
            index: memberIndex,
          };
        }
      }
    }
    return {
      sourceMemberGroup: rolesMapping.get(oldRole),
      destinationMemberGroup: rolesMapping.get(newRole),
      ...extraArgs,
    };
  }, [boardMembers, activeMembers, alumniMembers]);

  // this function is triggered when someone attempts to join a branch
  const applyToBranch = () => {
    setBranchApplicationLoading(true);
    BranchService.apply(branchSlug)
      .then(() => {
        // update the user data with the application data, to be displayed on the profile page
        updateUserData({
          data: {
            ...userData,
            branch_applications: [
              ...userData.branch_applications,
              {
                ...branchData,
              },
            ],
          },
        });
        setToastContent(
          'You have applied to this branch successfully! Now wait for a verification from the branch administrators.',
          'success',
        );
      })
      .catch((err) => {
        logger.log(err);
        setToastContent(
          err.data,
          'error',
        );
      })
      .finally(() => setBranchApplicationLoading(false));
  };

  // this effect runs on mount and retrieves data about the branch and its members
  useEffect(() => {
    // Creates an AbortController which aborts ongoing requests if component is unmounted
    const abortController = new AbortController();

    // Get branch information
    BranchService.getBranch(branchSlug, abortController.signal)
      .then((response) => {
        setBranchData({
          ...response.branch,
          description: removeNbsp(response.branch.description),
          isTerminated: isBranchTerminated(response.branch.branch_type),
        });
        setHasEditRights(response.can_edit);
      })
      .catch((err) => {
        if (err?.status !== 404) {
          logger.log(err);
        } else {
          navigate('/404', { replace: true });
        }
      });

    // Get events information
    BranchService.getBranchEvents(branchSlug, abortController.signal)
      .then((response) => {
        formatAndSetEvents(response);
        toggleEventsLoading();
      })
      .catch((err) => {
        if (err?.message !== 'canceled' && err?.status !== 404) {
          logger.log(err);
          toggleEventsLoading();
        }
      });

    // during unmount of the component abort the ongoing requests to avoid memory leak
    return () => abortController.abort();
  }, [branchSlug, navigate, toggleEventsLoading, formatAndSetEvents]);

  // manageApplication handles accepting/reject branch applications
  const manageApplication = ([applicationId, applicationSlug], accepted, callbackFunction) => {
    if (hasEditRights) {
      BranchService.manageBranchMembership(branchSlug, applicationId, accepted)
        .then(() => {
          if (accepted) {
            // on success, transfer the member into the group with active members
            transferMember({
              newRoleId: BranchRoles.ACTIVE_MEMBER,
              memberSlug: applicationSlug,
              sourceMemberGroup: membershipApplications,
              destinationMemberGroup: activeMembers,
              extraDestinationMemberGroup: null,
              shouldRemove: true,
              shouldAdd: true,
            });
            setToastContent(
              'Application accepted successfully!',
              'success',
            );
          } else {
            // if the member is rejected, just yeet them from the list of applicants
            membershipApplications.setMembers((prevState) => prevState.filter(
              (user) => user.slug !== applicationSlug,
            ));
            setToastContent(
              'Application rejected successfully!',
              'success',
            );
          }
        })
        .catch((err) => {
          setToastContent(
            err?.data?.message || 'Error.',
            'error',
          );
          callbackFunction();
        });
    }
  };

  // removeMember handles removal of members from the branch
  const removeMember = (userSlug, memberGroup, callbackFunction) => {
    if (hasEditRights) {
      BranchService.removeMember(branchSlug, userSlug)
        .then(() => {
          memberGroup.setMembers((prevState) => prevState.filter(
            (user) => user.slug !== userSlug,
          ));
          boardMembers.setMembers((prevState) => prevState.filter(
            (user) => user.slug !== userSlug,
          ));
        })
        .catch((err) => {
          setToastContent(
            err?.data?.message || 'Error.',
            'error',
          );
          callbackFunction();
        });
    }
  };

  // changeMemberRole accepts the information about the new and old role of a member
  // and changes the roles
  const changeMemberRole = (userSlug, oldRole, newRole, callbackFunction) => {
    if (hasEditRights) {
      // prevent yeeting oneself from the board if one is the last person in the board
      if (
        oldRole === BranchRoles.BOARD
        && boardMembers.members.length < 2
        && userData.slug === userSlug
      ) {
        setToastContent(
          'You cannot remove yourself from the board when you are the only board member!',
          'error',
        );
        callbackFunction();
      } else {
        BranchService.changeMemberRole(branchSlug, userSlug, newRole)
          .then(() => {
            const transferMemberArguments = getTransferMemberArguments(userSlug, oldRole, newRole);
            transferMember({
              ...transferMemberArguments,
              newRoleId: newRole,
              memberSlug: userSlug,
            });
            if (!transferMemberArguments.shouldRemove) {
              callbackFunction();
            }
          })
          .catch((err) => {
            setToastContent(
              err?.data?.message || 'Error.',
              'error',
            );
            callbackFunction();
          });
      }
    }
  };

  // stickyMenuElements are used to organize data for the sticky menu
  // TODO: maybe it would be possible to create a hook like useStickyMenu
  //  to have all the logic behind stickyMenu and needed refs in one place
  const stickyMenuElements = [
    {
      title: 'Description',
      target: branchRefs.description,
    },
    {
      title: 'Board',
      target: branchRefs.board,
    },
    {
      title: 'Events',
      target: branchRefs.events.all,
      subElements: [
        {
          title: 'Open for application',
          target: branchRefs.events.openForApplication,
          isEmpty: !events.application.length,
        },
        {
          title: 'Upcoming',
          target: branchRefs.events.upcoming,
          isEmpty: !events.upcoming.length,
        },
        {
          title: 'In progress',
          target: branchRefs.events.inProgress,
          isEmpty: !events.inProgress.length,
        },
        {
          title: 'Past',
          target: branchRefs.events.past,
          isEmpty: !events.past.length,
        },
      ].filter((subElement) => !subElement.isEmpty),
    },
    {
      title: 'Members',
      target: branchRefs.members.all,
      subElements: [
        {
          title: 'Applications',
          target: branchRefs.members.membershipApplications,
          isEmpty: !membershipApplications.members.length,
        },
        {
          title: 'Active',
          target: branchRefs.members.active,
          isEmpty: !activeMembers.members.length,
        },
        {
          title: 'Alumni',
          target: branchRefs.members.alumni,
          isEmpty: !alumniMembers.members.length,
        },
      ].filter((subElement) => !subElement.isEmpty),
    },
  ];

  // Handle the branch description and header for terminated branches
  let branchDescription = null;
  const branchHeaderProperties = {
    className: 'branch-header',
    subtitle: null,
    backgroundImage: null,
  };
  if (branchData) {
    if (branchData.isTerminated) {
      branchDescription = (
        <p className="no-description-info small">
          This branch has been terminated.
          Are you from
          {' '}
          {branchData.name}
          {' '}
          and willing to reactivate the branch in your city?
          {' '}
          <Link to="/join-us#found-observer">
            Start here!
          </Link>
        </p>
      );
      branchHeaderProperties.className += ' terminated';
      branchHeaderProperties.subtitle = 'Terminated branch';
    } else {
      branchDescription = (
        <>
          <EditIcons
            className="entity-section-edit-icons description-icons"
            isBeingEdited={isBeingEdited?.description}
            onEditSubmit={() => onEntityEditSubmit(['description'])}
            onEditCancel={resetEntityEditFormData}
            onEditStart={() => toggleIsBeingEdited('description')}
            isLoading={isEntityEditLoading?.description}
            shouldDisplay={hasEditRights}
            shouldBeVisible={!isAnyBeingEdited}
          />
          {(!branchData.description && !isBeingEdited?.description) && (
            <p className="no-description-info small">
              This branch did not provide any description yet.
            </p>
          )}
          <ConditionalInput
            onChange={(e) => setEntityEditFormData('description', e.target.value)}
            staticComponent={{
              element: Div,
              props: {
                className: 'description-conditional-input',
              },
            }}
            editableContent={entityEditFormData?.description}
            content={createParagraphs(branchData.description, 'branch-description')}
            condition={isBeingEdited?.description}
            inputComponent={{
              element: TextInput,
              props: {
                multiline: true,
                placeholder: 'Tell us about your branch!',
                className: 'description-conditional-input',
              },
            }}
            isAllowedToEdit={hasEditRights}
            isInputDisabled={isEditingDisabled}
          />
        </>
      );
      branchHeaderProperties.backgroundImage = branchData.large_profile_picture_path;
    }
  }

  // branchApplicationStatus defines what kind of element should be there on the right side
  // of the StickyMenu
  const branchApplicationStatus = getBranchApplicationStatus(
    branchSlug,
    userData?.branches,
    userData?.branch_applications,
  );

  return (
    <div className="entity-wrapper" id="branch">
      <EntityHeader
        title={branchFullName}
        subtitle={branchHeaderProperties.subtitle}
        backgroundImage={branchHeaderProperties.backgroundImage}
        className={branchHeaderProperties.className}
        detailsFields={detailsFields}
        blurred={!!branchData}
      >
        {(!!branchData && hasEditRights) && (
          <ImageFileInput
            onChange={(file) => onEntityEditSubmit(['profile_picture_path'], true, file)}
            title="Change the banner picture (max. 2MB)"
            disabled={isEditingDisabled || isAnyBeingEdited}
            Icon={VrpanoIcon}
            isLoading={isEntityEditLoading?.profile_picture_path}
          />
        )}
      </EntityHeader>
      {(branchData && !branchData?.isTerminated) && (
        <StickyMenu
          elements={stickyMenuElements}
          rightElement={branchApplicationStatus !== BranchApplicationStatus.INVALID ? (
            <Button
              onClick={applyToBranch}
              disabled={branchApplicationStatus === BranchApplicationStatus.PENDING}
              isLoading={branchApplicationLoading}
              className="primary-button"
              id="branch-application-button"
            >
              {branchApplicationStatus === BranchApplicationStatus.CAN_APPLY ? 'Join' : 'Pending application'}
            </Button>
          ) : null}
        />
      )}
      <div className="entity-inner-content">
        {branchData ? (
          <>
            <EntitySection
              title="Description"
              className="entity-section description"
              innerRef={branchRefs.description}
            >
              {branchDescription}
            </EntitySection>
            {!branchData.isTerminated && (
              <EntitySection
                title="Board"
                innerRef={branchRefs.board}
              >
                <CardList
                  loading={boardMembers.membersLoading}
                  emptyMessage="No board members found in this branch"
                  items={boardMembers.members}
                  cardType={PersonCard}
                  gridSettings={membersGridSettings}
                  horizontal
                  customCardButton={hasEditRights ? {
                    element: SplitButton,
                    props: {
                      label: 'Manage a board member',
                      mainAction: {
                        text: 'View profile',
                        component: Link,
                      },
                      extraActions: [
                        {
                          text: 'Remove from the board',
                          onClick: (
                            slug,
                            callbackFunction,
                            // callbackFunction must be passed as it is a SplitButton
                          ) => changeMemberRole(
                            slug,
                            BranchRoles.BOARD,
                            BranchRoles.ACTIVE_MEMBER,
                            callbackFunction,
                          ),
                          Icon: GroupRemoveIcon,
                        },
                      ],
                    },
                  } : null}
                />
              </EntitySection>
            )}
            <EntitySection
              title="Events"
              innerRef={branchRefs.events.all}
              nested
            >
              {!hasEvents && (
                <p className="no-cards dimmed">
                  This branch has no record of events yet.
                </p>
              )}
              <EntitySection
                title="Open for applications"
                visibilityCondition={!!events.application.length}
                headingClassName="card-subheading"
                headingVariant="h2"
                innerRef={branchRefs.events.openForApplication}
                className="event-group"
              >
                <CardList
                  items={events.application}
                  cardButtonsText="Apply now"
                  emptyMessage="This branch has no events open for applications"
                  loading={eventsLoading.application}
                  cardType={EventCard}
                />
              </EntitySection>
              <EntitySection
                title="Upcoming"
                visibilityCondition={!!events.upcoming.length}
                headingClassName="card-subheading"
                headingVariant="h2"
                innerRef={branchRefs.events.upcoming}
                className="event-group"
              >
                <CardList
                  items={events.upcoming}
                  emptyMessage="This branch has no upcoming events"
                  loading={eventsLoading.upcoming}
                  cardType={EventCard}
                />
              </EntitySection>
              <EntitySection
                title="In progress"
                visibilityCondition={!!events.inProgress.length}
                headingClassName="card-subheading"
                headingVariant="h2"
                innerRef={branchRefs.events.inProgress}
                className="event-group"
              >
                <CardList
                  items={events.inProgress}
                  emptyMessage="This branch has no events in progress"
                  loading={eventsLoading.inProgress}
                  cardType={EventCard}
                />
              </EntitySection>
              <EntitySection
                title="Past"
                visibilityCondition={!!events.past.length}
                headingClassName="card-subheading"
                headingVariant="h2"
                innerRef={branchRefs.events.past}
                className="event-group"
              >
                <CardList
                  items={events.past}
                  emptyMessage="This branch has no past events"
                  loading={eventsLoading.past}
                  cardType={EventCard}
                  showMoreButton={{
                    visibilityCondition: morePastEventsAvailable,
                    onClick: loadMorePastEvents,
                    isLoading: morePastEventsLoading,
                    text: 'Show more past events',
                  }}
                />
              </EntitySection>
            </EntitySection>
            {!branchData.isTerminated && (
              <EntitySection
                title="Members"
                innerRef={branchRefs.members.all}
                nested
                visibilityCondition={hasMembers}
              >
                {hasEditRights && (
                  <EntitySection
                    title="Applications"
                    innerRef={branchRefs.members.membershipApplications}
                    className="members members-applications"
                    headingClassName="card-subheading"
                    headingVariant="h2"
                    visibilityCondition={!!membershipApplications.members.length}
                  >
                    <CardList
                      loading={membershipApplications.membersLoading}
                      emptyMessage="No incoming branch applications"
                      items={membershipApplications.members}
                      showMoreButton={{
                        visibilityCondition: membershipApplications.moreMembersAvailable,
                        onClick: membershipApplications.getMoreMembers,
                        isLoading: membershipApplications.moreMembersLoading,
                        text: 'Show more incoming applications',
                      }}
                      cardType={PersonCard}
                      gridSettings={membersGridSettings}
                      customCardButton={{
                        element: SplitButton,
                        props: {
                          label: 'Manage an incoming membership application',
                          mainAction: {
                            text: 'View profile',
                            component: Link,
                          },
                          extraActions: [
                            {
                              text: 'Accept',
                              onClick: (
                                applicationData,
                                callbackFunction,
                                // callbackFunction must be passed as it is a SplitButton
                              ) => manageApplication(applicationData, true, callbackFunction),
                              Icon: AddPersonIcon,
                            },
                            {
                              text: 'Reject',
                              onClick: (
                                applicationData,
                                callbackFunction,
                                // callbackFunction must be passed as it is a SplitButton
                              ) => manageApplication(applicationData, false, callbackFunction),
                              Icon: RemovePersonIcon,
                            },
                          ],
                        },
                      }}
                    />
                  </EntitySection>
                )}
                <EntitySection
                  title="Active"
                  innerRef={branchRefs.members.active}
                  className="members-section active-members"
                  headingClassName="card-subheading"
                  headingVariant="h2"
                  visibilityCondition={!!activeMembers.members.length}
                >
                  <CardList
                    loading={activeMembers.membersLoading}
                    emptyMessage="No active members found in this branch"
                    items={activeMembers.members}
                    showMoreButton={{
                      visibilityCondition: activeMembers.moreMembersAvailable,
                      onClick: activeMembers.getMoreMembers,
                      isLoading: activeMembers.moreMembersLoading,
                      text: 'Show more active members',
                    }}
                    cardType={PersonCard}
                    gridSettings={membersGridSettings}
                    customCardButton={hasEditRights ? {
                      element: SplitButton,
                      props: {
                        label: 'Manage an active member',
                        mainAction: {
                          text: 'View profile',
                          component: Link,
                        },
                        extraActions: [
                          {
                            text: 'Make alumni',
                            onClick: (
                              slug,
                              callbackFunction,
                            ) => changeMemberRole(
                              slug,
                              BranchRoles.ACTIVE_MEMBER,
                              BranchRoles.ALUMNI,
                              callbackFunction,
                            ),
                            Icon: AlumniIcon,
                          },
                          {
                            text: 'Add to the board',
                            onClick: (
                              slug,
                              callbackFunction,
                            ) => changeMemberRole(
                              slug,
                              BranchRoles.ACTIVE_MEMBER,
                              BranchRoles.BOARD,
                              callbackFunction,
                            ),
                            Icon: GroupAddIcon,
                            isAddToBoard: true,
                          },
                          {
                            text: 'Remove from the branch',
                            onClick: (
                              slug,
                              callbackFunction,
                            ) => removeMember(slug, activeMembers, callbackFunction),
                            Icon: RemovePersonIcon,
                          },
                        ],
                      },
                    } : null}
                  />
                </EntitySection>
                <EntitySection
                  title="Alumni"
                  innerRef={branchRefs.members.alumni}
                  className="members-section alumni"
                  headingClassName="card-subheading"
                  headingVariant="h2"
                  visibilityCondition={!!alumniMembers.members.length}
                >
                  <CardList
                    loading={alumniMembers.membersLoading}
                    emptyMessage="No alumni found in this branch"
                    items={alumniMembers.members}
                    showMoreButton={{
                      visibilityCondition: alumniMembers.moreMembersAvailable,
                      onClick: alumniMembers.getMoreMembers,
                      isLoading: alumniMembers.moreMembersLoading,
                      text: 'Show more alumni',
                    }}
                    cardType={PersonCard}
                    gridSettings={membersGridSettings}
                    customCardButton={hasEditRights ? {
                      element: SplitButton,
                      props: {
                        label: 'Manage an alumni member',
                        mainAction: {
                          text: 'View profile',
                          component: Link,
                        },
                        extraActions: [
                          {
                            text: 'Make active',
                            onClick: (
                              slug,
                              callbackFunction,
                            ) => changeMemberRole(
                              slug,
                              BranchRoles.ALUMNI,
                              BranchRoles.ACTIVE_MEMBER,
                              callbackFunction,
                            ),
                            Icon: ActiveIcon,
                          },
                          {
                            text: 'Add to the board',
                            onClick: (
                              slug,
                              callbackFunction,
                            ) => changeMemberRole(
                              slug,
                              BranchRoles.ALUMNI,
                              BranchRoles.BOARD,
                              callbackFunction,
                            ),
                            Icon: GroupAddIcon,
                            isAddToBoard: true,
                          },
                          {
                            text: 'Remove from the branch',
                            onClick: (
                              slug,
                              callbackFunction,
                              // callbackFunction must be passed as it is a SplitButton
                            ) => removeMember(slug, alumniMembers, callbackFunction),
                            Icon: RemovePersonIcon,
                          },
                        ],
                      },
                    } : null}
                  />
                </EntitySection>
              </EntitySection>
            )}
          </>
        ) : (
          <div className="entity-loader">
            <CircularProgress size={70} />
          </div>
        )}
      </div>
    </div>
  );
}

export default Branch;

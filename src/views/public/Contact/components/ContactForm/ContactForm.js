import React from 'react';

import 'views/public/Contact/components/ContactForm/ContactForm.css';

function ContactForm() {
  return (
    <section className="contact-contact-container">
      <div className="contact-info">
        <div className="contact-mails">
          <p className="contact-info-headers">Contact Information:</p>

          {/* THIS IS FOR PR PAGE ONLY */}
          <div>
            <p>Interested in becoming a member of EESTEC or founding an Observer?</p>
            <p>Or maybe you would like to contact us regarding a partnership?</p>
            <p>Would you like to learn more about our privacy policy?</p>
            <p id="board-mail">
              Send us an email at: board[at]eestec[dot]net
            </p>
          </div>
        </div>
        <div className="contact-address">
          <p className="contact-info-headers">Postal Address:</p>
          <p className="contact-info-bold">EESTEC</p>
          <p className="contact-info-bold">
            Electrical Engineering STudents&apos; European assoCiation
          </p>
          <p>
            EESTEC c/o AMIV an der ETH CAB E37,
            <br />
            Universitätstrasse 6, 8092 Zürich, Switzerland
            <br />
            Email: board@eestec.net
          </p>
        </div>
      </div>
      {/* Form Todos : onSubmit send email */}
    </section>
  );
}

export default ContactForm;

const contactData = [
  {
    name: 'new member',
    to: 'example@eestec.net',
    cc: '',
    text: 'Interested in becoming a member of EESTEC?',
    link: 'EESTEC Chairperson',
    toText: 'Contact Person',
    index: 0,
  },
  {
    name: 'new observer',
    to: 'vc-ea@eestec.net',
    cc: '',
    text: 'Interested in founding an Observer?',
    link: 'Contact our Vice Chairperson for External Affairs',
    toText: 'VC External Affairs',
    index: 1,
  },
  {
    name: 'partnership',
    to: 'cr-board@eestec.net',
    cc: 'vc-ea@eestec.net',
    text: 'Interested in a partnership?',
    link: 'Contact the Corporate Relations team',
    toText: 'Corporate Relations Board',
    index: 2,
  },
  {
    name: 'technical',
    to: 'support@eestec.net',
    cc: 'it-board@eestec.net',
    text: 'Technical questions or problems with this page?',
    link: 'Contact support',
    toText: 'EESTEC.net Support',
    index: 3,
  },
  {
    name: 'gdpr and privacy',
    to: 'legal-experts-leader@eestec.net',
    cc: 'treasurer@eestec.net',
    text: 'GDPR requests and privacy information?',
    link: 'Contact our legal department',
    toText: 'Legal experts leader',
    index: 4,
  },
];

export default contactData;

import React, { useContext } from 'react';
import PropTypes from 'prop-types';

import ModalContext from 'context/ModalContext';
import ButtonComponent from 'components/Button/Button';
import SingleContactBoard from 'views/public/Contact/components/ContactBoard/SingleContactBoard/SingleContactBoard';
import { Card } from 'components/Card/Card';
import 'views/public/Contact/components/ContactBoard/ContactBoard.css';

/**
 __BoardCard__ (a.k.a __ContactBoard__) is a variant of
 [`Card`](./?path=/docs/components-cards-card)
 which is used for describing a Board member (at the moment only on the
 [Contacts](https:eestec.net/contact) page) - its name, position and picture.
*/
function ContactBoard({
  img,
  desc,
  name,
  occupation,
}) {
  const { setModalContent } = useContext(ModalContext);

  const setModal = () => {
    const modalData = {
      img, alt: name, desc, occupation, name,
    };
    setModalContent(<SingleContactBoard modalData={modalData} />);
  };

  const readMoreButton = () => (
    <ButtonComponent
      className="contact-board-button"
      onClick={setModal}
    >
      Read More
    </ButtonComponent>
  );

  return (
    <Card
      image={img}
      title={name}
      className="board-card-image"
      customCardButton={readMoreButton()}
    >
      {occupation}
    </Card>
  );
}

ContactBoard.propTypes = {
  img: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  /**
   A description of the duties of a given Board member - used to be passed to the
   modal containing detailed information about a Board member.
  */
  desc: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.string)]).isRequired,
  /**
   a.k.a __position__
  */
  occupation: PropTypes.string.isRequired,
};

export default ContactBoard;

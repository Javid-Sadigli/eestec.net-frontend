/* eslint-disable no-alert */
import React from 'react';
import placeholderImg from 'assets/img/member-placeholder.jpg';
import BoardCard from './ContactBoard';
import ModalContextDecorator from '../../../../../../.storybook/decorators/ModalContextDecorator';

export default {
  title: 'Components/Cards/BoardCard',
  component: BoardCard,
  decorators: [ModalContextDecorator],
  args: {
    img: placeholderImg,
    desc: ['I do stuff'],
    occupation: 'Vice-Chairperson for the Storybook',
    name: 'John Paul Doe',
  },
};

function Template(args) {
  return (
    <BoardCard
      {...args}
    />
  );
}

export const Default = Template.bind({});

import React from 'react';
import PropTypes from 'prop-types';
import 'views/public/Contact/components/ContactBoard/SingleContactBoard/SingleContactBoard.css';

/**
 __SingleContactBoard__ is a modal element presenting detailed information about a board member
 */
function SingleContactBoard({ modalData }) {
  const {
    img, desc, name, occupation, alt,
  } = modalData;
  return (
    <div className="single-contact-board">
      <div className="single-board-photo-container">
        <img src={img} alt={alt} className="single-board-photo" />
      </div>
      <div className="single-board-member-info">
        <div className="single-board-header">
          <h3>
            {name}
          </h3>
          <p>
            {occupation}
          </p>
        </div>
        <div className="single-board-desc">
          <p>Responsible for:</p>
          <ul>
            {desc.map((item, index) => (
              // eslint-disable-next-line react/no-array-index-key
              <li key={`${name}-${index}`}>{item}</li>
            ))}
          </ul>
        </div>
      </div>
    </div>
  );
}

SingleContactBoard.propTypes = {
  modalData: PropTypes.shape({
    img: PropTypes.string,
    desc: PropTypes.arrayOf(PropTypes.string),
    name: PropTypes.string,
    occupation: PropTypes.string,
    alt: PropTypes.string,
  }).isRequired,
};
export default SingleContactBoard;

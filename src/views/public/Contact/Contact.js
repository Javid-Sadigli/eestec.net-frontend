import React, { useContext } from 'react';

import ModalContext from 'context/ModalContext';
import Heading from 'components/Heading/Heading';
import ContactBoard from 'views/public/Contact/components/ContactBoard/ContactBoard';
import ContactForm from 'views/public/Contact/components/ContactForm/ContactForm';
import boardMembersData from 'utils/board';
import useDocumentTitle from 'hooks/useDocumentTitle';

import 'views/public/Contact/Contact.css';

/**
 __Contact__ is a view used to display the Contact page
 */
function Contact() {
  useDocumentTitle('Contact');
  const { modalContent } = useContext(ModalContext);

  return (
    <section className={`contact ${modalContent ? 'blurred' : ''}`}>
      <Heading variant="h3" lineAfter styleVariant="h1" className="contact-board-title">The Board</Heading>
      <div className="contact-board-container">
        {
          boardMembersData.map(({
            name, desc, occupation, img,
          }) => (
            <ContactBoard
              key={name}
              name={name}
              desc={desc}
              occupation={occupation}
              img={img}
            />
          ))
        }
      </div>
      <Heading variant="h3" lineAfter styleVariant="h2" className="contact-contact-title">CONTACT</Heading>
      <ContactForm />
    </section>
  );
}

export default Contact;

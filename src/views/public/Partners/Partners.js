import React, { useContext } from 'react';
import ModalContext from 'context/ModalContext';
import HeadlinedContent from 'components/HeadlinedContent/HeadlinedContent';
import PartnersSlider from 'components/PartnersSlider/PartnersSlider';
import useDocumentTitle from 'hooks/useDocumentTitle';

import 'views/public/Partners/Partners.css';

/**
 __Partners__ is a view used to display the Partners page of the website
 */
function Partners() {
  const { modalContent } = useContext(ModalContext);
  useDocumentTitle('Partners');

  return (
    <div id="partners" className={modalContent ? 'blurred' : ''}>
      <PartnersSlider />
      <div className="section partners-top-message">
        <p>
          Our members are the
          <b> next generation of professionals who</b>
          , with their skills
          and knowledge, will be the ones supporting the companies in achieving
          their vision and goals.
          <br />
          Would you like to have access to a network of passionate and highly
          skilled students?
          <br />
          You can partner with us!
        </p>
      </div>
      <div className="section become-partner">
        <HeadlinedContent
          title="Become our Annual Partner"
          variant="h1"
        >
          <p>
            As our Annual Partner you can gain direct access to our network throughout the year,
            not only via external but also through internal channels.
            Only this package includes exclusive services,
            thanks to which your brand can achieve high visibility.
          </p>
        </HeadlinedContent>
        <HeadlinedContent
          title="Co-organize a Webinar with us"
          variant="h1"
          splitRight
        >
          <p>
            As our partner, you will have a chance to introduce practices and technologies
            that your company implements, or to deliver a presentation upon other fields of
            interest.
          </p>
        </HeadlinedContent>
        <HeadlinedContent
          title="Promote your company through our network"
          variant="h1"
        >
          <p>
            If you choose this package, you can leverage the broad range of our network’s
            channels to promote your brand and your initiatives, by placing the content of
            your choice in front of targeted audiences from all over Europe.
          </p>
        </HeadlinedContent>
        <HeadlinedContent
          title="Get involved in our Projects"
          variant="h1"
          splitRight
        >
          <p>
            <b>
              <a
                href="https://ssa.eestec.net"
                target="_blank"
                rel="noreferrer"
              >
                Soft Skills Academy
              </a>
            </b>
            <br />
            Soft Skills Academy enables each year hundreds of students
            in different European cities to develop their soft skills,
            by participating in free of charge training sessions.
            Sessions are organized in cooperation with EESTEC trainers,
            companies and individual professionals, who wish to share their
            knowledge with students.
          </p>
          <p>
            <b>
              <a
                href="https://eestechchallenge.eestec.net"
                target="_blank"
                rel="noreferrer"
              >
                EESTech Challenge
              </a>
            </b>
            <br />
            EESTech Challenge is our annual hackathon, in which each year approximately 300
            students take part, competing first in small teams on a certain task in the Local Rounds
            and the winners later on participating in the Final Round of the competition. Companies
            involved in the project have the chance to get in touch with
            young, talented engineers and to share their expertise as mentors or during
            a presentation or lecture. The topic of the EESTech Challenge is chosen every year
            anew to fit the current trends in technology.
          </p>
        </HeadlinedContent>
      </div>
      {/*      <div className="section contact-partners">
        <div className="contact-us">
          <Heading variant="h1">Are you interested? Contact us</Heading>
          <EmailForm onSubmit={handleSubmit} baseComponentClass="partners" />
        </div>
      </div> */}
    </div>
  );
}

export default Partners;

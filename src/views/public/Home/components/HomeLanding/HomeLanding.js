import React from 'react';

import 'views/public/Home/components/HomeLanding/HomeLanding.css';

/**
 __HomeLanding__ is the top part of the homepage
 */
function HomeLanding() {
  return (
    <div className="landing">
      <div className="landing-text-wrapper">
        <div className="landing-text">
          <p>We are</p>
          <h1 className="accent">EESTEC</h1>
          <h2>Electrical Engineering STudents&apos; European assoCiation</h2>
        </div>
      </div>
      <div className="map" />
    </div>
  );
}

export default HomeLanding;

import React, { useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import { useCountUp } from 'react-countup';

import useOnScreen from 'hooks/useOnScreen';

/**
 __Counter__ deals with the incrementing statistics visible at the homepage
 */
function Counter({ end, duration = 1.5 }) {
  const [run, setRun] = useState(false);
  const countUpRef = useRef();
  const isVisible = useOnScreen(countUpRef);
  const { start } = useCountUp({ ref: countUpRef, duration, end });
  useEffect(() => {
    if (isVisible && !run) {
      setRun(true);
      start();
    }
  }, [run, start, isVisible]);

  return <p ref={countUpRef} className="number accent" />;
}

Counter.propTypes = {
  end: PropTypes.number.isRequired,
  duration: PropTypes.number,
};

Counter.defaultProps = {
  duration: 1.5,
};

export default Counter;

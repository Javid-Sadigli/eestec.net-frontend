import React from 'react';

import Counter from 'views/public/Home/components/HomeNumbers/components/Counter';

import 'views/public/Home/components/HomeNumbers/HomeNumbers.css';

/**
 __HomeNumber__ presents the number statistics visible at the homepage
 */
function HomeNumbers() {
  return (
    <div className="home-counter section">
      <div className="home-opportunities">
        <p>
          <span className="accent">LEARN.</span>
          {' '}
          <span>TRAVEL.</span>
          {' '}
          <span className="accent">MAKE FRIENDS.</span>
        </p>
        <p><span>IMPROVE YOURSELF.</span></p>
      </div>
      <div className="home-numbers flex-centered">
        <div id="home-members">
          <Counter end={5000} />
          <p className="number-label">Members</p>
        </div>
        <div id="home-universities">
          {
            // TODO: Pull this information from BE
          }
          <Counter end={42} />
          <p className="number-label">Universities</p>
        </div>
        <div id="home-countries">
          <Counter end={24} />
          <p className="number-label">Countries</p>
        </div>
      </div>
    </div>
  );
}

export default HomeNumbers;

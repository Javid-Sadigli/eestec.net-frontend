import React from 'react';

import './EESTECVideo.css';

const VIDEO_LINK = 'https://www.youtube.com/embed/8r3V1sAcRFg?autoplay=1&mute=1';

/**
 __EESTECVideo__ is a component presenting the promotional video of EESTEC
 */
function EESTECVideo() {
  return (
    <div className="homepage-video-wrapper">
      <iframe
        className="homepage-video"
        src={VIDEO_LINK}
        title="What is EESTEC - Video"
        frameBorder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen
      />
    </div>
  );
}

export default EESTECVideo;

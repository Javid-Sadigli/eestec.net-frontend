import React, { useContext } from 'react';
import HeadlinedContent from 'components/HeadlinedContent/HeadlinedContent';
import Heading from 'components/Heading/Heading';
import PartnersSlider from 'components/PartnersSlider/PartnersSlider';
import ModalContext from 'context/ModalContext';
import useDocumentTitle from 'hooks/useDocumentTitle';
import HomeLanding from 'views/public/Home/components/HomeLanding/HomeLanding';
import HomeNumbers from 'views/public/Home/components/HomeNumbers/HomeNumbers';
import EESTECVideo from 'views/public/Home/components/EESTECVideo/EESTECVideo';
import 'views/public/Home/Home.css';

import logoEC from 'assets/img/ec-logo-white.svg';
import logoSSA from 'assets/img/ssa-logo-white.svg';

const websiteSSA = 'https://ssa.eestec.net';
const websiteEC = 'https://eestechchallenge.eestec.net';

/**
 __Home__ is a view used to display the homepage of the website
 */
function Home() {
  const { modalContent } = useContext(ModalContext);
  useDocumentTitle('EESTEC', true);

  return (
    <div id="homepage" className={modalContent ? 'blurred' : ''}>
      <HomeLanding />
      <HomeNumbers />
      <div className="section" id="section-about-us">
        <EESTECVideo />
        <HeadlinedContent
          title="About us"
          variant="h3"
          styleVariant="h1"
          fullWidthText
          className="no-side-padding"
          headingClassName="homepage-section-title"
        >
          <p>
            Our mission is to spread and strengthen a network of students across Europe. We develop
            an international collaborative environment to the students, where we can improve
            ourselves in every aspect. EESTEC experience is unique.
          </p>
        </HeadlinedContent>
      </div>
      <div className="section" id="section-projects">
        <Heading variant="h3" lineAfter styleVariant="h1" className="homepage-section-title">Projects</Heading>
        <div className="flex-centered projects">
          <a href={websiteSSA} target="_blank" rel="noreferrer">
            <img src={logoSSA} alt="Soft Skills Academy" />
          </a>
          <a href={websiteEC} target="_blank" rel="noreferrer">
            <img src={logoEC} alt="EESTech Challenge" />
          </a>
        </div>
      </div>
      <div className="section" id="section-partners">
        <Heading variant="h3" lineAfter styleVariant="h1" className="homepage-section-title">Partners</Heading>
        <PartnersSlider />
      </div>
    </div>
  );
}

export default Home;

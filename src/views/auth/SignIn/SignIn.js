import React, {
  useContext,
  useEffect,
  useMemo,
  useState,
} from 'react';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import { useLocation, useNavigate } from 'react-router-dom';
import AuthContext from 'context/AuthContext';
import AuthService from 'api/service/auth';
import UserService from 'api/service/user';
import CheckboxInput from 'components/CheckboxInput/CheckboxInput';
import TextInput from 'components/TextInput/TextInput';
import Button from 'components/Button/Button';
import ClickableIcon from 'components/ClickableIcon/ClickableIcon';
import LinkButton from 'components/LinkButton/LinkButton';
import useForm from 'hooks/useForm';
import validateFields, { validateEmail, validateNotEmpty } from 'utils/validators';
import useDocumentTitle from 'hooks/useDocumentTitle';

import 'views/auth/SignIn/SignIn.css';

/**
 __SignIn__ is a view used to display the form for login and handle the logic of this process
*/
function SignIn() {
  const location = useLocation();
  useDocumentTitle('Sign in');
  const { updateUserData } = useContext(AuthContext);
  const navigate = useNavigate();
  const [isPhaseTwo, setIsPhaseTwo] = useState(false);
  const [isRemembered, setRemembered] = useState(false);
  const [incorrectCount, setIncorrectCount] = useState(0);
  const [isResetButtonLoading, setResetButtonLoading] = useState(false);

  // a boolean value indicating whether the display of this view is for normal
  // login purposes or to verify the email address if a user was not previously
  // logged in
  const isLoginForVerification = useMemo(
    () => location?.state?.from.startsWith('/verify-email'),
    [location],
  );

  // use the useForm hook to handle inputs
  const {
    formData,
    errors,
    setErrors,
    validators,
    isFormLoading,
    handleSubmit,
    handleInputChange,
  } = useForm({
    formKeys: ['email', 'password'],
    validators: {
      email: [
        validateNotEmpty,
        validateEmail,
      ],
      password: [validateNotEmpty],
    },
    onSubmit: () => {
      AuthService.login({
        email: formData.email,
        password: formData.password,
        remember: isRemembered,
      })
        .then(() => {
          // if the login is successful, attempt updating global user data
          UserService.getUserData()
            .then((userDataResponse) => {
              const unverified = !userDataResponse.data.email_verified_at
                || !userDataResponse.data.branch_verified_at;
              // update the user data stored globally with the new value
              updateUserData(userDataResponse, !unverified);
              if (unverified) {
                // if the login is for verification, navigate back to the email verification screen
                if (isLoginForVerification) {
                  navigate(location.state.from, { replace: true });
                } else {
                  // otherwise just redirect unverified user to the EmailInfo screen
                  navigate('/login/email', {
                    state: {
                      email: formData.email,
                      verificationFaults: {
                        email: !userDataResponse.data.email_verified_at,
                        branch: !userDataResponse.data.branch_verified_at,
                      },
                      from: '/',
                    },
                  });
                }
              }
            })
            .catch(() => {
              setErrors({
                email: 'Something went wrong with receiving your data.',
              });
            });
        })
        .catch(({ data }) => {
          setErrors({
            email: data.errors?.email?.[0],
            password: data.errors?.password?.[0],
          });
        });
    },
  });

  useEffect(() => {
    if (errors) {
      // everytime an error occurs, increment the incorrect count
      setIncorrectCount((prevState) => prevState + 1);
    }
  }, [errors, setIncorrectCount]);

  // after three incorrect logins, display a suggestion for resetting the password
  useEffect(() => {
    if (incorrectCount > 2 && !isPhaseTwo) {
      setErrors(null);
      setIncorrectCount(0);
      setIsPhaseTwo(true);
    }
  }, [incorrectCount, isPhaseTwo, setErrors, setIncorrectCount, setIsPhaseTwo]);

  // handleFormPhases resets the incorrect count, errors and proceeds to phase two/one
  const handleFormPhases = () => {
    setErrors(null);
    setIncorrectCount(0);
    setIsPhaseTwo((prevState) => !prevState);
  };

  // handleForgottenPassword attempts to send a request for a password reset and
  // redirects to a confirmation page
  const handleForgottenPassword = () => {
    setResetButtonLoading(true);
    const errorsFound = validateFields({ email: validators.email }, formData);
    if (errorsFound) {
      setErrors(errorsFound);
      setResetButtonLoading(false);
    } else {
      setErrors(null);
      AuthService.forgotPassword({
        email: formData.email,
      })
        .then(() => {
          navigate('/login/email', { state: { email: formData.email, from: '/' } });
        })
        .catch(({ data }) => {
          setErrors({
            email: data.errors?.email?.[0],
          });
          setResetButtonLoading(false);
        });
    }
  };

  const goToRegister = () => navigate('/register');

  // render different forms, depending on the phase
  return isPhaseTwo ? (
    <form id="password-reset" className="form-container form-container-wide sign-in-form-container">
      <ClickableIcon
        onClick={handleFormPhases}
        icon={ArrowBackIcon}
        className="form-header-icon"
        title="Back to sign in"
      />
      <h1 className="form-heading">Reset password</h1>
      <p className="small dimmed reset-password-info">
        Tell us the email address associated with your account,
        and we’ll send you an email with a link to reset your password.
      </p>
      <TextInput
        label="Email"
        id="email"
        type="email"
        autoComplete="username"
        value={formData.email}
        onValueChange={(e) => handleInputChange('email', e.target.value)}
        errorMessage={errors?.email}
      />
      <Button
        type="submit"
        className="primary-button form-single-button"
        onClick={handleForgottenPassword}
        isLoading={isResetButtonLoading}
      >
        Reset password
      </Button>
    </form>
  ) : (
    <form id="sign-in" className="form-container sign-in-form-container">
      <h1 className="form-heading">
        {`Sign in${isLoginForVerification ? ' to verify your account' : ''}`}
      </h1>
      <TextInput
        label="Email"
        type="email"
        id="email"
        autoComplete="username"
        value={formData.email}
        onValueChange={(e) => handleInputChange('email', e.target.value)}
        errorMessage={errors?.email}
      />
      <TextInput
        label="Password"
        type="password"
        id="password"
        autoComplete="current-password"
        value={formData.password}
        onValueChange={(e) => handleInputChange('password', e.target.value)}
        errorMessage={errors?.password}
      />
      <CheckboxInput
        id="remember-me"
        label="Remember me"
        onChange={() => setRemembered((prevState) => !prevState)}
        checked={isRemembered}
      />
      <div className="horizontal-buttons">
        <Button className="secondary-button" onClick={handleFormPhases}>Forgot password?</Button>
        <Button
          type="submit"
          className="primary-button"
          isLoading={isFormLoading}
          onClick={handleSubmit}
        >
          Log in
        </Button>
      </div>
      <p className="small dimmed bottom-form-text">
        Don&apos;t have an account?
        {' '}
        <LinkButton onClick={goToRegister}>Sign up</LinkButton>
      </p>
    </form>
  );
}

export default SignIn;

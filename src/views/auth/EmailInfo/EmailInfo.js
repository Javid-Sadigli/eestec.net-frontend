import React, {
  useContext, useEffect, useLayoutEffect, useState,
} from 'react';
import {
  Navigate,
  useLocation, useNavigate, useParams,
} from 'react-router-dom';

import AuthService from 'api/service/auth';
import Button from 'components/Button/Button';
import LinkButton from 'components/LinkButton/LinkButton';
import ErrorMessage from 'components/Message/components/ErrorMessage/ErrorMessage';
import SuccessMessage from 'components/Message/components/SuccessMessage/SuccessMessage';
import AuthContext from 'context/AuthContext';

// getTexts returns the proper text information for the EmailInfo screen,
// depending on the values of referer and state
function getTexts(referer, state) {
  const texts = {};
  if (referer === 'register') {
    texts.heading = <h1 className="form-heading">Email verification</h1>;
    texts.details = (
      <>
        <p className="small justified">
          <span>
            Your account was created, yet in order for you to be able to access the
            internal part of the website, we need to verify your email address.
          </span>
        </p>
        <p className="small justified">
          <span>
            Additionally, your account will have to be verified by an administrator of your branch.
          </span>
        </p>
        <p className="small justified">
          <span>
            An email was sent to
            {' '}
            <strong>{state.email}</strong>
            , containing a verification link.
            Follow the instructions given within the message to activate your account.
          </span>
        </p>
        <p className="small justified">
          <span>
            You are now logged in and you can browse some parts of the website or
            edit your profile settings.
          </span>
        </p>
      </>
    );
  } else if (state.verificationFaults) {
    let verificationFaultsDetails;
    if (state.verificationFaults.email) {
      verificationFaultsDetails = 'your verification of the email address';
      if (state.verificationFaults.branch) {
        verificationFaultsDetails += ' and a verification from your branch administrator';
      }
    } else {
      verificationFaultsDetails = 'a verification from your branch administrator';
    }

    texts.heading = <h1 className="form-heading">Waiting for verification</h1>;
    texts.details = (
      <>
        <p className="small justified">
          <span>
            You are now logged in and you can browse some parts of the website or
            edit your profile settings.
          </span>
        </p>
        <p className="small justified">
          <span>
            However, your account is not fully verified yet. We are still waiting
            for
            {' '}
            {verificationFaultsDetails}
            .
          </span>
        </p>
      </>
    );
  } else {
    texts.heading = <h1 className="form-heading">Password resetting</h1>;
    texts.details = (
      <p className="small justified">
        <span>
          If there exists an account associated with
          {' '}
          <strong>{state.email}</strong>
          , an email was sent to this address, containing a link to reset your password.
          Check your inbox and follow the instructions given within the message.
        </span>
      </p>
    );
  }
  return texts;
}

/**
 __EmailInfo__ is a view that presents user with information regarding their verification status
 after a successful login.
 */
function EmailInfo() {
  const { enableAuthRedirection } = useContext(AuthContext);
  const navigate = useNavigate();
  const { state } = useLocation();
  const { referer } = useParams();
  const [isSendEmailLoading, setSendEmailLoading] = useState(false);
  const [emailResponse, setEmailResponse] = useState(null);

  useLayoutEffect(() => {
    enableAuthRedirection();
  }, [enableAuthRedirection]);

  // handling the "Send again" button
  // TODO: move it to a separate hook as its reused by two different views
  useEffect(() => {
    if (isSendEmailLoading) {
      AuthService.resendEmailVerification()
        .then(() => {
          setEmailResponse({
            success: true,
            details: 'The email has been sent.',
          });
          setSendEmailLoading(false);
        })
        .catch(() => {
          setEmailResponse({
            success: false,
            details: 'An error occurred.',
          });
          setSendEmailLoading(false);
        });
    }
  }, [isSendEmailLoading, setEmailResponse, setSendEmailLoading]);

  if (!state) {
    return (
      <Navigate
        to="/404"
        replace
      />
    );
  }

  const { heading, details } = getTexts(referer, state);

  let goToHomepageSection;
  let emailResponseIndicator = null;
  // if email is unverified, render button to send the email verification message again
  if (state?.verificationFaults?.email) {
    goToHomepageSection = (
      <div className="text-and-button-container">
        <p className="small">
          Haven&apos;t received an email?
          {' '}
          <LinkButton
            onClick={() => setSendEmailLoading(true)}
            disabled={isSendEmailLoading}
            className="send-again-button"
          >
            Send again
          </LinkButton>
        </p>
        <Button
          className="secondary-button"
          onClick={() => navigate(state.from)}
        >
          Go to homepage
        </Button>
      </div>
    );
    // if an email was sent (meaning we have emailResponse), display the proper information
    if (emailResponse) {
      emailResponseIndicator = emailResponse.success ? (
        <SuccessMessage className="unrelated">{emailResponse.details}</SuccessMessage>
      ) : (
        <ErrorMessage className="unrelated">{emailResponse.details}</ErrorMessage>
      );
    }
  } else {
    // if the email is verified, just allow returning to homepage
    goToHomepageSection = (
      <Button
        className="secondary-button form-single-button"
        onClick={() => navigate(state.from)}
      >
        Go to homepage
      </Button>
    );
  }

  return (
    <div className="form-container form-container-wide email-info-form-container justified">
      {heading}
      {details}
      {goToHomepageSection}
      {emailResponseIndicator}
    </div>
  );
}

export default EmailInfo;

import React, { useMemo, useState } from 'react';
import TextInput from 'components/TextInput/TextInput';
import Button from 'components/Button/Button';
import { useNavigate, useSearchParams } from 'react-router-dom';
import useForm from 'hooks/useForm';
import {
  validateMinLength, validateNotEmpty, validateSamePasswords,
} from 'utils/validators';
import AuthService from 'api/service/auth';
import useDocumentTitle from 'hooks/useDocumentTitle';

/**
 __PasswordReset__ is a view that a user sees after clicking an link
 from the email message received after initiation the process of password reset.
 It allows resetting a password.
 */
function PasswordReset() {
  useDocumentTitle('Reset your password');
  // phase two changes the content to a message about a successful reset process
  const [isPhaseTwo, setIsPhaseTwo] = useState(false);
  const navigate = useNavigate();
  const [searchParamsIterator] = useSearchParams();

  // get searchParams as an object from the iterator
  const searchParams = useMemo(
    () => Object.fromEntries([...searchParamsIterator]),
    [searchParamsIterator],
  );

  // handle the data with useForm to include validation
  const {
    formData,
    setErrors,
    errors,
    isFormLoading,
    handleSubmit,
    handleInputChange,
  } = useForm({
    formKeys: ['password', 'passwordConfirmation'],
    validators: {
      password: [
        validateNotEmpty,
        (password) => validateMinLength(password, 8),
        (password) => validateSamePasswords(password, formData.passwordConfirmation),
      ],
      passwordConfirmation: [
        validateNotEmpty,
        (password) => validateSamePasswords(password, formData.password),
      ],
    },
    onSubmit: () => {
      AuthService.resetPassword({
        password: formData.password,
        password_confirmation: formData.passwordConfirmation,
        email: searchParams.email,
      }, searchParams.token)
        // if success, just set the phase to the second one, indicating success
        .then(() => setIsPhaseTwo(true))
        .catch(({ data }) => {
          setErrors({
            password: data.errors?.password?.[0] || data.errors?.email?.[0],
          });
        });
    },
  });

  let content;

  // if any parameter is missing, display information about an invalid link
  if (!searchParams.token || !searchParams.email) {
    content = (
      <div className="form-container form-container-wide">
        <h1 className="form-heading">Invalid link</h1>
        <p className="small justified">
          <span>
            Your password reset link is invalid.
            Please make sure it is correct or try initiating the process again.
          </span>
        </p>
      </div>
    );
  } else {
    content = isPhaseTwo ? (
      <div className="form-container form-container-wide">
        <h1 className="form-heading">Password reset successfully</h1>
        <p className="small justified">
          <span>
            Your new password has been set successfully. You can now log in using your new password.
          </span>
        </p>
        <div className="horizontal-buttons">
          <Button
            className="secondary-button"
            onClick={() => navigate('/')}
          >
            Go to homepage
          </Button>
          <Button
            className="primary-button"
            onClick={() => navigate('/login')}
          >
            Go to login
          </Button>
        </div>
      </div>
    ) : (
      <form id="reset-password" className="form-container">
        <h1 className="form-heading">Set a new password</h1>
        <TextInput
          label="Password"
          type="password"
          id="password"
          autoComplete="new-password"
          value={formData.password}
          onValueChange={(e) => handleInputChange('password', e.target.value)}
          errorMessage={errors?.password}
        />
        <TextInput
          label="Confirm password"
          type="password"
          autoComplete="new-password"
          id="password-confirmation"
          value={formData.passwordConfirmation}
          onValueChange={(e) => handleInputChange('passwordConfirmation', e.target.value)}
          errorMessage={errors?.passwordConfirmation}
        />
        <Button
          type="submit"
          className="primary-button form-single-button"
          onClick={handleSubmit}
          isLoading={isFormLoading}
        >
          Reset password
        </Button>
      </form>
    );
  }

  return content;
}

export default PasswordReset;

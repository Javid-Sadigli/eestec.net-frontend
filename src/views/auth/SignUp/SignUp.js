import React, { useContext, useEffect, useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import ArrowForwardIcon from '@mui/icons-material/ArrowForward';
import ArrowBackIcon from '@mui/icons-material/ArrowBack';
import BranchService from 'api/service/branches';
import AuthService from 'api/service/auth';
import UserService from 'api/service/user';
import TextInput from 'components/TextInput/TextInput';
import Button from 'components/Button/Button';
import ClickableIcon from 'components/ClickableIcon/ClickableIcon';
import Autocomplete from 'components/Autocomplete/Autocomplete';
import LinkButton from 'components/LinkButton/LinkButton';
import useForm from 'hooks/useForm';
import AuthContext from 'context/AuthContext';
import validateFields, {
  validateEmail,
  validateMinLength,
  validateNotEmpty,
  validateSamePasswords,
} from 'utils/validators';
import useDocumentTitle from 'hooks/useDocumentTitle';
import logger from 'utils/logger';
import { getBranchTypeAndName, sortBranchesByTypeAndName } from 'utils/branches';

import 'views/auth/SignUp/SignUp.css';

function isBranchTheOnlyIssue(errors) {
  const keys = Object.keys(errors);
  return keys.length === 1 && keys.includes('branch');
}

/**
 __SignUp__ is a view used to display the form for registration and handle the logic
 of this process
 */
function SignUp() {
  useDocumentTitle('Sign up');
  const { updateUserData } = useContext(AuthContext);
  const navigate = useNavigate();

  const [isPhaseTwo, setIsPhaseTwo] = useState(false);
  const [isPhaseTwoLoading, setIsPhaseTwoLoading] = useState(false);
  const [pickedBranch, setPickedBranch] = useState(null);
  const [branchError, setBranchError] = useState(null);
  const [branchList, setBranchList] = useState([]);

  // use the useForm hook to handle inputs
  const {
    formData,
    errors,
    setErrors,
    validators,
    isFormLoading,
    setFormLoading,
    handleSubmit,
    handleInputChange,
  } = useForm({
    formKeys: ['firstName', 'lastName', 'email', 'password', 'passwordConfirmation'],
    validators: {
      firstName: [validateNotEmpty],
      lastName: [validateNotEmpty],
      email: [validateNotEmpty, validateEmail],
      password: [
        validateNotEmpty,
        (password) => validateMinLength(password, 8),
        (password) => validateSamePasswords(password, formData.passwordConfirmation),
      ],
      passwordConfirmation: [
        validateNotEmpty,
        (password) => validateSamePasswords(password, formData.password),
      ],
    },
    // onSubmit handles the actual registration, it is executed after a submission of the form
    // after selecting the branch
    onSubmit: () => {
      AuthService.register({
        first_name: formData.firstName,
        last_name: formData.lastName,
        email: formData.email,
        password: formData.password,
        password_confirmation: formData.passwordConfirmation,
        branch: pickedBranch?.cityName,
        terms: true,
      })
        .then(() => {
          UserService.getUserData()
            .then((userDataResponse) => {
              updateUserData(userDataResponse, false);
              navigate('/register/email', { state: { email: formData.email, from: '/' } });
            })
            .catch(() => {
              setErrors({
                email: 'Something went wrong with receiving your data.',
              });
            });
        })
        .catch(({ data }) => {
          // if branch is the only issue in the second phase, just display an error
          if (isBranchTheOnlyIssue(data.errors)) {
            setBranchError(data.errors?.branch?.[0]);
            setFormLoading(false);
          } else {
            // otherwise switch the phase back to the first one
            setBranchError(data.errors?.branch?.[0]);
            setErrors({
              email: data.errors?.email?.[0],
              password: data.errors?.password?.[0],
              passwordConfirmation: data.errors?.password_confirmation?.[0],
              firstName: data.errors?.first_name?.[0],
              lastName: data.errors?.last_name?.[0],
            });
            setIsPhaseTwo(false);
          }
        });
    },
  });

  // Getting branches list API call
  useEffect(() => {
    if (isPhaseTwo) {
      BranchService.getBranches()
        .then((response) => setBranchList(response.sort(sortBranchesByTypeAndName).map(({
          // eslint-disable-next-line camelcase
          name, slug, id, branch_type,
        }) => ({
          name: getBranchTypeAndName(name, branch_type),
          cityName: name,
          slug,
          id,
        }))))
        .catch((err) => logger.log(err))
        .finally(() => setIsPhaseTwoLoading(false));
    }
  }, [isPhaseTwo, setIsPhaseTwoLoading, setBranchList]);

  const handleFormPhases = () => setIsPhaseTwo((prevState) => !prevState);

  // launchPhaseTwo handles the first phase of the form, it sends data without the branch,
  // verifies such data and if the error returned is only regarding the missing branch
  // the second phase is launched, otherwise the rest of the errors is shown
  const launchPhaseTwo = () => {
    setIsPhaseTwoLoading(true);
    const errorsFound = validateFields(validators, formData);
    if (errorsFound) {
      setErrors(errorsFound);
      setIsPhaseTwoLoading(false);
    } else {
      setErrors(null);
      AuthService.register({
        first_name: formData.firstName,
        last_name: formData.lastName,
        email: formData.email,
        password: formData.password,
        password_confirmation: formData.passwordConfirmation,
        terms: true,
      })
        .then(() => {
          // This occurs when a user is authenticated but their local storage did not preserve
          // this information. Logout the user and allow registration
          AuthService.logout()
            .then(() => launchPhaseTwo())
            .catch(() => {
              setErrors({
                email: 'Something went wrong. Please try again.',
              });
              setIsPhaseTwoLoading(false);
            });
        })
        .catch(({ data }) => {
          // the first submission of the form data SHOULD result in an error
          // since the branch is not picked yet, if this is the only error
          // that means that everything is as expected and the second phase should be launched
          if (isBranchTheOnlyIssue(data.errors)) {
            handleFormPhases();
          } else {
            // otherwise display the errors and do not proceed to the second phase
            setErrors({
              email: data.errors?.email?.[0],
              password: data.errors?.password?.[0],
              passwordConfirmation: data.errors?.password_confirmation?.[0],
              firstName: data.errors?.first_name?.[0],
              lastName: data.errors?.last_name?.[0],
            });
            setIsPhaseTwoLoading(false);
          }
        });
    }
  };

  const goToLogin = () => navigate('/login');

  // render different forms, depending on the phase
  return (isPhaseTwo && branchList.length) ? (
    <form id="branch-selection" className="form-container form-container-wide sign-up-form-container">
      <ClickableIcon
        onClick={handleFormPhases}
        icon={ArrowBackIcon}
        className="form-header-icon"
        title="Back to sign up"
      />
      <h1 className="form-heading">Step 2 of 2</h1>
      <p className="small dimmed branch-selection-info">
        In order to create your account, you will have to
        choose the EESTEC branch that you are a member of.
      </p>
      <Autocomplete
        id="branch-selection-dropdown"
        label="Choose branch"
        options={branchList}
        optionLabel="name"
        errorMessage={branchError}
        onChange={setPickedBranch}
      />
      <div className="text-and-button-container">
        <p className="small">
          By signing up, you agree to the
          {' '}
          <Link to="/terms-and-conditions" target="_blank" rel="noopener noreferrer">Terms and Conditions</Link>
          {' '}
          of EESTEC.
        </p>
        <Button
          type="submit"
          className="primary-button"
          isLoading={isFormLoading}
          onClick={handleSubmit}
        >
          Sign up
        </Button>
      </div>
    </form>
  ) : (
    <form id="sign-up" className="form-container form-container-wide sign-up-form-container">
      <h1 className="form-heading">Create your account</h1>
      <div className="horizontal-inputs">
        <TextInput
          label="First name"
          id="first-name"
          type="text"
          value={formData.firstName}
          onValueChange={(e) => handleInputChange('firstName', e.target.value)}
          errorMessage={errors?.firstName}
        />
        <TextInput
          label="Last name"
          id="last-name"
          type="text"
          value={formData.lastName}
          onValueChange={(e) => handleInputChange('lastName', e.target.value)}
          errorMessage={errors?.lastName}
        />
      </div>
      <TextInput
        label="Email"
        id="email"
        type="email"
        autoComplete="username"
        value={formData.email}
        onValueChange={(e) => handleInputChange('email', e.target.value)}
        errorMessage={errors?.email}
      />
      <TextInput
        label="Password"
        id="password"
        type="password"
        autoComplete="new-password"
        value={formData.password}
        onValueChange={(e) => handleInputChange('password', e.target.value)}
        errorMessage={errors?.password}
      />
      <TextInput
        label="Confirm password"
        id="password-confirmation"
        autoComplete="new-password"
        type="password"
        value={formData.passwordConfirmation}
        onValueChange={(e) => handleInputChange('passwordConfirmation', e.target.value)}
        errorMessage={errors?.passwordConfirmation}
      />
      <Button
        type="submit"
        className="primary-button form-single-button"
        icon={ArrowForwardIcon}
        onClick={launchPhaseTwo}
        isLoading={isPhaseTwoLoading}
      >
        Next
      </Button>
      <p className="small dimmed bottom-form-text">
        Already have an account?
        {' '}
        <LinkButton onClick={goToLogin}>Sign in</LinkButton>
      </p>
    </form>
  );
}

export default SignUp;

import React, {
  useContext, useLayoutEffect, useMemo, useState, useCallback,
} from 'react';
import {
  Navigate, useNavigate, useParams, useSearchParams,
} from 'react-router-dom';
import useDocumentTitle from 'hooks/useDocumentTitle';
import Button from 'components/Button/Button';
import { CircularProgress } from '@mui/material';
import AuthService from 'api/service/auth';
import UserService from 'api/service/user';
import AuthContext from 'context/AuthContext';
import logger from 'utils/logger';

// mapping of verification statuses
const VerificationStatus = {
  INIT: 0,
  LINK_INVALID: 1,
  LINK_EXPIRED: 2,
  STARTED: 3,
  SUCCESS: 4,
  ERROR: 5,
};

// serve errors without an exhaustive descriptions from BE
const errorMapping = {
  'Invalid signature.': 'Your email verification link is invalid. '
    + 'Please make sure it is correct or try initiating the process again '
    + 'from your profile page.',
};

/**
 __EmailVerification__ is the screen presented to the user after clicking the
 email verification button found in the email message.
 */
function EmailVerification() {
  useDocumentTitle('Email verification');
  const { updateUserData, userData } = useContext(AuthContext);
  const { token } = useParams();
  const [verificationStatus, setVerificationStatus] = useState(VerificationStatus.INIT);
  const [error, setError] = useState(null);
  const navigate = useNavigate();
  const [searchParamsIterator] = useSearchParams();
  // get searchParams as an object from the iterator
  const searchParams = useMemo(
    () => Object.fromEntries([...searchParamsIterator]),
    [searchParamsIterator],
  );

  // after useLayoutEffect verifies the validity of the link, try verifying the email
  // with the token provided by the link from the email message
  const verify = useCallback(() => {
    setVerificationStatus(VerificationStatus.STARTED);
    AuthService.verifyEmail(
      userData.id,
      token,
      {
        expires: searchParams.expires,
        signature: searchParams.signature,
      },
    ).then(() => {
      UserService.getUserData()
        .then((userDataResponse) => {
          updateUserData(userDataResponse);
          setVerificationStatus(VerificationStatus.SUCCESS);
        })
        .catch((err) => {
          logger.log(err);
          // verification was a success so set such a status anyway
          setVerificationStatus(VerificationStatus.SUCCESS);
        });
    }).catch(({ data }) => {
      setError(errorMapping[data.message] || data.message);
      setVerificationStatus(VerificationStatus.ERROR);
    });
  }, [setError, setVerificationStatus, token, searchParams, userData.id, updateUserData]);

  // before any other action is done, check if the link is valid
  useLayoutEffect(() => {
    if (verificationStatus === VerificationStatus.INIT) {
      if (
        !searchParams.expires
        || !searchParams.signature
        // eslint-disable-next-line no-restricted-globals
        || isNaN(new Date(searchParams.expires * 1000))
      ) {
        setVerificationStatus(VerificationStatus.LINK_INVALID);
      } else if (new Date(searchParams.expires * 1000) < new Date()) {
        setVerificationStatus(VerificationStatus.LINK_EXPIRED);
      } else {
        verify();
      }
    }
  }, [verify, verificationStatus, setVerificationStatus, searchParams]);

  // if the user is already verified, redirect to homepage
  if (verificationStatus === VerificationStatus.INIT && userData.email_verified_at) {
    return <Navigate to="/" replace />;
  }

  let content;
  let textHeading;
  // depending on the verification status, set the proper information in content
  switch (verificationStatus) {
    case VerificationStatus.LINK_INVALID:
      content = (
        <p className="small justified">
          <span>
            Your email verification link is invalid.
            Please make sure it is correct or try initiating the process again
            from your profile page.
          </span>
        </p>
      );
      textHeading = 'Invalid link';
      break;
    case VerificationStatus.LINK_EXPIRED:
      content = (
        <p className="small justified">
          <span>
            Your email verification link has expired.
            Please try initiating the process again from your profile page.
          </span>
        </p>
      );
      textHeading = 'Link expired';
      break;
    case VerificationStatus.ERROR:
      content = (
        <p className="small justified">
          <span>
            {error}
          </span>
        </p>
      );
      textHeading = 'Verification failed';
      break;
    case VerificationStatus.SUCCESS:
      content = (
        <>
          <p className="small justified">
            <span>
              Your email has been successfully verified!
            </span>
          </p>
          {!userData.branch_verified_at && (
          <p className="small justified">
            <span>
              However, your account is not fully activated yet.
              We are still waiting for a verification from your branch administrator.
              You can still browse some parts of the website or edit your profile settings.
            </span>
          </p>
          )}
          <Button
            className="secondary-button form-single-button"
            onClick={() => navigate('/')}
          >
            Go to homepage
          </Button>
        </>
      );
      textHeading = 'Verification successful';
      break;
    default:
      content = (
        <div className="card-loader">
          <CircularProgress size={70} />
        </div>
      );
      textHeading = 'Please wait...';
  }

  return (
    <div className="form-container form-container-wide">
      <h1 className="form-heading">{textHeading}</h1>
      {content}
    </div>
  );
}

export default EmailVerification;

import React, { useContext } from 'react';
import { Navigate, useParams } from 'react-router-dom';
import AuthContext from 'context/AuthContext';
import Private from 'routes/Private';
import Profile from 'views/internal/Profile/Profile';

/**
 __ProfileRouting__ redirects the user to `/my-profile` if they tried to access
 their own profile by using the route `/users/<THEIR_SLUG>`
*/
function ProfileRouting() {
  const { userData } = useContext(AuthContext);
  const { userSlug } = useParams();

  return userSlug === userData?.slug ? (
    <Navigate to="/my-profile" replace />
  ) : (
    <Private Component={Profile} />
  );
}

export default ProfileRouting;

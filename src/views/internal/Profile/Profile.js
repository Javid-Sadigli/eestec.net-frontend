/* eslint-disable camelcase */
import React, {
  useContext, useEffect, useMemo, useRef, useState,
} from 'react';
import AuthContext from 'context/AuthContext';
import { Link, useNavigate, useParams } from 'react-router-dom';
import UserService from 'api/service/user';
import EntityHeader from 'components/EntityHeader/EntityHeader';
import useDocumentTitle from 'hooks/useDocumentTitle';
import EntitySection from 'components/EntitySection/EntitySection';
import useEvents from 'hooks/useEvents';
import CardList from 'components/CardList/CardList';
import EventCard from 'components/EventCard/EventCard';
import useUserRefs from 'views/internal/Profile/hooks/useUserRefs';
import StickyMenu from 'components/StickyMenu/StickyMenu';
import WarningMessage from 'components/Message/components/WarningMessage/WarningMessage';
import LinkButton from 'components/LinkButton/LinkButton';
import AuthService from 'api/service/auth';
import Button from 'components/Button/Button';
import ExpandLessIcon from '@mui/icons-material/ExpandLess';
import useEntityEdit from 'hooks/useEntityEdit';
import ConditionalInput from 'components/ConditionalInput/ConditionalInput';
import Paragraph from 'utils/componentsWrappers/Paragraph';
import Div from 'utils/componentsWrappers/Div';
import TextInput from 'components/TextInput/TextInput';
import EditIcons from 'components/EditIcons/EditIcons';
import CircularProgress from '@mui/material/CircularProgress';
import Tooltip from '@mui/material/Tooltip';
import CameraAlt from '@mui/icons-material/CameraAlt';
import VrpanoIcon from '@mui/icons-material/Vrpano';
import WarningIcon from '@mui/icons-material/Warning';
import BlockIcon from '@mui/icons-material/Block';
import ImageFileInput from 'components/ImageFileInput/ImageFileInput';
import { capitalizeFirstLetter, createParagraphs, removeNbsp } from 'utils/misc';
import { getBranchTypeAndName, isBranchTerminated } from 'utils/branches';
import logger from 'utils/logger';
import EmailIcon from '@mui/icons-material/Email';

import './Profile.css';

import personPlaceholder from 'assets/img/member-placeholder.jpg';

// Proper mapping of data for profiles not the user's ones
const mapProfileData = (data) => ({
  first_name: data.first_name,
  last_name: data.last_name,
  slug: data.slug,
  email_verified_at: data.email_verified_at,
  branch_verified_at: data.branch_verified_at,
  profile_photo_path: data.profile_photo_path,
  banner_path: data.banner_path,
  branches: data.branches,
  branch_applications: data.branch_applications,
  description: removeNbsp(data.description),
});

// keys for different data that should be rendered for the event sections of the profile
const eventSectionKeys = [
  {
    name: 'application',
    title: 'Open for applications',
    buttonText: 'Apply now',
    emptyMessage: 'no events open for applications',
  },
  {
    name: 'upcoming',
    title: 'Upcoming',
    emptyMessage: 'no upcoming events',
  },
  {
    name: 'inProgress',
    title: 'In progress',
    emptyMessage: 'no events in progress',
  },
  {
    name: 'past',
    title: 'Past',
    emptyMessage: 'no past events',
    hasShowMoreButton: true,
  },
];

/**
 __Profile__ is a view used to display the profile page - own or someone else's
 */
function Profile() {
  const { updateUserData, userData } = useContext(AuthContext);
  const descriptionRef = useRef();
  const navigate = useNavigate();
  const { userSlug } = useParams();
  const isMyProfile = useMemo(() => !userSlug, [userSlug]);
  const [profileData, setProfileData] = useState(null);
  const [isDescriptionWrapped, setIsDescriptionWrapped] = useState(true);
  const [isSendEmailLoading, setSendEmailLoading] = useState(false);
  const [emailResponse, setEmailResponse] = useState(null);
  const {
    formatAndSetEvents: setOrganizedEvents,
    toggleEventsLoading: toggleOrganizedEventsLoading,
    ...organizedEvents
  } = useEvents();
  const {
    formatAndSetEvents: setAttendedEvents,
    toggleEventsLoading: toggleAttendedEventsLoading,
    ...attendedEvents
  } = useEvents();

  // take advantage of the useEntityEdit hook to handle data changing
  const {
    setEntityEditFormData,
    toggleIsBeingEdited,
    onEntityEditSubmit,
    isEntityEditLoading,
    isEditingDisabled,
    isBeingEdited,
    entityEditFormData,
    isAnyBeingEdited,
    resetEntityEditFormData,
  } = useEntityEdit({
    apiServiceCall: (formData, requestArgs) => UserService.editUserData(
      userData.slug,
      formData,
      ...requestArgs,
    ),
    data: profileData,
    setData: setProfileData,
    // updateUserData as a success callback, used for updating the global
    // user state after we make changes on the page itself
    successCallback: (newData) => updateUserData({ data: newData }, true, true),
  });
  const userRefs = useUserRefs();
  useDocumentTitle(profileData
    ? `${capitalizeFirstLetter(profileData.first_name)} ${capitalizeFirstLetter(profileData.last_name)}`
    : null);

  // prefixes for section titles, depending on who is viewing the profile
  const personPrefix = useMemo(
    () => (userSlug ? {
      sentence: 'This person has ',
      link: '',
    } : {
      sentence: 'You have ',
      link: 'My ',
    }),
    [userSlug],
  );

  const hasOrganizedEvents = useMemo(() => (
    Object.values(organizedEvents.eventsLoading)
      .some((elem) => !!elem)
    || Object.keys(organizedEvents.events)
      .map((eventKey) => organizedEvents.events[eventKey].length)
      .some((elem) => elem > 0)
  ), [organizedEvents.events, organizedEvents.eventsLoading]);

  const hasAttendedEvents = useMemo(() => (
    Object.values(attendedEvents.eventsLoading)
      .some((elem) => !!elem)
    || Object.keys(attendedEvents.events)
      .map((eventKey) => attendedEvents.events[eventKey].length)
      .some((elem) => elem > 0)
  ), [attendedEvents.events, attendedEvents.eventsLoading]);

  // this effect handles fetching data on mount - profile info
  useEffect(() => {
    // Creates an AbortController which aborts ongoing requests if component is unmounted
    const abortController = new AbortController();

    if (isMyProfile) {
      UserService.getUserData(abortController.signal)
        .then(({ data }) => {
          updateUserData({ data });
          setProfileData({
            ...data,
            description: removeNbsp(data.description),
          });
        })
        .catch((err) => {
          if (err?.status !== 404) {
            logger.log(err);
          } else {
            navigate('/404', { replace: true });
          }
        });
    } else {
      UserService.getUserProfile(userSlug, abortController.signal)
        .then((response) => {
          setProfileData(mapProfileData(response));
        })
        .catch((err) => {
          if (err?.status !== 404) {
            logger.log(err);
          } else {
            navigate('/404', { replace: true });
          }
        });
    }

    // during unmount of the component abort the ongoing requests to avoid memory leak
    return () => abortController.abort();
  }, [
    isMyProfile,
    setProfileData,
    updateUserData,
    navigate,
    userData.slug,
    userSlug,
  ]);

  // this effect handles fetching data on mount - events info
  useEffect(() => {
    // Creates an AbortController which aborts ongoing requests if component is unmounted
    const abortController = new AbortController();

    UserService.getUserAttendedEvents(userSlug || userData.slug, abortController.signal)
      .then((response) => {
        setAttendedEvents(response);
        toggleAttendedEventsLoading();
      })
      .catch((err) => {
        if (err?.message !== 'canceled' && err?.status !== 404) {
          logger.log(err);
          toggleAttendedEventsLoading();
        }
      });

    UserService.getUserOrganizedEvents(userSlug || userData.slug, abortController.signal)
      .then((response) => {
        setOrganizedEvents(response);
        toggleOrganizedEventsLoading();
      })
      .catch((err) => {
        if (err?.message !== 'canceled' && err?.status !== 404) {
          logger.log(err);
          toggleOrganizedEventsLoading();
        }
      });

    // during unmount of the component abort the ongoing requests to avoid memory leak
    return () => abortController.abort();
  }, [
    toggleOrganizedEventsLoading,
    setOrganizedEvents,
    toggleAttendedEventsLoading,
    setAttendedEvents,
    userData.slug,
    userSlug,
  ]);

  // handling the "Send again" button
  // TODO: move it to a separate hook as its reused by two different views
  useEffect(() => {
    if (isSendEmailLoading) {
      AuthService.resendEmailVerification()
        .then(() => {
          setEmailResponse({
            success: true,
            details: 'The email has been sent.',
          });
          setSendEmailLoading(false);
        })
        .catch(() => {
          setEmailResponse({
            success: false,
            details: 'An error occurred.',
          });
          setSendEmailLoading(false);
        });
    }
  }, [isSendEmailLoading, setEmailResponse, setSendEmailLoading]);

  // stickyMenuElements are used to organize data for the sticky menu
  // TODO: maybe it would be possible to create a hook like useStickyMenu
  //  to have all the logic behind stickyMenu and needed refs in one place
  const stickyMenuElements = [
    {
      title: `${personPrefix.link}Events`,
      target: userRefs.events.all,
      subElements: [
        {
          title: 'Organized',
          target: userRefs.events.organized.all,
          isEmpty: !hasOrganizedEvents,
        },
        {
          title: 'Attended',
          target: userRefs.events.attended.all,
          isEmpty: !hasAttendedEvents,
        },
      ].filter((subElement) => !subElement.isEmpty),
    },
  ];

  // generateEventSection renders the EntitySection for events using the provided key argument
  // TODO: This function can be moved to a separate hook of some sorts in order to make it reusable
  //  for other Entity pages, like Branch
  const generateEventSection = (sectionKey) => {
    let visibilityCondition;
    let eventGroup;
    if (sectionKey === 'organized') {
      visibilityCondition = hasOrganizedEvents;
      eventGroup = organizedEvents;
    } else {
      visibilityCondition = hasAttendedEvents;
      eventGroup = attendedEvents;
    }
    return (
      <EntitySection
        title={sectionKey}
        visibilityCondition={visibilityCondition}
        headingClassName="card-subheading"
        headingVariant="h2"
        innerRef={userRefs.events[sectionKey].all}
        nested
      >
        {eventSectionKeys.map(({
          name, title, buttonText, emptyMessage, hasShowMoreButton,
        }) => (
          <EntitySection
            title={title}
            key={`${sectionKey}-${name}`}
            headingClassName="card-subheading"
            headingVariant="h3"
            visibilityCondition={!!eventGroup.events[name].length}
            className="event-group"
          >
            <CardList
              items={eventGroup.events[name]}
              cardButtonsText={buttonText}
              emptyMessage={`${personPrefix.sentence}${emptyMessage}`}
              loading={eventGroup.eventsLoading.application}
              cardType={EventCard}
              horizontal
              showMoreButton={hasShowMoreButton ? {
                visibilityCondition: eventGroup.morePastEventsAvailable,
                onClick: eventGroup.loadMorePastEvents,
                isLoading: eventGroup.morePastEventsLoading,
                text: 'Show more',
              } : null}
            />
          </EntitySection>
        ))}
      </EntitySection>
    );
  };

  const verificationWarnings = [];

  // Depending if the viewing user is the same as the profile, display more or less
  // specific information
  if (isMyProfile) {
    if (!profileData?.branch_verified_at) {
      verificationWarnings.push(
        <WarningMessage className="unrelated" key="branch-warning">
          Branch not verified
        </WarningMessage>,
      );
    }
    if (!profileData?.email_verified_at) {
      verificationWarnings.push(
        <WarningMessage className="unrelated" key="email-warning">
          <>
            Email not verified.
            {' '}
            <LinkButton
              onClick={() => setSendEmailLoading(true)}
              disabled={isSendEmailLoading || !!emailResponse}
              className="send-again-button"
            >
              {emailResponse ? emailResponse.details : 'Send verification email again'}
            </LinkButton>
          </>
        </WarningMessage>,
      );
    }
  } else if (!profileData?.branch_verified_at || !profileData?.email_verified_at) {
    verificationWarnings.push(
      <WarningMessage className="unrelated" key="verification-warning">
        User not verified
      </WarningMessage>,
    );
  }

  const shouldDisplayDescriptionWrapButton = !descriptionRef.current
    ? false : descriptionRef.current.scrollHeight > descriptionRef.current.clientHeight;

  return (
    <div className="entity-wrapper" id="profile">
      <EntityHeader
        backgroundImage={profileData?.banner_path}
        blurred={!!profileData}
        className="person-header"
      >
        {(profileData && isMyProfile) && (
          <ImageFileInput
            onChange={(file) => onEntityEditSubmit(['banner_path'], false, file, [true, false])}
            title="Change the banner picture (max. 2MB)"
            disabled={isEditingDisabled || isAnyBeingEdited}
            Icon={VrpanoIcon}
            isLoading={isEntityEditLoading?.banner_path}
          />
        )}
      </EntityHeader>
      {profileData ? (
        <div className="entity-column-layout">
          <div className="entity-sidebar">
            {/* TODO: Entity sidebar can be a separate reusable component */}
            <div className="entity-sidebar-main-picture-wrapper">
              <img
                className={
                  `entity-sidebar-main-picture${verificationWarnings.length > 0 ? ' unverified' : ''}`
                }
                src={profileData.profile_photo_path || personPlaceholder}
                alt={`${profileData.first_name} ${profileData.last_name}`}
              />
              {isMyProfile && (
                <ImageFileInput
                  onChange={(file) => onEntityEditSubmit(['profile_photo_path'], false, file, [false, true])}
                  title="Change the profile picture (max. 2MB)"
                  disabled={isEditingDisabled || isAnyBeingEdited}
                  Icon={CameraAlt}
                  isLoading={isEntityEditLoading?.profile_photo_path}
                />
              )}
            </div>
            <div className="entity-sidebar-text-content">
              <div className="entity-sidebar-titles">
                <h1 className="entity-sidebar-title">
                  <ConditionalInput
                    onChange={(e) => setEntityEditFormData('first_name', e.target.value)}
                    staticComponent={{
                      element: Paragraph,
                      props: {
                        className: 'username capitalized',
                      },
                    }}
                    editableContent={entityEditFormData?.first_name}
                    content={<span>{profileData?.first_name}</span>}
                    condition={isBeingEdited?.last_name}
                    inputComponent={{
                      element: TextInput,
                      props: {
                        placeholder: 'First name',
                        className: 'username',
                      },
                    }}
                    isAllowedToEdit={isMyProfile}
                    isInputDisabled={isEditingDisabled}
                  />
                  <ConditionalInput
                    onChange={(e) => setEntityEditFormData('last_name', e.target.value)}
                    staticComponent={{
                      element: Paragraph,
                      props: {
                        className: 'username capitalized',
                      },
                    }}
                    editableContent={entityEditFormData?.last_name}
                    content={<span>{profileData?.last_name}</span>}
                    condition={isBeingEdited?.last_name}
                    inputComponent={{
                      element: TextInput,
                      props: {
                        placeholder: 'Last name',
                        className: 'username',
                      },
                    }}
                    isAllowedToEdit={isMyProfile}
                    isInputDisabled={isEditingDisabled}
                  />
                </h1>
                <EditIcons
                  className="names"
                  isBeingEdited={isBeingEdited?.last_name}
                  onEditSubmit={() => onEntityEditSubmit(['last_name', 'first_name'])}
                  onEditCancel={resetEntityEditFormData}
                  onEditStart={() => toggleIsBeingEdited('last_name')}
                  isLoading={isEntityEditLoading?.last_name}
                  shouldDisplay={isMyProfile}
                  shouldBeVisible={!isAnyBeingEdited}
                />
                {profileData?.branches?.map(({ name, slug, branch_type }) => (
                  <p className="entity-sidebar-subtitle" key={`sidebar-branch-${slug}`}>
                    <Link to={`/cities/${slug}`}>
                      {getBranchTypeAndName(name, branch_type)}
                    </Link>
                    {isBranchTerminated(branch_type) && (
                      <Tooltip title="A terminated branch" arrow>
                        <BlockIcon className="terminated-icon" />
                      </Tooltip>
                    )}
                  </p>
                ))}
                {profileData?.branch_applications?.map(({ name, slug, branch_type }) => (
                  <p className="entity-sidebar-subtitle" key={`sidebar-branch-application-${slug}`}>
                    <Link to={`/cities/${slug}`}>
                      {getBranchTypeAndName(name, branch_type)}
                    </Link>
                    <Tooltip title="Pending confirmation" arrow>
                      <WarningIcon className="unverified-icon" />
                    </Tooltip>
                  </p>
                ))}
                {isMyProfile && (
                  <div className={`email-profile-wrapper${isBeingEdited?.email ? ' wrapped' : ''}`}>
                    <EmailIcon className="email-profile-icon" />
                    <ConditionalInput
                      onChange={(e) => setEntityEditFormData('email', e.target.value)}
                      staticComponent={{
                        element: Paragraph,
                        props: {
                          className: 'email',
                        },
                      }}
                      editableContent={entityEditFormData?.email}
                      content={<span>{profileData?.email}</span>}
                      condition={isBeingEdited?.email}
                      inputComponent={{
                        element: TextInput,
                        props: {
                          placeholder: 'Email',
                          className: 'email',
                        },
                      }}
                      isAllowedToEdit={isMyProfile}
                      isInputDisabled={isEditingDisabled}
                    />
                    <EditIcons
                      isBeingEdited={isBeingEdited?.email}
                      onEditSubmit={() => onEntityEditSubmit(['email'])}
                      onEditCancel={resetEntityEditFormData}
                      onEditStart={() => toggleIsBeingEdited('email')}
                      isLoading={isEntityEditLoading?.email}
                      shouldDisplay={isMyProfile}
                      shouldBeVisible={!isAnyBeingEdited}
                    />
                    <p style={{ display: 'none' }}>error</p>
                  </div>
                )}
                {verificationWarnings}
              </div>
              {(profileData.description || isMyProfile) && (
                <EntitySection title="About me" headingVariant="h2" headingLineAfter>
                  <EditIcons
                    className="description-icons entity-section-edit-icons profile-description-icons"
                    isBeingEdited={isBeingEdited?.description}
                    onEditSubmit={() => onEntityEditSubmit(['description'])}
                    onEditCancel={resetEntityEditFormData}
                    onEditStart={() => toggleIsBeingEdited('description')}
                    isLoading={isEntityEditLoading?.description}
                    shouldDisplay={isMyProfile}
                    shouldBeVisible={!isAnyBeingEdited}
                  />
                  {(!profileData.description && isMyProfile && !isBeingEdited?.description) && (
                    <p className="no-description-info small">
                      You did not provide any description yet.
                    </p>
                  )}
                  {/*
                    TODO: Description can be moved to another component to avoid re-rendering
                     of the whole view on changing of the wrap set
                    */
                  }
                  <ConditionalInput
                    onChange={(e) => setEntityEditFormData('description', e.target.value)}
                    staticComponent={{
                      element: Div,
                      props: {
                        ref: descriptionRef,
                        className: 'profile-description-conditional-input',
                      },
                    }}
                    editableContent={entityEditFormData?.description}
                    content={createParagraphs(profileData.description, 'profile-description')}
                    condition={isBeingEdited?.description}
                    inputComponent={{
                      element: TextInput,
                      props: {
                        multiline: true,
                        placeholder: 'Tell us something about you!',
                        className: 'profile-description-conditional-input',
                        innerRef: descriptionRef,
                      },
                    }}
                    isAllowedToEdit={isMyProfile}
                    isInputDisabled={isEditingDisabled}
                    className={isDescriptionWrapped ? 'wrapped-text' : null}
                  />
                  {(shouldDisplayDescriptionWrapButton && !isBeingEdited?.description) && (
                    <div className="show-more-button-wrapper mobile-only">
                      <Button
                        onClick={() => setIsDescriptionWrapped(((prevState) => !prevState))}
                        className="secondary-button wrapped-text-button"
                        icon={ExpandLessIcon}
                      >
                        {isDescriptionWrapped ? 'Show more' : 'Show less'}
                      </Button>
                    </div>
                  )}
                </EntitySection>
              )}
            </div>
          </div>
          <div className="entity-column-layout-content">
            {(profileData)
            && <StickyMenu elements={stickyMenuElements} />}
            {profileData && (
              <div className="entity-inner-content">
                <EntitySection
                  title={`${personPrefix.link}Events`}
                  innerRef={userRefs.events.all}
                  nested
                >
                  {(!hasOrganizedEvents && !hasAttendedEvents) && (
                    <p className="no-cards dimmed">
                      {personPrefix.sentence}
                      no record of events yet
                    </p>
                  )}
                  {generateEventSection('organized')}
                  {generateEventSection('attended')}
                </EntitySection>
              </div>
            )}
          </div>
        </div>
      ) : (
        <div className="entity-loader">
          <CircularProgress size={70} />
        </div>
      )}
    </div>
  );
}

export default Profile;

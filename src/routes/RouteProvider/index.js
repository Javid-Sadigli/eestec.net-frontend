import React from 'react';
import {
  BrowserRouter as Router, Routes,
} from 'react-router-dom';
import routing from 'routes/routes';
import { renderRedirections, renderRouteCategory } from 'routes/RouteProvider/utils/routesRendering';

function Routing() {
  return (
    <Router>
      <Routes>
        {renderRouteCategory(routing.routes.mainLayout)}
        {renderRouteCategory(routing.routes.logoLayout)}
        {renderRedirections(routing.redirections)}
      </Routes>
    </Router>
  );
}

export default Routing;

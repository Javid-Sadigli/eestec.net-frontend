import Home from 'views/public/Home/Home';
import Events from 'views/public/Events/Events';
import Branches from 'views/public/Branches/Branches';
import JoinUs from 'views/public/JoinUs/JoinUs';
import AboutUs from 'views/public/AboutUs/AboutUs';
import Partners from 'views/public/Partners/Partners';
import Documents from 'views/public/Documents/Documents';
import Terms from 'views/public/TermsAndConditions/TermsAndConditions';
import Contact from 'views/public/Contact/Contact';
import Branch from 'views/public/Branch/Branch';
import Event from 'views/public/Event/Event';
import ProfileRouting from 'views/internal/Profile/components/ProfileRouting/ProfileRouting';

// commonly accessible routes of the main layout (header+footer)
export default [
  {
    // a path of the desired view
    path: '/',
    // the component to be rendered at the given path
    Element: Home,
    // indicates whether the route should be exact or just the beginning should match
    exact: true,
  },
  {
    path: '/events',
    Element: Events,
    exact: true,
  },
  {
    path: '/cities',
    Element: Branches,
    exact: true,
  },
  {
    path: '/join-us',
    Element: JoinUs,
    exact: true,
  },
  {
    path: '/about',
    Element: AboutUs,
    exact: true,
  },
  {
    path: '/partners',
    Element: Partners,
    exact: true,
  },
  {
    path: '/documents',
    Element: Documents,
    exact: true,
  },
  {
    path: '/terms',
    Element: Terms,
    exact: true,
  },
  {
    path: '/contact',
    Element: Contact,
    exact: true,
  },
  {
    path: '/cities/:branchSlug',
    Element: Branch,
  },
  {
    path: '/events/:eventSlug',
    Element: Event,
  },
  {
    // this is a private path but the logic behind resolving the route is elsewhere
    // so consider it common
    path: '/users/:userSlug',
    Element: ProfileRouting,
  },
];

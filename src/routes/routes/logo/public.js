import SignIn from 'views/auth/SignIn/SignIn';
import SignUp from 'views/auth/SignUp/SignUp';
import PasswordReset from 'views/auth/PasswordReset/PasswordReset';

// public-only routes of the logo layout
export default [
  {
    // a path of the desired view
    path: '/login',
    // indicates whether the route should be exact or just the beginning should match
    exact: true,
    // the component to be rendered at the given path
    Element: SignIn,
  },
  {
    path: '/register',
    exact: true,
    Element: SignUp,
  },
  {
    path: '/reset-password',
    exact: true,
    Element: PasswordReset,
  },
];

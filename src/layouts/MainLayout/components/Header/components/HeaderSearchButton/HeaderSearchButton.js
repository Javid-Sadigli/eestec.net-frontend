import React, { useState, useEffect, useRef } from 'react';
import SearchIcon from '@mui/icons-material/Search';

import './HeaderSearchButton.css';

/**
 __HeaderSearchButton__ is the button containing the logic used for searching different
 elements of the website from within the top menu,
 */
export default function HeaderSearchButton() {
  const [searchShownStatus, setSearchShownStatus] = useState(false);
  const [searchText, setSearchText] = useState('');
  const searchRef = useRef(null);

  const setSearchStatus = (shown) => {
    if (window.innerWidth <= 768) {
      setSearchShownStatus(true);
    } else {
      setSearchShownStatus(shown);
    }
  };

  // Use it only to set the correct shown status
  // according to the screen size on the first render of the screen
  useEffect(() => {
    setSearchStatus(false);
  }, []);

  // If the user hovers over the icon, the search gets focused.
  // This should not happen on mobile.
  useEffect(() => {
    if (searchShownStatus && window.innerWidth > 768) {
      searchRef.current.focus();
    }
  }, [searchShownStatus]);

  return (
    <div
      className={`header-search-box ${searchShownStatus ? 'header-search-box-open' : 'header-search-box-closed'}`}
      onMouseLeave={() => (searchText !== '' ? null
        : setSearchStatus(false))}
      onMouseEnter={() => {
        setSearchStatus(true);
      }}
    >
      {
      searchShownStatus
          && (
          <input
            ref={searchRef}
            onBlur={() => setSearchStatus(false)}
            onKeyDown={(e) => (e.key === 'Escape' && setSearchStatus(false))}
            value={searchText}
            onChange={(e) => setSearchText(e.target.value)}
            type="text"
            className="header-search-box-input"
            placeholder="Search"
          />
          )
    }
      <SearchIcon className={`header-search-box-icon ${searchShownStatus ? 'header-search-box-icon-open' : ''}`} />
    </div>
  );
}

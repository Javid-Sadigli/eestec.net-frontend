# EESTEC.net Frontend

Welcome to the **EESTEC.net Frontend** project.

## Documentation

The project is documented exhaustively within the code
and on the [**EESTEC.net Storybook Page**](https://eestec.gitlab.io/eestec-net/eestec.net-frontend).

If you want to contribute to the project, reading the Storybook is **crucial** for understanding the project's
structure, design principles and all reusable components and hooks.

## Setting up the development version of the website

### Prerequisites

- Node.js - 20.x
- yarn - 3.3.1

### Configuring the API source

If you do not configure the API to a specific base url, the default `http://localhost:8000` will be used, which
should point to your local Laravel Backend service.
Refer to the [Backend repository](https://gitlab.com/eestec/eestec.net-backend) for information on how to setup
Laravel locally.

If you wish to fetch data from other sources, you have to perform following steps:

1. Make a copy of the `.env.example` file in the root of the project and rename the copy to `.env`
2. In the `.env` file uncomment the line containing `REACT_APP_BASE_API_URL` and set it to the base url you want, e.g.
   ```
   REACT_APP_BASE_API_URL=https://api.eestec.net
   ```

**NOTE:** Using the public/staging API of EESTEC.net will only allow you to fetch data which does not require
authentication. You will not be able to access any internal parts of the data due to the CORS policy.

### Running the project

1. Run `yarn install` to download all the node modules
2. Run `yarn start`
3. Open [http://localhost:3000](http://localhost:3000) to view the website in the browser.
4. Enjoy coding!

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn storybook`

Runs the storybook in the development mode.\
Open [http://localhost:6006](http://localhost:6006) to view the storybook in the browser.

The storybook will reload if you make edits.

### `yarn test`

Launches the test runner in the interactive watch mode.\
There are no tests implemented at the moment.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
This is not a command that you should be concerned about, since the deployment is handled by our pipelines.

### `yarn build-storybook`

Builds the storybook for a deployment.

This is not a command that you should be concerned about, since the deployment of the storybook is handled by our pipelines.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can’t go back!**

If you aren’t satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you’re on your own.

You don’t have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn’t feel obligated to use this feature. However we understand that this tool wouldn’t be useful if you couldn’t customize it when you are ready for it.

_Seems that we do not need this script for now, just don't use it_ ;)

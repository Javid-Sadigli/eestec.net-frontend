## Purpose of the MR

## Anything to note:

## Before submit the MR I have:

- [ ] Made sure the implementation corresponds to the provided designs:
  - [ ] Colors.
  - [ ] Spacing/padding/margins.
  - [ ] Font size.
- [ ] Made sure that whatever changes I am introducing, all the elements of the website 
are responsive (including the new ones)
- [ ] Made sure to check common use-cases for errors and made sure any edge-cases won't cause the application to fail
- [ ] Ran linting, and made sure that:
  - [ ] There are no lint errors.
  - [ ] The exceptions were added only if absolutely necessary.
- [ ] Made sure that, while running the application:
  - [ ] Pages load properly, without throwing errors.
  - [ ] There are no errors in the console.
  - [ ] All user interactions are logical.
- [ ] Made sure that all the new elements I added and all the changes I made to existing ones are properly documented:
  - [ ] As comments in the code (if applicable).
  - [ ] In the storybook (if applicable).
- [ ] Tagged a reviewer.

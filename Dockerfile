FROM node:18-alpine AS builder

WORKDIR /usr/src/app
COPY package.json yarn.lock ./
RUN yarn
COPY . ./
RUN yarn build

FROM node:18-alpine
WORKDIR /app
RUN yarn global add serve
COPY --from=builder /usr/src/app/build /app

EXPOSE 3000
CMD [ "serve", "-n", "-s" ]

import EESTECnetStorybookTheme from './themes/EESTECnetStorybookTheme';
import '../src/index.css';
import './styles.css';

export const parameters = {
  actions: { argTypesRegex: "^on[A-Z].*" },
  controls: {
    matchers: {
      color: /(background|color)$/i,
      date: /Date$/,
    },
    sort: 'requiredFirst',
  },
  backgrounds: {
    default: 'EESTEC',
    values: [
      {
        name: 'EESTEC',
        value: '#141414',
      },
    ],
  },
  options: {
    storySort: {
      order: ['Docs', ['INTRO', '*'], 'Hooks', ['About', '*'], 'Components', '*'],
    },
  },
  docs: {
    theme: EESTECnetStorybookTheme,
  },
  layout: 'centered',
}

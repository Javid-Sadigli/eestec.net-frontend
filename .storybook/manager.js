import { addons } from '@storybook/addons';
import EESTECnetStorybookTheme from './themes/EESTECnetStorybookTheme';

addons.setConfig({
  theme: EESTECnetStorybookTheme,
});

import SplitButton from '../src/components/SplitButton/SplitButton';
import Button from '../src/components/Button/Button';
import placeholderImg from '../src/assets/img/entity-placeholder.jpg';
import React from 'react';

export const splitButtonMockedProps = {
  element: SplitButton,
  props: {
    label: 'Split button label',
    mainAction: {
      text: 'Main click',
      component: Button,
      onClick: () => alert('Main click action!'),
    },
    extraActions: [
      {
        text: 'Secondary click 1',
        component: Button,
        onClick: () => alert('Secondary click 1 action!'),
      },
      {
        text: 'Secondary click 2',
        component: Button,
        onClick: () => alert('Secondary click 2 action!'),
      },
    ],
  },
};

export const cardListItemMapper = {
  Card: (n) => ({
    image: placeholderImg,
    title: `Item ${n + 1}`,
    slug: `item-${n + 1}`,
  }),
  PersonCard: (n) => ({
    first_name: 'John',
    last_name: `${n + 1}th`,
    slug: `john-${n + 1}th`,
  }),
  EventCard: (n) => ({
    name: `${n + 1}th IT Sprint`,
    slug: `${n + 1}th-it-sprint`,
    location: 'LC Waikiki',
    start_date: '2022-01-01',
    end_date: '2022-01-02',
  }),
  BranchCard: (n) => ({
    name: `Waikiki ${n + 1}`,
    slug: `waikiki-${n + 1}`,
    type: n % 2 ? 'Junior Local Committee' : 'Local Committee',
  }),
};

export const loremIpsum = 'Yes plz banjo DIY lolang, viral four dollar toast kickstarter'
  + 'poke man bun bodega boys stumptown etsy whatever tousled drinking vinegar. '
  + 'Fingerstache everyday carry bespoke, live-edge salvia four loko marfa'
  + 'big mood. Kinfolk authentic bitters, before they sold out microdosing'
  + 'edison bulb stumptown palo santo iPhone etsy jianbing hoodie artisan'
  + 'vape PBR&B. Ramps gluten-free shabby chic, glossier asymmetrical'
  + 'semiotics typewriter. Artisan trust fund farm-to-table, seitan brunch'
  + 'squid Brooklyn pop-up swag pabst. Pickled praxis kombucha bitters'
  + 'vexillologist disrupt leggings. Chia pork belly lumber'
  + 'intelligentsia DIY 3 wolf moon mustache.';

export const generateExampleItems = (n, className = '') => [...Array(n).keys()]
  .map((num) => (
    <div className={`example-item ${className}`} key={`example-item-${num + 1}`}>Item {num + 1}</div>
  ));


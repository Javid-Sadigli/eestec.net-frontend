import ToastContext from '../../src/context/ToastContext';

export default function ToastContextDecorator(Story, context) {

  return (
    <ToastContext.Provider value={{
      toast: {
        autoHideDuration: context.args.autoHideDuration,
        open: context.args.open,
        vertical: context.args.vertical,
        horizontal: context.args.horizontal,
        content: context.args.content,
        type: context.args.type,
      },
      setToast: () => {},
      setToastContent: () => {},
    }}>
      {Story()}
    </ToastContext.Provider>
  )
}

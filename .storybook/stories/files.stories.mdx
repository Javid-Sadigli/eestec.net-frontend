import { Meta } from '@storybook/addon-docs';

<Meta title="Docs/File structure" />

# File structure

Contents of the `src` folder:
```raw
├───api
│   ├───config
|   │   └───...
│   └───service
|       └───...
├───assets
│   ├───img
│   │   ├───partners
|   │   └───...
|   └───...
├───components
│   ├───StickyMenu
|   │   ├───StickyMenu.js
|   │   ├───StickyMenu.stories.js
|   │   ├───StickyMenu.css
|   │   └───components
|   │       └───StickyMenuElement.js
|   └───...
├───context
│   ├───AuthContext.js
│   ├───ModalContext.js
|   └───...
├───hooks
|   ├───useOnScreen.js
|   ├───useOnScreen.stories.js
|   └───...
├───layouts
|   ├───MainLayout
|   │   ├───components
|   │   │   ├───Footer
|   │   │   │   ├───Footer.js
|   │   │   │   └───Footer.css
|   │   │   └───...
|   │   ├───MainLayout.js
|   │   └───MainLayout.css
|   └───...
├───routes
|   ├───Private.js
|   ├───PublicOnly.js
|   ├───Redirect.js
|   ├───routes
|   │   ├───layout
|   │   │   ├───common.js
|   │   │   ├───private.js
|   │   │   ├───public.js
|   │   │   └───index.js
|   │   ├───logo
|   │   │   └───...
|   │   ├───redirections
|   │   │   └───index.js
|   │   └───index.js
│   └───RouteProvider
|       └───index.js
├───stories
|   ├───css.stories.mdx
|   └───...
├───utils
|   ├───partners.js
|   └───...
└───views
    ├───auth
    │   └───...
    ├───internal
    │   └───...
    └───public
        ├───Home
        │   ├───components
        │   │   ├───HomeLanding
        │   │   │   ├───HomeLanding.js
        │   │   │   └───HomeLanding.css
        │   │   ├───HomeNumbers
        │   │   │   ├───HomeNumbers.js
        │   │   │   ├───HomeNumbers.css
        │   │   │   └───components
        │   │   │       └───...
        │   │   └───...
        │   ├───Home.js
        │   └───Home.css
        ├───Events
        │   └───...
        └───...

```

The tree above presents the current way of structuring the code, its bigger and smaller components and other elements so that everything is always clear to find. I made this tree quite extended so that everything is clear and well-presented.
Here's an explanation of different elements of the tree:
- `api`

  All logic behind API calls can go here. Thanks to this design **we don't need to build every single call with axios from scratch from within components**, we can just use services defined here (and define more services in the future). In the `service` folder we can find different classes which correspond to different categories of entities that we are manipulating with the API. And so, e.g. in a file `branch.js` we have the `BranchService` where we can simply define further functions to create, read, update or delete branches. Afterwards we just call these static functions from within our React components and handle the Promises.

- `assets`

  In this folder we put all the elements that are not code but are used within the code - e.g images. It can have more subdirectories created as we go. For example now the photos for testimonials are in `assets/img/testimonials/` directory.

- `components`

  All the components (usually smaller ones) that are **reusable** in different places on the website and/or **are not associated with any specific page**. An example for such is a `StickyMenu` which is not an element associated with any specific page, it is used on many different pages with different variants. Another example can be some formatted and themed `Button` that we would use anywhere in the website where we have... buttons. Each component has its own folder in which we can find `Component.js`, `Component.css` and `Component.stories.js`. The first two files are obvious. The `*.stories.js*` file's purpose is to document the component usage for this Storybook.

- `context`

  A directory for the definitions of context and its providers. Context is used to work with state that should be shared across multiple components, so-called global state.

- `hooks`

  A place to store all custom hooks that are not associated with any specific component or pages (the ones which are associated with a component or a view should be in a `hooks` folder in the subtree of that component, e.g. hooks in the `views/internal/Branch` view). There are many places where we manipulate state and effects in a similar manner. We can reuse some code thanks to custom hooks, the example for that would be the hook `useForm`. Other hooks can be some used for e.g. getting the information if an element is visible on the screen. Instead of writing all the logic for that within a component, we can just put it in a hook - in that case `useOnScreen`. **For more information on hooks, see [Hooks](/story/docs-hooks--page) page of this wiki**

- `layout`

  A place for defining elements of the layouts that are wrapping the main content of the website. Examples of that are the `MainLayout` (wrapping the content with a `Header` and a `Footer`) and the `LogoLayout` used for auth-related pages. In the future if needed we can define more reusable layouts.

- `routes`

  A directory for all the routing magic we are doing. At this point we have the `RouterProvider` for defining all the routes in the website. Additionally `Private` component is used to wrap views that are only for logged in users and `Redirect` component is there to handle redirections. The `PublicOnly` element works the same way. In the `routes` subdirectory routes declaration can be found for each layout.

- `stories`

  A directory to store all the Markdowns of files documenting the project in this Storybook.

- `utils`

  A place for all code (possibly reusable) that is not specifically a React code, just some JS, a utility. An example can be the `partners.js` piece of code, which gets all the partners logos and information into an Array and exports it nicely in one variable.

- `views`

  All the places for BIGGER COMPONENTS (or wrappers, containers - call it what you want). Basically here we store all the __pages of the website__. In most of the cases pages happen to be quite complex, with different API calls and other effects. The code of each page is inside a subdirectory specific for this page. As you can see in the tree as an example we have a page `Home` which is `views/public/Home/Home.js`.

  What is more - as you know it's good to split such more complex containers into smaller components. That's why each view can have its own `components` folder where we put smaller components **specific for this page** and not reusable by other parts of the website (that's the difference between such subfolders and the main `src/components` directory which consists of reusable components.

  Finally, the `views` are divided into three subdirectories:
   - `auth` - for pages which are about logging in or registration (this also concerns views for password resetting or confirming the email addresses)
   - `public` - for all the public pages of the website, which don't require logging in,
   - `internal` - for the internal part of the website, which requires login.

**Other notes:**
1. As mentioned, we have a differentiation between reusable `components` and `views` (pages). Once understood, this approach makes things much easier and clearer. It is just enough to remember what views are and how they are different from normal smaller components.

2. As you can see, we have a convention of nesting the components, both if it comes to `views` and `components`. So if we have a view `Home` which has an internal component `HomeNumber`, this `HomeNumber` component can have more smaller components that it is dependent on and that are **only used in it**. Therefore it can have another `components` folder inside.

3. Last small thing - naming convention of `.js` and `.css` files. One practice is to use `index.js` and `styles.css` or something similar within each folder containing each component. **We decided on another approach**. If we have a component **A** inside an `A` directory, its code files are `A.js` and `A.css`.


import { Meta } from '@storybook/addon-docs';

<Meta title="Docs/API calls" />

# API calls

The library <a href="https://github.com/axios/axios" rel="noopener noreferrer" target="_blank">`axios`</a> is used to send requests to EESTEC.net backend service
and retrieve information required to display on the website. This library proves useful as it simplifies certain
parts of the native `fetch` API and helps reducing boilerplate code. Another great aspect of the library
is its handling of network errors.
_(It even allows you to monitor the progress of upload and download in case we ever want to provide something like that in the future to improve UX!)_

In order to simplify the management of requests even more and reduce the boilerplate even more, we adjusted
`axios` functionalities to fit our project and provide an easy way to handle the requests and responses.

All configuration code and declarations of API service calls should be placed in `api/config` and `api/service` directories respectively.

**ALWAYS use those services to call the API or to create more functions that call it. No custom logic calling `axios`**
**directly from other parts of the project code should be written anywhere else.**

<br/>

## Configuration of the API engine - `api/config`

<br/>

### `defaults.js`

This file contains the definition of **clients** that we will be using to perform API calls.
For now we have two clients that are created using `axios.create` and have proper base urls associated:

- **API Client** (`BACKEND_URL/api/`) - for regular API calls to EESTEC.net back end service,
- **AUTH Client** (`BACKEND_URL/auth/`) - for EESTEC.net back end service API calls that handle authentication and authorization.

In the future a need may emerge to create more clients that would e.g. call external services. Creation of the new clients
should be rather straightforward if you take a look at the existing code.

Both of the currently defined clients depend on the `BASE_URL` value which is the base URL of our API service. This variable
is set based on `REACT_APP_BASE_API_URL` environment variable (which can be set e.g. using `.env file`).
If the value of this environment variable is empty, `localhost` is used
(for development purposes - refer to <a href="https://gitlab.com/eestec/eestec.net-backend/-/blob/dev/README.md" rel="noopener noreferrer" target="_blank">EESTEC.net backend documentation</a>
for information on how to set up your own local server of EESTEC.net backend for development purposes).

### `request.js`

The file `request.js` contains boilerplate code to create an async `request` function called on our clients. This function makes a pre-analysis of network errors
and handles the Promise appropriately. What is also important in this file are the definitions of `apiRequest` and `authRequest`. These two functions should
be called by API service classes described below.

<br/>

## API service classes - `api/service`

Each type of an element that we are fetching from our backend service should have their class defined. For example the logic of fetching events and branches
is handled respectively in `events.js` and `branches.js` files by defining `EventService` and `BranchService`.
These classes contain static methods (and only static methods!) which names describe the actions they are performing.
The purpose of `EventService.getEvents()` and `EventService.editEvent()` is rather self explanatory in that way.

Why creating those classes? For clarity, simplicity and creating some code with semantic meaning that does not require
getting into playing with urls every single time.

The static methods of the classes should call the requests described above (`apiRequest`, `authRequest` and others in the future if we ever need different types of them).
The argument passed to the request function should be a single object that consists of all the options that `axios` needs, like e.g. method type, data, **relative** url of the client.
The signature of those static methods of the service classes is up to you. Usually it just takes a single slug but especially for `PUT/PATCH` calls the logic gets
more complicated than this and may require adjusting the arguments to the form which is needed by `axios`. All of that can be done within the methods.

### Example definition of a class service
```js
export default class UserService {
  static getUserData(abortSignal = null) {
    return apiRequest({
      url: '/user',
      method: 'GET',
      signal: abortSignal,
    });
  }

  ...

  // The code below handles the alteration of user data and shapes the form data according to the arguments
  static editUserData(slug, data, withBannerPicture = false, withSquarePicture = false) {
    // Get rid of photos in the request data since we do not want do send this information by default
    let {
      profile_photo_path, banner_path, ...requestData
    } = data;
    // Include the banner photo if user wants it
    if (withBannerPicture) {
      requestData = {
        ...requestData,
        banner_path,
      };
    // Include the square photo if user wants it
    } else if (withSquarePicture) {
      requestData = {
        ...requestData,
        profile_photo_path,
      };
    }
    return apiRequest({
      url: `/users/${slug}`,
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      data: getFormData({
        ...requestData,
        _method: 'PUT',
      }),
    });
  }
}
```

You may have noticed that even though the second method described in the aforementioned example is editing data it has
a method of `POST` and not `PUT/PATCH`. This was required by Laravel for some reason (ask our BE magicians if you are curious).
Instead the actual method is declared within the data object of the options.

Finally - these are the classes and their static methods that should be called from our components and views to call API services. There is no need to call axios default
functions from scratch anywhere since this boilerplate makes the code much more elegant and readable if you understand the concept. Calling a method of a service classes
can be handled as any other `Promise` to handle the responses and catch errors.

### Example usage of a service class

```js
BranchService.getBranches()
  .then((response) => setBranches(response))
  .catch((err) => logger.log(err))
  .finally(() => setAreBranchesLoading(false));
```

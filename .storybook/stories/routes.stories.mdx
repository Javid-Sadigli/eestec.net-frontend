import { Meta, ArgsTable, Description  } from '@storybook/addon-docs';
import Private from '../../src/routes/Private';
import PublicOnly from '../../src/routes/PublicOnly';
import Redirect from '../../src/routes/Redirect';

<Meta title="Docs/Routes" />

# Routes

The project uses <a href="https://reactrouter.com/en/v6.3.0/getting-started/overview" rel="noopener noreferrer" target="_blank">`react-router-v6`</a>
to handle routing and creation of different URLs and pages on the website. All logic behind the routing can be found in the `routes` directory.

The most important part of this part of the code is the `RouteProvider` component that declares all the routes available on EESTEC.net.
Inside the `BrowserRouter` every single path is declared using the `Route` element.

The `Route` element accepts following props that are interesting for us:

- `path` - the relative to website's base URL route of the declared page, can be multi-level with multiple slashes.
A parameter that should be passed to a page can be defined by putting a colon before it, e.g. `/events/:eventSlug`,
- `exact` - if set, the path would be only resolved for an exact match, otherwise also for pages which routes start with the given `path`,
- `element` - the actual component (view) that we want to render under the given route.

The last property - `element` - is particularly interesting as we can use this property to wrap our desired views into
custom written component wrappers such as `Private`, `PublicOnly` or `Redirect` that handle what should happen to a user's
request to visit a certain page depending on their authorization status.

<br />

## Wrapping components

<br />

### `Private`

<Description of={Private} />
<ArgsTable of={Private} />

### `PublicOnly`

<Description of={PublicOnly} />
<ArgsTable of={PublicOnly} />

### `Redirect`

<Description of={Redirect} />
<ArgsTable of={Redirect} />

## Routes splitting and definitions

In order to make it easier for us to define routes, the rendering of `<Route />` elements is handled by mapping through
routes specification declared in other files.

Let's remember that the website is split into layouts. Each layout is associated with one page or many pages,
which means that each layout can be associated with many routes.
In the `routes/routes` directory subdirectories can be found containing route declarations
for each layout. The structure of each of those subdirectories remains exactly the same and it can have the following `.js` files:

### Commonly accessible routes - `common.js`
Exports declarations of commonly accessible routes of a layout (routes which should be accessible both for authenticated users and guests)
```js
export default [
  {
    // a path of the desired view
    path: '/',
    // the component to be rendered at the given path
    Element: Home,
    // indicates whether the route should be exact or just the beginning should match
    exact: true,
  },
  // ...
];
```

### Private routes - `private.js`
Exports declarations of routes accessible only by logged-in users (depending on the additional properties we can define whether a logged-in, unverified user should have access to the given route),
```js
export default [
  {
    // a path of the desired view - together with the named parameter to be passed
    // as a route specification
    path: '/verify-email/:token',
    // indicates whether a logged in but unverified user should have access
    allowUnverified: true,
    // the component to be rendered at the given path
    Element: EmailVerification,
    // indicates whether the route should be exact or just the beginning should match
    exact: true,
  },
  // ...
];
```

### Public-only routes - `public.js`
Exports declarations of routes accessible only by logged-out users (e.g. the login or sign up view),
```js
export default [
  {
    // a path of the desired view
    path: '/login',
    // indicates whether the route should be exact or just the beginning should match
    exact: true,
    // the component to be rendered at the given path
    Element: SignIn,
  },
  // ...
];
```

### Layout routes definition - `index.js`
Gathers the three aforementioned route categories and exports them all together.
```js
export default {
  // the root element of the layout - can be null if there are no wrappers serving outlets
  root: {
    // the root path of the elements that should have this layout
    path: '/',
    // the layout wrapper element, which renders the route-specific content as Outlet
    Element: MainLayout,
  },
  // specification of different route categorie of the given layout
  routes: {
    common: commonRoutes,
    private: privateRoutes,
    public: [],
  },
};
```

## Redirections definition

As the last one within the routing provider, redirections are rendered.
The redirections definitions are also created in separate files, under the `routes/routes/redirections` directory.
An example of a redirection specification is as follows:
```js
{
  // a source path of the redirection (together with all thje params we want)
  source: '/branches/:branchSlug',
  // a destination path of the redirection (without parameters)
  destination: '/cities',
  // indicates whether a browser history element should be replaced or just pushed to the stack
  replace: true,
  // parameters names (in the order of occurrence) that should be put into the redirection
  paramNames: ['branchSlug'],
}
```

## Explicit declaration of private routes

As an addition to all the aforementioned details, all the private routes should be also
**explicitly** defined in the `routes/routes/index.js` folder, as this information
is crucial for making a decision whether a user should be redirected after a logout or not.

## Examples of mapped routes

Without the mapping of JS objects created in the `routes/routes` folder, the routes declaration
would look as follows. The route-rendering functions can be found in the `routes/utils` directory.
Under the hood, the mapping logic renders the following components within the routing provider:

### A regular page without parameters

```js
<Route
  exact
  path="/events"
  element={<Events />}
/>
```

### A regular page with parameters

```js
<Route
  // it comes with a parameter so we need to remove the `exact` property
  path="/events/:eventSlug" // this parameter will be readable within the code of <Event />
  element={<Event />}
/>
```

### A page that should only be accessible to registered users

```js
<Route
  exact
  path="/my-profile"
  element={(
    <Private
      allowUnverified // allow registered users even if they are not verified
      Component={Profile} // the actual component to be rendered if authorization is successful
    />
  )}
/>
```

### A page that should only be accessible to registered, verified users

```js
<Route
  path="/some-private-route/:someParameter"
  element={(
    <Private
      Component={SomePrivateRouteComponent} // the actual component to be rendered if authorization is successful
    />
  )}
/>
```

### A page that should only be accessible to users who are not logged in

```js
<Route
  exact
  path="/login"
  element={<PublicOnly Component={SignIn} />}
/>
```

### A redirection without parameters

```js
<Route
  exact
  path="/branches"
  element={(
    <Redirect
      to="/cities" // target of the redirection
      replace // should replace the old route in the browser history
    />
  )}
/>
```

### A redirection with parameters

```js
<Route
  path="/branches/:branchSlug"
  element={(
    <Redirect
      to="/cities" // target of the redirection
      replace // should replace the old route in the browser history
      paramNames={['branchSlug']} // branchSlug is the parameter to pass on with the redirection
    />
  )}
/>
```

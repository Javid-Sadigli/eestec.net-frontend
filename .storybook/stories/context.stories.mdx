import { Meta, Description } from '@storybook/addon-docs';
import { AuthProvider } from '../../src/context/AuthContext';
import { ModalProvider } from '../../src/context/ModalContext';
import { PartnersProvider } from '../../src/context/PartnersContext';
import { ToastProvider } from '../../src/context/ToastContext';

<Meta title="Docs/Context" />

# Context

Certain elements of the website have to be stored as a global state, not the local state
since they are reused by numerous components across the whole website.

For this purpose, several Context Providers have been implemented, as the Context API
(read more <a href="https://reactjs.org/docs/context.html" rel="noopener noreferrer" target="_blank">here</a> and <a href="https://reactjs.org/docs/hooks-reference.html#usecontext" rel="noopener noreferrer" target="_blank">here</a>)
provides a light-weight built-in functionality of storing and manipulating the global state.

All context providers should be defined within the `context` folder of the project. Then, these providers
should wrap the content of the website in some place - preferably in the "lowest" possible place in the
components' tree (every change of context state re-renders all its provider's children so the less
re-renders, the better).

<br />

## Context Providers

<br />

### `AuthContext`

<Description of={AuthProvider} />

#### Output

| Parameter               | Type       | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| :---------------------- | :--------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `loggedIn`              | `bool`     | A boolean value indicating whether a user is logged in - `true` if user is logged in, `false` otherwise                                                                                                                                                                                                                                                                                                                                                                                           |
| `userData`              | `object`   | An object containing the data of logged in user (names, email, branches etc.).                                                                                                                                                                                                                                                                                                                                                                                                                    |
| `shouldRedirect`        | `bool`     | A boolean value indicating whether a logged in user should be redirected from public-only pages. This value is updated on each update of user data accordingly to the second argument of `updateUserData`. How it is used now is that this value is set to false if after a login a user should be presented with additional information before proceeding to the actual website. Afterwards the value is reset to `false` so any next calls to public-only pages are redirected to the homepage. |
| `enableAuthRedirection` | `function` | A function that sets `shouldRedirect` to true.                                                                                                                                                                                                                                                                                                                                                                                                                                                    |
| `updateUserData`        | `function` | A function used to update the user data stored in the global context state. It may be used e.g. after a successful registration or login, after a logout or after changing user data directly or by applying to a branch or a team.                                                                                                                                                                                                                                                               |
| `logout`                | `function` | A function that logs the user out, setting `loggedIn` to `false`, setting `shouldRedirect` to false and clearing `userData` from both the global state and the browser's local storage.                                                                                                                                                                                                                                                                                                           |

<br />

### `ModalContext`

<Description of={ModalProvider} />

#### Output

| Parameter         | Type       | Description                                                                                                                           |
| :---------------- | :--------- | ------------------------------------------------------------------------------------------------------------------------------------- |
| `modalContent`    | `Element`  | Any React element (component) that should be rendered as a modal.                                                                     |
| `setModalContent` | `function` | A function that sets the modal content. Accepts one argument - the element to be rendered. If `null` is passed, the modal disappears. |

<br />

### `PartnersContext`

<Description of={PartnersProvider} />

#### Output

| Parameter         | Type       | Description                                                                                                                                |
| :---------------- | :--------- | ------------------------------------------------------------------------------------------------------------------------------------------ |
| `partners`        | `array`    | An array containing objects with partners data (`name`, `logo`, `slug`, `description`).                                                    |
| `setPartners    ` | `function` | A function that sets the partners data. The partner data should be an array with partners objects (`name`, `logo`, `slug`, `description`). |

<br />

### `ToastContext`

<Description of={ToastProvider} />

#### Output

| Parameter         | Type       | Description                                                                                                                                                                                                            |
| :---------------- | :--------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `toast`           | `Element`  | Any React element (component) that should be rendered as a modal.                                                                                                                                                      |
| `setToast`        | `function` | A function used directly in the `<Toast />` component, for disabling the toast. It **should not** be used anywhere else!                                                                                               |
| `setToastContent` | `function` | A function that sets the new toast content. It accepts two arguments, first being the `content` of the toast (`string`), second being the desired `type` of the toast (`'info'`, `'success'`, `'warning'`, `'error'`). |

<br />

## Example

### Providing

```js
const ExampleContext = createContext(null);

export function ExampleProvider({ children }) {
  const [exampleContent, setExampleContent] = useState(null);

  const exampleValue = useMemo(
    () => ({ exampleContent, setExampleContent }),
    [exampleContent, setExampleContent],
  );

  return (
    <ExampleContext.Provider value={exampleValue}>
      {children}
    </ExampleContext.Provider>
  );
}

ExampleProvider.propTypes = {
  children: PropTypes.node.isRequired,
};

export default ExampleContext;
```

### Consuming

```js
// Component One
const { setExampleContent } = useContext(ExampleContext);

useEffect(() => {
  ExampleService.getExamples()
    .then((response) => setExampleContent(response))
    .catch((err) => logger.log(err));
}, []);

// Component Two
const { exampleContent } = useContext(ExampleContext);

return (
  <div>{exampleContent}</div>
);
```
